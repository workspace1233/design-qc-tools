import arcpy

arcpy.env.overwriteOutput = True

class ParentItem1(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name

    def ItemNoOLD(self):
        arcpy.AddMessage('Tool Check Run For item 1.6')

        arcpy.CopyFeatures_management(Fibre_duct, "Duct_Copy")
        arcpy.CalculateField_management("Duct_Copy", "OriginalFID_duct", "!OBJECTID!", "PYTHON3")
        arcpy.CopyFeatures_management(Chamber, self.Output_name)
        arcpy.CopyFeatures_management(Chamber, "chamber_Copy")
        arcpy.CalculateField_management("chamber_Copy", "OriginalFID_chamber", "!OBJECTID!", "PYTHON3")

        try:
            if arcpy.Exists(self.Output_name):
                lstFields_ch = arcpy.ListFields(self.Output_name)
                field_names_ch = [f.name for f in lstFields_ch]
                if "Error" not in field_names_ch:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error", "TEXT",100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name2.. :  " + str(e))

        arcpy.Select_analysis("chamber_Copy", "enet_chamber", 'ownership = 3')
        arcpy.Select_analysis("chamber_Copy", "eir_chamber", 'ownership = 2')
        arcpy.Select_analysis("chamber_Copy", "nbi_chamber", 'ownership = 1 and status = 1')
        arcpy.Select_analysis("Duct_Copy", "nbi_duct", 'owner = 1 and status = 1')

        arcpy.SpatialJoin_analysis("nbi_duct", "enet_chamber", "enet_nbi_duct", join_operation='JOIN_ONE_TO_ONE', join_type='KEEP_ALL', match_option='INTERSECT')
        arcpy.SpatialJoin_analysis("nbi_chamber", "enet_nbi_duct", "enet_nbi_chamber", join_operation='JOIN_ONE_TO_ONE', join_type='KEEP_ALL', match_option='INTERSECT')

        arcpy.SpatialJoin_analysis("nbi_duct", "eir_chamber", "eir_nbi_duct", join_operation='JOIN_ONE_TO_ONE', join_type='KEEP_ALL', match_option='INTERSECT')
        arcpy.SpatialJoin_analysis("nbi_chamber", "eir_nbi_duct", "eir_nbi_chamber", join_operation='JOIN_ONE_TO_ONE', join_type='KEEP_ALL', match_option='INTERSECT')

        #arcpy.SpatialJoin_analysis("nbi_duct", "enet_nbi_chamber", "enet_nbi_chamber_duct", join_operation='JOIN_ONE_TO_ONE', join_type='KEEP_ALL', match_option='INTERSECT')
        #arcpy.SpatialJoin_analysis("eir_chamber", "enet_nbi_chamber_duct", "enet_nbi_eir_chamber", join_operation='JOIN_ONE_TO_ONE', join_type='KEEP_ALL', match_option='INTERSECT')

        arcpy.SpatialJoin_analysis("enet_nbi_chamber", "eir_nbi_chamber", "common_join", join_operation='JOIN_ONE_TO_ONE', join_type='KEEP_ALL', match_option='INTERSECT')

        array = arcpy.da.FeatureClassToNumPyArray("common_join", ["OriginalFID_chamber", "Join_Count"], skip_nulls=True)

        try:
            for data in array:
                with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Error")) as cursor:
                    for rows in cursor:
                        if rows[0] != None and str(data[1]) != 'None':
                            if rows[0] == int(data[0]) and int(data[1]) == 1:
                                rows[1] = "Chambers can't connect directly without NBI"
                            cursor.updateRow(rows)
                    del cursor


        except Exception as ex:
            arcpy.AddMessage(ex)

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 1.6')

        arcpy.CopyFeatures_management(Fibre_duct, "Duct_Copy")
        arcpy.CalculateField_management("Duct_Copy", "OriginalFID_duct", "!OBJECTID!", "PYTHON3")
        arcpy.CopyFeatures_management(Chamber, self.Output_name)
        arcpy.CopyFeatures_management(Chamber, "chamber_Copy")
        arcpy.CalculateField_management("chamber_Copy", "OriginalFID_chamber", "!OBJECTID!", "PYTHON3")

        try:
            if arcpy.Exists(self.Output_name):
                lstFields_ch = arcpy.ListFields(self.Output_name)
                field_names_ch = [f.name for f in lstFields_ch]
                if "Error" not in field_names_ch:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name2.. :  " + str(e))


        test = arcpy.SelectLayerByAttribute_management("chamber_Copy", "NEW_SELECTION", 'ownership = 3')

        arcpy.CopyFeatures_management(test, "chamber_enet")

        test_location = arcpy.SelectLayerByLocation_management("Duct_Copy", "intersect", "chamber_enet")

        arcpy.CopyFeatures_management(test_location, "duct_enet_copy")

        subset_test = arcpy.SelectLayerByAttribute_management("duct_enet_copy", "SUBSET_SELECTION", ' "owner" = 1 ')

        arcpy.CopyFeatures_management(subset_test, "suset_copy")

        final_enet_copy = arcpy.SelectLayerByLocation_management("chamber_Copy", "intersect", subset_test)

        arcpy.CopyFeatures_management(final_enet_copy, "final_enet_copy")

        arcpy.AddField_management("final_enet_copy", "Error", "TEXT", 100)

        # try:
        #     with arcpy.da.UpdateCursor("final_enet_copy", ("ownership", "Error")) as cursor:
        #         for row in cursor:
        #             if row[0] != None:
        #                 if row[0] == 1:
        #                     row[1] = "Chambers can't connect directly without NBI"
        #             cursor.updateRow(row)
        #         del cursor
        # except Exception as ex:
        #     arcpy.AddMessage(ex)


        test_eir = arcpy.SelectLayerByAttribute_management("chamber_Copy", "NEW_SELECTION", 'ownership = 2')

        arcpy.CopyFeatures_management(test_eir, "chamber_eir")

        test_eir_location = arcpy.SelectLayerByLocation_management("Duct_Copy", "intersect", "chamber_eir")

        arcpy.CopyFeatures_management(test_eir_location, "duct_eir_copy")

        subset_eir_test = arcpy.SelectLayerByAttribute_management("duct_eir_copy", "SUBSET_SELECTION", ' "owner" = 1 ')

        arcpy.CopyFeatures_management(subset_eir_test, "suset_eir_copy")

        final_eir_copy = arcpy.SelectLayerByLocation_management("chamber_Copy", "intersect", subset_eir_test)

        arcpy.CopyFeatures_management(final_eir_copy, "final_eir_copy")

        arcpy.AddField_management("final_eir_copy", "Error", "TEXT", 100)



        #arcpy.Merge_management(["final_eir_copy", "final_enet_copy"], "merge_copy")

        arcpy.SpatialJoin_analysis("final_eir_copy", "final_enet_copy", "join_copy", "JOIN_ONE_TO_ONE", "KEEP_ALL", "", "INTERSECT")

        try:
            with arcpy.da.UpdateCursor("join_copy", ("ownership", "Error", "Join_Count")) as cursor:
                for row in cursor:
                    if row[0] != None:
                        if row[0] != 1 and row[2] == 1:
                            row[1] = "Chambers can't connect directly without NBI"
                    cursor.updateRow(row)
                del cursor
        except Exception as ex:
            arcpy.AddMessage(ex)

        array_enet_chamber = arcpy.da.FeatureClassToNumPyArray("join_copy", ["OriginalFID_chamber", "Error"], skip_nulls=True)

        try:
            for data in array_enet_chamber:
                    with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Error")) as cursor:
                        for row in cursor:
                            if str(data[1]) != 'None':
                                if int(row[0]) == int(data[0]):
                                    row[1] = data[1]
                            cursor.updateRow(row)
                        del cursor
        except Exception as ex:
            arcpy.AddMessage(ex)
        #
        # try:
        #     for data in array_eir_chamber:
        #             with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Error")) as cursor:
        #                 for row in cursor:
        #                     if str(data[1]) != 'None':
        #                         if int(row[0]) == int(data[0]):
        #                             row[1] = data[1]
        #                     cursor.updateRow(row)
        #                 del cursor
        # except Exception as ex:
        #     arcpy.AddMessage(ex)
        


    def __del__(self):
        #### Object Remove Here ######
        if arcpy.Exists("Duct_Copy"):
            arcpy.Delete_management("Duct_Copy")
        if arcpy.Exists("chamber_Copy"):
            arcpy.Delete_management("chamber_Copy")
        if arcpy.Exists("chamber_enet"):
            arcpy.Delete_management("chamber_enet")
        if arcpy.Exists("duct_enet_copy"):
            arcpy.Delete_management("duct_enet_copy")
        if arcpy.Exists("suset_copy"):
            arcpy.Delete_management("suset_copy")
        if arcpy.Exists("chamber_eir"):
            arcpy.Delete_management("chamber_eir")
        if arcpy.Exists("duct_eir_copy"):
            arcpy.Delete_management("duct_eir_copy")
        if arcpy.Exists("suset_eir_copy"):
            arcpy.Delete_management("suset_eir_copy")
        if arcpy.Exists("final_enet_copy"):
            arcpy.Delete_management("final_enet_copy")
        if arcpy.Exists("final_eir_copy"):
            arcpy.Delete_management("final_eir_copy")
        if arcpy.Exists("join_copy"):
            arcpy.Delete_management("join_copy")


class BaseClass(ParentItem1):
    def __init__(self):
        from datetime import datetime

        now = datetime.now()
        start_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run Start's @" + start_time)

        if arcpy.Exists("ChamberTransition"):
            arcpy.Delete_management("ChamberTransition")

        ### Object Intialize Here ####
        if item1point1 == 'true':
            ParentItem1.__init__(self, 'ChamberTransition')
            ParentItem1.ItemNo(self)
            ParentItem1.__del__(self)


    def remove_duplicates_None_values(self, array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[1] != 'None':
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1

    def remove_duplicates_zero_values(array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[2] != 0:
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1

    def largest(self, arr, n):

        # Initialize maximum element
        max = arr[1]
        # items = arr[0]
        # Traverse array elements from second
        # and compare every element with
        # current max
        # for items[0] in arr:
        for i in range(1, n):
            if arr[i] > max:
                max = arr[i]
        return max

    def largest(self, arr, n):

        # Initialize maximum element
        max = arr[0]

        # Traverse array elements from second
        # and compare every element with
        # current max
        for i in range(1, n):
            if arr[i] > max:
                max = arr[i]
        return max

    def get_only_maximum_length(self, array, id):
        array_length = []
        array_id = []
        i = 0
        if len(array) != 0:
            for data in array:
                if len(array_id) == 0 and id in data:
                    array_id.append(data[0])
                    array_length.append(data[1])
                elif id in data:
                    array_id.append(data[0])
                    array_length.append(data[1])

            n = len(array_length)
            i = self.largest(array_length,n)
            #array_id.pop(i)
        return i

    def __del__(self):
        ### Object Remove Here ######


        fieldList = arcpy.ListFields(self.Output_name)
        fieldsToBeDeleted = []
        for field in fieldList:
            if str(field.name) not in (["OBJECTID", "Shape", "name", "Error", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area",
                     "SHAPE_Area"]):
                fieldsToBeDeleted.append(field.name)

        arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))

        try:
            with arcpy.da.UpdateCursor(self.Output_name, ("Error")) as cursor:
                for row in cursor:
                    if row[0] == None:
                        cursor.deleteRow()
                del cursor
        except Exception as ex:
            arcpy.AddMessage(ex)

        try:
            if arcpy.Exists(self.Output_name):
                arcpy.DeleteField_management(self.Output_name, fieldsToBeDeleted)
        except Exception as e:
            print(e)
        except arcpy.ExecuteError as ex:
            print(ex)
        arcpy.AddMessage("Finished Deleting unwanted fields of error ")
        arcpy.AddMessage(str(self.Output_name) + " - Log File has been Generated")

        from datetime import datetime

        now = datetime.now()
        end_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run End's @" + end_time)


######### Get All Input Parameter ###########

arcpy.env.overwriteOutput = True

input_geodatabase = arcpy.GetParameterAsText(0)

Fibre_duct = arcpy.GetParameterAsText(1)
Chamber = arcpy.GetParameterAsText(2)

######## Checked Input #########

item1point1 = arcpy.GetParameterAsText(3)

######### Oject Creation #######
# main function
if __name__ == '__main__':
    if arcpy.Exists("ChamberTransition"):
        arcpy.Delete_management("ChamberTransition")
    # created the object for BaseClass
    ob = BaseClass()