import arcpy
import numpy as np
import os
arcpy.env.overwriteOutput = True

class ParentItem1(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name
        self.span_copy = "Span_Copy"

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 1.1')

        arcpy.CopyFeatures_management(Span, self.span_copy)
        arcpy.CalculateField_management("Span_Copy", "OriginalFID_span", "!OBJECTID!", "PYTHON3")
        arcpy.CopyFeatures_management(Pole, "Pole_Copy")
        arcpy.CopyFeatures_management(Chamber, "Chamber_Copy")
        arcpy.CopyFeatures_management(Customer_Building, "Customer_Building_Copy")
        arcpy.CopyFeatures_management(Olt, "olt_Copy")
        SR = arcpy.Describe("Span_Copy").spatialReference

        if not arcpy.Exists(self.Output_name):
            arcpy.CopyFeatures_management(Span, self.Output_name)

        arcpy.Merge_management(["Pole_Copy", "Chamber_Copy" , "olt_Copy" , "Customer_Building_Copy"], "merged_layers")

        # arcpy.FeatureVerticesToPoints_management("Span_Copy",
        #                                          "both_ends_span_copy",
        #                                          point_location = "BOTH_ENDS")

        lstFields_fs = arcpy.ListFields(self.span_copy)
        field_names_fs = [f.name for f in lstFields_fs]
        if "OriginalOID" not in field_names_fs:
            arcpy.AddField_management(self.span_copy, "OriginalOID", "LONG", 10)
            arcpy.CalculateField_management(self.span_copy, "OriginalOID", "!OBJECTID!", "PYTHON3")
        else:
            pass
        fields_span = ("OriginalOID" , "SHAPE@XY")
        arr_span = arcpy.da.FeatureClassToNumPyArray(self.span_copy, fields_span, skip_nulls=True, explode_to_points=True)
        arcpy.AddMessage("arr_span")
        arcpy.AddMessage(str(arr_span))

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase
        arcpy.AddMessage(str(p1))
        p2 = p1 + '\\' + "span_start"
        p3 = p1 + '\\' + "span_end"
        arcpy.AddMessage(str(p2))

        def remove_dup_by_first_element(list1):
            first_element_list = []
            list2 = []
            for x in list1:
                if x[0] not in first_element_list:
                    list2.append(x)
                    first_element_list.append(x[0])
            return list2

        arr_span_start = remove_dup_by_first_element(arr_span)
        # arcpy.AddMessage("arr_span_start")
        # arcpy.AddMessage(arr_span_start)

        arr_span_end1 = arr_span
        arr_span_end2 = arr_span_end1[::-1]
        arr_span_end2 = remove_dup_by_first_element(arr_span_end2)
        arr_span_end = arr_span_end2[::-1]
        # arcpy.AddMessage("arr_span_end")
        # arcpy.AddMessage(arr_span_end)

        arr_span_start_numpy = np.array(arr_span_start)
        arcpy.AddMessage("arr_span_start_numpy")
        arcpy.AddMessage(arr_span_start_numpy)

        arr_span_end_numpy = np.array(arr_span_end)
        arcpy.AddMessage("arr_span_end_numpy")
        arcpy.AddMessage(arr_span_end_numpy)

        if not arcpy.Exists(p2):
            arcpy.da.NumPyArrayToFeatureClass(arr_span_start_numpy, p2, ("SHAPE@XY"), SR)
        if not arcpy.Exists(p3):
            arcpy.da.NumPyArrayToFeatureClass(arr_span_end_numpy, p3, ("SHAPE@XY"), SR)

        arcpy.Merge_management(["span_start", "span_end"], "both_ends_span_copy")
        # #
        arcpy.analysis.SpatialJoin("both_ends_span_copy", "merged_layers", "SJ_span_merged", "JOIN_ONE_TO_ONE", "KEEP_ALL",
                                   "", "INTERSECT", "", "")

        try:
            if arcpy.Exists("SJ_span_merged"):
                lstFields_sc = arcpy.ListFields("SJ_span_merged")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in SJ_span_merged..")
                    arcpy.AddField_management("SJ_span_merged", "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        with arcpy.da.UpdateCursor("SJ_span_merged", ("Join_Count", "Error")) as cursor:
            for rows in cursor:
                if rows[0] == 0:
                    rows[1] = "Span not snapped"
                    cursor.updateRow(rows)
            del cursor

        array = arcpy.da.FeatureClassToNumPyArray("SJ_span_merged", ["OriginalOID", "Error"], skip_nulls=True)
        arcpy.AddMessage("array")
        arcpy.AddMessage(array)
        array1 = self.remove_duplicates_None_values(array)
        arcpy.AddMessage("array1")
        arcpy.AddMessage(array1)

        try:
            if arcpy.Exists(self.Output_name):
                lstFields_sc = arcpy.ListFields(self.Output_name)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error", "TEXT",100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final Log File- 1.1 ")
            for x in array1:
                with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Error")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            if row[1] != None:
                                arcpy.AddMessage(str(row[1]))
                                row[1] = str(row[1]) + "," + x[1]
                            elif row[1] == None:
                                row[1] = x[1]
                                print(x[1])
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final  Log File- 1.1.1:  " + str(e))

    def __del__(self):
        #### Object Remove Here ######
        if arcpy.Exists("Span_Copy"):
            arcpy.Delete_management("Span_Copy")
        if arcpy.Exists("olt_copy"):
            arcpy.Delete_management("olt_copy")
        if arcpy.Exists("Duct_Copy"):
            arcpy.Delete_management("Duct_Copy")
        if arcpy.Exists("Pole_Copy"):
            arcpy.Delete_management("Pole_Copy")
        if arcpy.Exists("Chamber_Copy"):
            arcpy.Delete_management("Chamber_Copy")
        if arcpy.Exists("Customer_Building_Copy"):
            arcpy.Delete_management("Customer_Building_Copy")
        if arcpy.Exists("SJ_span_merged"):
            arcpy.Delete_management("SJ_span_merged")
        if arcpy.Exists("merged_layers"):
            arcpy.Delete_management("merged_layers")
        if arcpy.Exists("both_ends_span_copy"):
            arcpy.Delete_management("both_ends_span_copy")
        if arcpy.Exists("span_end"):
            arcpy.Delete_management("span_end")
        if arcpy.Exists("span_start"):
            arcpy.Delete_management("span_start")

class ParentItem2(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name2 = name

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 1.2')

        arcpy.CopyFeatures_management(Fibre_duct, "Duct_Copy")
        arcpy.CalculateField_management("Duct_Copy", "OriginalFID_span", "!OBJECTID!", "PYTHON3")
        arcpy.CopyFeatures_management(Pole, "Pole_Copy")
        arcpy.CopyFeatures_management(Chamber, "Chamber_Copy")
        arcpy.CopyFeatures_management(Customer_Building, "Customer_Building_Copy")
        arcpy.CopyFeatures_management(Olt, "olt_copy")
        SR = arcpy.Describe("Duct_Copy").spatialReference

        if not arcpy.Exists(self.Output_name2):
            arcpy.CopyFeatures_management(Fibre_duct, self.Output_name2)

        arcpy.Merge_management(["Pole_Copy", "Chamber_Copy", "olt_Copy", "Customer_Building_Copy"], "merged_layers")

        # arcpy.FeatureVerticesToPoints_management("Duct_Copy",
        #                                          "both_ends_duct_copy",
        #                                          point_location="BOTH_ENDS")

        lstFields_fs = arcpy.ListFields("Duct_Copy")
        field_names_fs = [f.name for f in lstFields_fs]
        if "OriginalOID" not in field_names_fs:
            arcpy.AddField_management("Duct_Copy", "OriginalOID", "LONG", 10)
            arcpy.CalculateField_management("Duct_Copy", "OriginalOID", "!OBJECTID!", "PYTHON3")
        else:
            pass

        fields_duct = ("OriginalOID", "SHAPE@XY")
        arr_duct = arcpy.da.FeatureClassToNumPyArray("Duct_Copy", fields_duct, skip_nulls=True,
                                                     explode_to_points=True)
        arcpy.AddMessage("arr_duct")
        arcpy.AddMessage(str(arr_duct))

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase
        arcpy.AddMessage(str(p1))
        p2 = p1 + '\\' + "duct_start"
        p3 = p1 + '\\' + "duct_end"
        arcpy.AddMessage(str(p2))

        def remove_dup_by_first_element(list1):
            first_element_list = []
            list2 = []
            for x in list1:
                if x[0] not in first_element_list:
                    list2.append(x)
                    first_element_list.append(x[0])
            return list2

        arr_duct_start = remove_dup_by_first_element(arr_duct)
        # arcpy.AddMessage("arr_duct_start")
        # arcpy.AddMessage(arr_duct_start)

        arr_duct_end1 = arr_duct
        arr_duct_end2 = arr_duct_end1[::-1]
        arr_duct_end2 = remove_dup_by_first_element(arr_duct_end2)
        arr_duct_end = arr_duct_end2[::-1]
        # arcpy.AddMessage("arr_duct_end")
        # arcpy.AddMessage(arr_duct_end)

        arr_duct_start_numpy = np.array(arr_duct_start)
        arcpy.AddMessage("arr_duct_start_numpy")
        arcpy.AddMessage(arr_duct_start_numpy)

        arr_duct_end_numpy = np.array(arr_duct_end)
        arcpy.AddMessage("arr_duct_end_numpy")
        arcpy.AddMessage(arr_duct_end_numpy)

        if not arcpy.Exists(p2):
            arcpy.da.NumPyArrayToFeatureClass(arr_duct_start_numpy, p2, ("SHAPE@XY"), SR)
        if not arcpy.Exists(p3):
            arcpy.da.NumPyArrayToFeatureClass(arr_duct_end_numpy, p3, ("SHAPE@XY"), SR)

        arcpy.Merge_management(["duct_start", "duct_end"], "both_ends_duct_copy")

        arcpy.analysis.SpatialJoin("both_ends_duct_copy", "merged_layers", "SJ_duct_merged", "JOIN_ONE_TO_ONE",
                                   "KEEP_ALL",
                                   "", "INTERSECT", "", "")

        try:
            if arcpy.Exists("SJ_duct_merged"):
                lstFields_sc = arcpy.ListFields("SJ_duct_merged")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in SJ_duct_merged..")
                    arcpy.AddField_management("SJ_duct_merged", "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        with arcpy.da.UpdateCursor("SJ_duct_merged", ("Join_Count", "Error")) as cursor:
            for rows in cursor:
                if rows[0] == 0:
                    rows[1] = "Duct not snapped"
                    cursor.updateRow(rows)
            del cursor

        array = arcpy.da.FeatureClassToNumPyArray("SJ_duct_merged", ["OriginalOID", "Error"], skip_nulls=True)
        arcpy.AddMessage("array")
        arcpy.AddMessage(array)
        array1 = self.remove_duplicates_None_values(array)
        arcpy.AddMessage("array1")
        arcpy.AddMessage(array1)

        try:
            if arcpy.Exists(self.Output_name2):
                lstFields_sc = arcpy.ListFields(self.Output_name2)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name2, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name2.. :  " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final Log File- 1.2 ")
            for x in array1:
                with arcpy.da.UpdateCursor(self.Output_name2, ("OBJECTID", "Error")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            if row[1] != None:
                                arcpy.AddMessage(str(row[1]))
                                row[1] = str(row[1]) + "," + x[1]
                            elif row[1] == None:
                                row[1] = x[1]
                                print(x[1])
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final  Log File- 1.2:  " + str(e))

    def __del__(self):
        #### Object Remove Here ######

        if arcpy.Exists("Duct_Copy"):
            arcpy.Delete_management("Duct_Copy")
        if arcpy.Exists("olt_copy"):
            arcpy.Delete_management("olt_copy")
        if arcpy.Exists("Duct_Copy"):
            arcpy.Delete_management("Duct_Copy")
        if arcpy.Exists("Pole_Copy"):
            arcpy.Delete_management("Pole_Copy")
        if arcpy.Exists("Chamber_Copy"):
            arcpy.Delete_management("Chamber_Copy")
        if arcpy.Exists("Customer_Building_Copy"):
            arcpy.Delete_management("Customer_Building_Copy")
        if arcpy.Exists("SJ_duct_merged"):
            arcpy.Delete_management("SJ_duct_merged")
        if arcpy.Exists("merged_layers"):
            arcpy.Delete_management("merged_layers")
        if arcpy.Exists("both_ends_duct_copy"):
            arcpy.Delete_management("both_ends_duct_copy")
        if arcpy.Exists("duct_start"):
            arcpy.Delete_management("duct_start")
        if arcpy.Exists("duct_end"):
            arcpy.Delete_management("duct_end")

class ParentItem3(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 1.3')

        arcpy.CopyFeatures_management(Span, "span_copy")
        arcpy.CalculateField_management("span_copy", "OriginalFID_span", "!OBJECTID!", "PYTHON3")

        arcpy.CopyFeatures_management(Fibre_duct, "fibre_duct_copy")
        arcpy.CopyFeatures_management(Customer_Building, "building_copy")


        if not arcpy.Exists(self.Output_name):
            arcpy.CopyFeatures_management(Span, self.Output_name)


        try:
            arcpy.SpatialJoin_analysis("span_copy", "fibre_duct_copy", "span_duct_joined", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="SHARE_A_LINE_SEGMENT_WITH")
            arcpy.SpatialJoin_analysis("span_duct_joined", "building_copy", "span_duct_building_joined",
                                       join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL",
                                       match_option="INTERSECT")
        except Exception as ex:
            arcpy.AddMessage(ex)

        try:
            if arcpy.Exists("span_duct_building_joined"):
                lstFields_sc = arcpy.ListFields("span_duct_building_joined")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in span_duct_building_joined..")
                    arcpy.AddField_management("span_duct_building_joined", "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to span_duct_building_joined.. :  " + str(e))


        try:
            with arcpy.da.UpdateCursor("span_duct_building_joined", ("Join_Count", "Join_Count_1" , "span_type", "Error")) as cursor:
                for rows in cursor:
                    if rows[0] == 0 and rows[1] == 0 and rows[2] == 2:
                        # arcpy.AddMessage(str(rows[0]))
                        rows[3] = "Underground span doesn't have a drop"

                        cursor.updateRow(rows)
                del cursor
        except Exception as ex:
            arcpy.AddMessage("Issue in writing (Error) to span_duct_building_joined.. :  " + str(ex))



        array = arcpy.da.FeatureClassToNumPyArray("span_duct_building_joined", ["OriginalFID_span", "Error"], skip_nulls=True)

        try:
            if arcpy.Exists(self.Output_name):
                lstFields_sc = arcpy.ListFields(self.Output_name)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error", "TEXT",100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))


        try:
            arcpy.AddMessage("Updating errors in final Log File- 1.3 ")
            for x in array:
                with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Error")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            if row[1] != None:
                                arcpy.AddMessage(str(row[1]))
                                row[1] = str(row[1]) + "," + x[1]
                            elif row[1] == None:
                                row[1] = x[1]
                                print(x[1])
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final  Log File- 1.3:  " + str(e))


    def __del__(self):
        #### Object Remove Here ######
        if arcpy.Exists("Span_copy"):
            arcpy.Delete_management("Span_copy")
        if arcpy.Exists("fibre_duct_copy"):
            arcpy.Delete_management("fibre_duct_copy")
        if arcpy.Exists("span_duct_joined"):
            arcpy.Delete_management("span_duct_joined")
        if arcpy.Exists("span_duct_building_joined"):
            arcpy.Delete_management("span_duct_building_joined")
        if arcpy.Exists("building_copy"):
            arcpy.Delete_management("building_copy")


class ParentItem4(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name3 = name



    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 1.4')

        if not arcpy.Exists(self.Output_name3):
            arcpy.CopyFeatures_management(Customer_Building, self.Output_name3)

        if not arcpy.Exists("Customer_Building_Copy"):
            arcpy.CopyFeatures_management(Customer_Building, "Customer_Building_Copy")

        if not arcpy.Exists('Fibre_Drop_copy'):
            arcpy.CopyFeatures_management(Fibre_Drop, 'Fibre_Drop_copy')


        arcpy.SpatialJoin_analysis("Customer_Building_Copy", "Fibre_Drop_copy", "SJ_Building_drop", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="INTERSECT")

        try:
            if arcpy.Exists("SJ_Building_drop"):
                lstFields_sc = arcpy.ListFields("SJ_Building_drop")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in SJ_Building_drop..")
                    arcpy.AddField_management("SJ_Building_drop", "Error", "TEXT",100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to SJ_Building_drop.. :  " + str(e))

        try:
            with arcpy.da.UpdateCursor("SJ_Building_drop", ("Join_Count", "Error")) as cursor:
                for rows in cursor:
                    if rows[0] == 0:
                        rows[1] = "House doesn't have a drop"
                        cursor.updateRow(rows)
                del cursor
        except Exception as ex:
            arcpy.AddMessage(ex)

        array1point4 = arcpy.da.FeatureClassToNumPyArray("SJ_Building_drop", ["TARGET_FID", "Error"], skip_nulls=True)
        arcpy.AddMessage("array_1.4")
        arcpy.AddMessage(array1point4)
        array1point4_filtered = self.remove_duplicates_None_values(array1point4)
        arcpy.AddMessage("array1point4_filtered")
        arcpy.AddMessage(array1point4_filtered)

        try:
            if arcpy.Exists(self.Output_name3):
                lstFields_sc = arcpy.ListFields(self.Output_name3)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name3, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final Log File- 1.4 ")
            for x in array1point4_filtered:
                with arcpy.da.UpdateCursor(self.Output_name3, ("OBJECTID", "Error")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            if row[1] != None:
                                arcpy.AddMessage(str(row[1]))
                                row[1] = str(row[1]) + "," + x[1]
                            elif row[1] == None:
                                row[1] = x[1]
                                print(x[1])
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final  Log File- 1.4:  " + str(e))



    def __del__(self):
        #### Object Remove Here ######

        if arcpy.Exists("Fibre_Drop_copy"):
            arcpy.Delete_management("Fibre_Drop_copy")

        if arcpy.Exists("Customer_Building_Copy"):
            arcpy.Delete_management("Customer_Building_Copy")

        if arcpy.Exists("SJ_Building_drop"):
            arcpy.Delete_management("SJ_Building_drop")

class ParentItem5(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name3 = name

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 1.5')

        arcpy.CopyFeatures_management(Customer_Building, "Customer_Building_Copy")
        arcpy.CalculateField_management("Customer_Building_Copy", "OriginalFID_cb", "!OBJECTID!", "PYTHON3")

        if not arcpy.Exists(self.Output_name3):
            arcpy.CopyFeatures_management(Customer_Building, self.Output_name3)

        try:
            if not arcpy.Exists('Span_copy'):
                arcpy.CopyFeatures_management(Span, 'Span_copy')
        except Exception as e:
            arcpy.AddMessage("Issue in creating copies of Fibre_Drop.. :  " + str(e))

        arcpy.SpatialJoin_analysis("Customer_Building_Copy", "Span_copy", "span_building_joined", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", match_option="INTERSECT")

        try:
            if arcpy.Exists("span_building_joined"):
                lstFields_sc = arcpy.ListFields("span_building_joined")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in span_building_joined..")
                    arcpy.AddField_management("span_building_joined", "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to span_building_joined.. :  " + str(e))

        try:
            with arcpy.da.UpdateCursor("span_building_joined", ("Join_Count", "Error")) as cursor:
                for rows in cursor:
                    if rows[0] == 0:
                        rows[1] = "House doesn't have a span"
                        cursor.updateRow(rows)
                del cursor
        except Exception as ex:
            arcpy.AddMessage(ex)


        array1point5 = arcpy.da.FeatureClassToNumPyArray("span_building_joined", ["OriginalFID_cb", "Error"], skip_nulls=True)
        arcpy.AddMessage("array1point5")
        arcpy.AddMessage(array1point5)
        filtered_array1point5 = self.remove_duplicates_None_values(array1point5)
        arcpy.AddMessage("filtered_array1point5")
        arcpy.AddMessage(filtered_array1point5)

        try:
            if arcpy.Exists(self.Output_name3):
                lstFields_sc = arcpy.ListFields(self.Output_name3)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name3..")
                    arcpy.AddField_management(self.Output_name3, "Error", "TEXT",100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name3.. :  " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final Log File- 1.5")
            for x in filtered_array1point5:
                with arcpy.da.UpdateCursor(self.Output_name3, ("OBJECTID", "Error")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            if row[1] != None:
                                arcpy.AddMessage(str(row[1]))
                                row[1] = str(row[1]) + "," + x[1]
                            elif row[1] == None:
                                row[1] = x[1]
                                print(x[1])
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final  Log File- 1.5:  " + str(e))

    def __del__(self):
        #### Object Remove Here ######

        if arcpy.Exists("Span_copy"):
            arcpy.Delete_management("Span_copy")
        if arcpy.Exists("Customer_Building_Copy"):
            arcpy.Delete_management("Customer_Building_Copy")
        if arcpy.Exists("span_building_joined"):
            arcpy.Delete_management("span_building_joined")


class BaseClass(ParentItem1):
    def __init__(self):
        from datetime import datetime

        now = datetime.now()
        start_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run Start's @" + start_time)

        if arcpy.Exists("SpanSnap"):
            arcpy.Delete_management("SpanSnap")

        if arcpy.Exists("DuctSnap"):
            arcpy.Delete_management("DuctSnap")

        if arcpy.Exists("Output"):
            arcpy.Delete_management("Output")

        if arcpy.Exists("HouseSnap"):
            arcpy.Delete_management("HouseSnap")

        ### Object Intialize Here ####
        if item1point1 == 'true':
            ParentItem1.__init__(self, 'SpanSnap')
            ParentItem1.ItemNo(self)
            ParentItem1.__del__(self)
        if item1point3 == 'true':
            ParentItem3.__init__(self, 'SpanSnap')
            ParentItem3.ItemNo(self)
            ParentItem3.__del__(self)

        if item1point2 == 'true':
            ParentItem2.__init__(self, 'DuctSnap')
            ParentItem2.ItemNo(self)
            ParentItem2.__del__(self)

        if item1point4 == 'true':
            ParentItem4.__init__(self, 'HouseSnap')
            ParentItem4.ItemNo(self)
            ParentItem4.__del__(self)
        if item1point5 == 'true':
            ParentItem5.__init__(self, 'HouseSnap')
            ParentItem5.ItemNo(self)
            ParentItem5.__del__(self)

    def remove_duplicates_None_values(self, array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[1] != 'None':
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1

    def remove_duplicates_zero_values(array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[2] != 0:
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1

    def largest(self, arr, n):

        # Initialize maximum element
        max = arr[1]
        # items = arr[0]
        # Traverse array elements from second
        # and compare every element with
        # current max
        # for items[0] in arr:
        for i in range(1, n):
            if arr[i] > max:
                max = arr[i]
        return max

    def largest(self, arr, n):

        # Initialize maximum element
        max = arr[0]

        # Traverse array elements from second
        # and compare every element with
        # current max
        for i in range(1, n):
            if arr[i] > max:
                max = arr[i]
        return max

    def get_only_maximum_length(self, array, id):
        array_length = []
        array_id = []
        i = 0
        if len(array) != 0:
            for data in array:
                if len(array_id) == 0 and id in data:
                    array_id.append(data[0])
                    array_length.append(data[1])
                elif id in data:
                    array_id.append(data[0])
                    array_length.append(data[1])

            n = len(array_length)
            i = self.largest(array_length,n)
            #array_id.pop(i)
        return i

    def __del__(self):
        ### Object Remove Here ######

        if item1point1 == 'true' or item1point3 == 'true':
            fieldList = arcpy.ListFields(self.Output_name)
            fieldsToBeDeleted = []
            for field in fieldList:
                if str(field.name) not in (["OBJECTID", "Shape", "name", "Error", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area",
                         "SHAPE_Area"]):
                    fieldsToBeDeleted.append(field.name)

            arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))

            try:
                with arcpy.da.UpdateCursor(self.Output_name, ("Error")) as cursor:
                    for row in cursor:
                        if row[0] == None:
                            cursor.deleteRow()
                    del cursor
            except Exception as ex:
                arcpy.AddMessage(ex)

            try:
                if arcpy.Exists(self.Output_name):
                    arcpy.DeleteField_management(self.Output_name, fieldsToBeDeleted)
            except Exception as e:
                print(e)
            except arcpy.ExecuteError as ex:
                print(ex)
            arcpy.AddMessage("Finished Deleting unwanted fields of error ")
            arcpy.AddMessage(str(self.Output_name) + " - Log File has been Generated")

            from datetime import datetime

            now = datetime.now()
            end_time = now.strftime("%H:%M:%S")

            arcpy.AddMessage("Script Run End's @" + end_time)

    ##########################################################################################################################################################
        if item1point2 == 'true':

            fieldList = arcpy.ListFields(self.Output_name2)
            fieldsToBeDeleted = []
            for field in fieldList:
                if str(field.name) not in (["OBJECTID", "Shape", "name", "Error", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area",
                         "SHAPE_Area"]):
                    fieldsToBeDeleted.append(field.name)

            arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))

            try:
                with arcpy.da.UpdateCursor(self.Output_name2, ("Error")) as cursor:
                    for row in cursor:
                        if row[0] == None:
                            cursor.deleteRow()
                    del cursor
            except Exception as ex:
                arcpy.AddMessage(ex)

            try:
                if arcpy.Exists(self.Output_name2):
                    arcpy.DeleteField_management(self.Output_name2, fieldsToBeDeleted)
            except Exception as e:
                print(e)
            except arcpy.ExecuteError as ex:
                print(ex)
            arcpy.AddMessage("Finished Deleting unwanted fields of error ")
            arcpy.AddMessage(str(self.Output_name2) + " - Log File has been Generated")

            from datetime import datetime

            now = datetime.now()
            end_time = now.strftime("%H:%M:%S")

            arcpy.AddMessage("Script Run End's @" + end_time)
    ###############################################################################################################################################################
        if item1point4 == 'true' or item1point5 == 'true':
            fieldList = arcpy.ListFields(self.Output_name3)
            fieldsToBeDeleted = []
            for field in fieldList:
                if str(field.name) not in (["OBJECTID", "Shape", "name", "Error", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area",
                         "SHAPE_Area"]):
                    fieldsToBeDeleted.append(field.name)

            arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))

            try:
                with arcpy.da.UpdateCursor(self.Output_name3, ("Error")) as cursor:
                    for row in cursor:
                        if row[0] == None:
                            cursor.deleteRow()
                    del cursor
            except Exception as ex:
                arcpy.AddMessage(ex)

            try:
                if arcpy.Exists(self.Output_name3):
                    arcpy.DeleteField_management(self.Output_name3, fieldsToBeDeleted)
            except Exception as e:
                print(e)
            except arcpy.ExecuteError as ex:
                print(ex)
            arcpy.AddMessage("Finished Deleting unwanted fields of error ")
            arcpy.AddMessage(str(self.Output_name3) + " - Log File has been Generated")

        from datetime import datetime

        now = datetime.now()
        end_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run End's @" + end_time)



######### Get All Input Parameter ###########

arcpy.env.overwriteOutput = True

input_geodatabase = arcpy.GetParameterAsText(0)

Span = arcpy.GetParameterAsText(1)
Customer_Building = arcpy.GetParameterAsText(2)
Pole = arcpy.GetParameterAsText(3)
Chamber = arcpy.GetParameterAsText(4)
Fibre_duct = arcpy.GetParameterAsText(5)
Fibre_Drop = arcpy.GetParameterAsText(6)
Olt = arcpy.GetParameterAsText(7)

######## Checked Input #########

item1point1 = arcpy.GetParameterAsText(8)
item1point2 = arcpy.GetParameterAsText(9)
item1point3 = arcpy.GetParameterAsText(10)
item1point4 = arcpy.GetParameterAsText(11)
item1point5 = arcpy.GetParameterAsText(12)

######### Oject Creation #######
# main function
if __name__ == '__main__':
    if arcpy.Exists("SpanSnap"):
        arcpy.Delete_management("SpanSnap")

    if arcpy.Exists("DuctSnap"):
        arcpy.Delete_management("DuctSnap")

    if arcpy.Exists("HouseSnap"):
        arcpy.Delete_management("HouseSnap")
    # created the object for BaseClass
    ob = BaseClass()
