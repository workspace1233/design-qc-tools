import arcpy

arcpy.env.overwriteOutput = True

class ParentItem1(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name
        self.temp_file = 'temp_file'
        self.spatial_join_boundaries = 'spatial_join_boundaries'
        #self.hide_fileds(Customer_Building, ['Shape', 'Shape_Length', 'Shape_area'])

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 2.1')

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase

        p2 = p1 + "\\" + self.Output_name

        if not arcpy.Exists(self.Output_name):
            if not arcpy.Exists(self.Output_name):
                feats = arcpy.FeatureSet(table=Boundaries)
                feats.save(p2)
        arcpy.AddMessage("Started Checking: Does the number of homes (sum of all unit_counts) match the premises count?")

        arcpy.SpatialJoin_analysis(Boundaries, Boundaries,
                                   self.spatial_join_boundaries, "JOIN_ONE_TO_ONE",
                                   "KEEP_ALL", "", "ARE_IDENTICAL_TO")

        try:
            arcpy.analysis.SummarizeWithin(self.spatial_join_boundaries, Customer_Building, self.temp_file, keep_all_polygons='KEEP_ALL', sum_fields=[['temp_hhp_tot', 'SUM']])
            arcpy.management.AddField(self.temp_file, "Warning", 'TEXT', field_length='500', field_is_nullable='NULLABLE')
            arcpy.management.AddField(self.temp_file, "Correct_Premise_Count", 'Long', field_length='50', field_is_nullable='NULLABLE')
        except Exception as e:
            print(e)
        except arcpy.ExecuteError as ex:
            print(ex)

        #arcpy.AddMessage(self.array)
        with arcpy.da.UpdateCursor(self.temp_file, ('homes', 'Warning', 'SUM_temp_hhp_tot')) as cursor:
            for row in cursor:
                if row[0] != None:
                    if int(row[0]) != int(row[2]):
                        row[1] = "Total number of premises within Boundary not matching the attribute"
                        cursor.updateRow(row)

            del cursor
        with arcpy.da.UpdateCursor(self.temp_file, ('homes', 'Warning')) as cursor:
            for row in cursor:
                if row[0] == None:
                    row[1] = "Null Case: Total number of premises within Boundary not matching the attribute"
                    cursor.updateRow(row)
            del cursor

        self.array = arcpy.da.FeatureClassToNumPyArray(self.temp_file, ["SUM_temp_hhp_tot", "TARGET_FID", "Warning"], skip_nulls=True)

        arcpy.AddMessage(self.array)

        try:
            arcpy.management.AddField(self.Output_name, "Warning", 'TEXT', field_length='500',field_is_nullable='NULLABLE')
            arcpy.management.AddField(self.Output_name, "Correct_Premise_Count", 'Long', field_length='50', field_is_nullable='NULLABLE')


            arcpy.management.AddField(self.Output_name, "Error_1", 'TEXT', field_length='500',
                                      field_is_nullable='NULLABLE')
            arcpy.management.AddField(self.Output_name, "Warning_1", 'TEXT', field_length='500', field_is_nullable='NULLABLE')
            arcpy.management.AddField(self.Output_name, "Premise_Number", 'TEXT', field_length='500', field_is_nullable='NULLABLE')
            arcpy.management.AddField(self.Output_name, "Correct_Premise_Count_1", 'Long', field_length='500',field_is_nullable='NULLABLE')
        except Exception as e:
            print(e)
        except arcpy.ExecuteError as ex:
            print(ex)

        for items in self.array:
            with arcpy.da.UpdateCursor(self.Output_name, ('homes', 'Warning', 'OBJECTID', 'Correct_Premise_Count', 'Warning_1', 'Correct_Premise_Count_1', 'Premise_Number')) as cursor:
                for row in cursor:
                    if str(row[2]) == str(items[1]):
                        if row[0] != None:
                            premise = str(row[0])
                        else:
                            premise = None
                        if row[1] != None:
                            msg = row[1] + ',' + items[2]
                            val = int(items[0])
                            row[1] = msg
                            row[4] = msg
                            row[3] = val
                            row[5] = val
                            row[6] = premise
                        elif row[1] == None and str(items[2]) != 'None':
                            msg = items[2]
                            val = int(items[0])
                            row[1] = msg
                            row[4] = msg
                            row[3] = val
                            row[5] = val
                            row[6] = premise
                        cursor.updateRow(row)
                del cursor

        arcpy.AddMessage(
            "Finished Checking: Does the number of homes (sum of all unit_counts) match the premises count?")


    def __del__(self):
        #### Object Remove Here ######
        # pass
        if arcpy.Exists(self.temp_file):
            arcpy.Delete_management(self.temp_file)
        if arcpy.Exists(self.spatial_join_boundaries):
            arcpy.Delete_management(self.spatial_join_boundaries)

class ParentItem2(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name
        self.temp_file_2 = 'temp_file_2'
        self.primary_boundary = 'primary_boundary'
        self.spatial_join_boundaries = 'spatial_join_boundaries'

    def ItemNo(self):
        arcpy.AddMessage('Tool Check For Item No 2.2')
        lstFields_bldng = arcpy.ListFields(Customer_Building)
        field_names_bldng = [f.name for f in lstFields_bldng]

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase

        p2 = p1 + "\\" + self.Output_name

        if not arcpy.Exists(self.Output_name):
            if not arcpy.Exists(self.Output_name):
                feats = arcpy.FeatureSet(table=Boundaries)
                feats.save(p2)

        if "spl_ratio" in field_names_bldng:

            arcpy.SpatialJoin_analysis(Boundaries, Boundaries,
                                       self.spatial_join_boundaries, "JOIN_ONE_TO_ONE",
                                       "KEEP_ALL", "", "ARE_IDENTICAL_TO")

            try:
                arcpy.analysis.SummarizeWithin(self.spatial_join_boundaries, Customer_Building, self.temp_file_2, keep_all_polygons='KEEP_ALL', sum_fields=[['temp_hhp_tot', 'SUM'], ['spl_ratio', 'MAX']])
                # self.array = arcpy.da.FeatureClassToNumPyArray(self.temp_file_2, ["SUM_temp_hhp_tot", "OBJECTID"])
            except Exception as e:
                print(e)
            except arcpy.ExecuteError as ex:
                print(ex)

            lstFields_temp = arcpy.ListFields(self.temp_file_2)
            lstFields_out = arcpy.ListFields(self.Output_name)
            field_names_temp = [f.name for f in lstFields_temp]
            field_names_outp = [f.name for f in lstFields_out]

            try:
                arcpy.AddField_management(self.temp_file_2, "Error", "TEXT", "100")
                arcpy.AddField_management(self.Output_name, "Error", "TEXT", "100")
                if "Correct_Premise_Count" not in field_names_outp:
                    arcpy.management.AddField(self.Output_name, "Correct_Premise_Count", 'Long', field_length='50', field_is_nullable='NULLABLE')
                if "Correct_Premise_Count" not in field_names_temp:
                    arcpy.management.AddField(self.temp_file_2, "Correct_Premise_Count", 'Long', field_length='50',
                                              field_is_nullable='NULLABLE')
                if "Error_1" not in field_names_outp:
                    arcpy.management.AddField(self.Output_name, "Error_1", 'TEXT', field_length='500',
                                          field_is_nullable='NULLABLE')
                if "Warning_1" not in field_names_outp:
                    arcpy.management.AddField(self.Output_name, "Warning_1", 'TEXT', field_length='50',
                                              field_is_nullable='NULLABLE')
                if "Premise_Number" not in field_names_outp:
                    arcpy.management.AddField(self.Output_name, "Premise_Number", 'Long', field_length='50',
                                              field_is_nullable='NULLABLE')
                if "Correct_Premise_Count_1" not in field_names_outp:
                    arcpy.management.AddField(self.Output_name, "Correct_Premise_Count_1", 'Long', field_length='500',
                                              field_is_nullable='NULLABLE')

            except Exception as e:
                print(e)
            except arcpy.ExecuteError as ex:
                print(ex)

            arcpy.AddMessage("Started Checking: Are there too many premises in primary boundaries (level=2)? ")

            try:
                # for items in self.array:
                with arcpy.da.UpdateCursor(self.temp_file_2, ("MAX_spl_ratio", "Sum_temp_hhp_tot", "Error", "level_", "OBJECTID", "Correct_Premise_Count")) as cursor:
                    for data in cursor:
                        # print(data)
                        if data[3] == 2:
                            if data[0] == 1 and data[1] >= 27:
                                arcpy.AddMessage(str(data[4]))
                                data[2] = "More than 27  premises in a 1:32 split ratio"
                                data[5] = 25
                            elif data[0] == 2 and data[1] >= 14:
                                data[2] = "More than 14  premises in a 1:16 split ratio"
                                data[5] = 16
                            elif data[0] == 3 and data[1] >= 7:
                                data[2] = "More than 7  premises in a 1:8 split ratio"
                                data[5] = 6
                            elif data[0] == 4 and data[1] > 1:
                                data[2] = "More than 1 premises in a 1:1 split ratio"
                                data[5] = 1
                        cursor.updateRow(data)
                    del cursor
            except Exception as e:
                print(e)
            except arcpy.ExecuteError as ex:
                print(ex)

            arcpy.AddMessage("Finished Checking: Are there too many premises in primary boundaries (level=2)? ")

            array_records = arcpy.da.FeatureClassToNumPyArray(self.temp_file_2, ["TARGET_FID", "Error", "Correct_Premise_Count"], skip_nulls=True)
            arcpy.AddMessage(array_records)

            try:
                arcpy.AddMessage("Updating errors in final Log File- 2.2 ")
                for x in array_records:
                    with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Error", "Error_1")) as cursor:
                        for row in cursor:
                            if str(row[0]) == str(x[0]):
                                if row[1] != None:
                                    arcpy.AddMessage(str(row[1]))
                                    row[1] = str(row[1]) + "," + x[1]
                                elif row[1] == None:
                                    row[1] = x[1]
                                if row[2] != None:
                                    arcpy.AddMessage(str(row[2]))
                                    row[2] = str(row[2]) + "," + x[1]
                                elif row[2] == None:
                                    row[2] = x[1]
                            else:
                                pass
                            cursor.updateRow(row)
                        del cursor
            except Exception as e:
                arcpy.AddMessage("Issue in Updating errors in final  Log File- 2.2:  " + str(e))
        else:
            arcpy.AddMessage("Item 2.2 did not work as spl_ratio filed is missing from customer building.")


    def __del__(self):
        #### Object Remove Here ######
        # pass
        if arcpy.Exists(self.temp_file_2):
            arcpy.Delete_management(self.temp_file_2)
        if arcpy.Exists(self.spatial_join_boundaries):
            arcpy.Delete_management(self.spatial_join_boundaries)
# class ParentItem3(object):
#     def __init__(self, name):
#         arcpy.AddMessage("entered successfully inside ParentItem3 - 2.3")
#         self.Output_name = name
#         self.summarize_within_boundary_customer_buildings = 'summarize_within_secondary_boundary_customer_buildings'
#
#     def ItemNo(self):
#         arcpy.AddMessage('Tool Check Run For item 2.3')
#         try:
#             if not arcpy.Exists(self.Output_name):
#                 arcpy.CopyFeatures_management(Boundaries, self.Output_name)
#             else:
#                 pass
#         except Exception as e:
#             arcpy.AddMessage("Issue in Deleting previous Output ... :  " + str(e))
#
#         try:
#             arcpy.analysis.SummarizeWithin(Boundaries, Customer_Building, self.summarize_within_boundary_customer_buildings, keep_all_polygons='KEEP_ALL',
#                                            sum_fields=[['temp_hhp_tot', 'SUM'], ['spl_ratio', 'MAX']])
#             self.array = arcpy.da.FeatureClassToNumPyArray(self.summarize_within_boundary_customer_buildings, ["SUM_temp_hhp_tot", "name"])
#         except Exception as e:
#             print(e)
#         except arcpy.ExecuteError as ex:
#             print(ex)
#
#         try:
#             arcpy.AddField_management(self.summarize_within_boundary_customer_buildings, "Error", "TEXT", "100")
#         except Exception as e:
#             print(e)
#         except arcpy.ExecuteError as ex:
#             print(ex)
#
#         arcpy.AddMessage("Item 2.3: Started Checking: Are there too many premises in primary boundaries (level=2)? ")
#
#         try:
#             for items in self.array:
#                 with arcpy.da.UpdateCursor(self.summarize_within_boundary_customer_buildings,
#                                            ("MAX_spl_ratio", "Sum_temp_hhp_tot", "Error", "level_", "name")) as cursor:
#                     for data in cursor:
#                         # print(data)
#                         if data[3] == 3:
#                             if data[0] == 1 and data[1] >= 25 and items[1] == data[4]:
#                                 data[2] = "More premises than secondary splitter ports"
#                             if data[0] == 2 and data[1] >= 13 and items[1] == data[4]:
#                                 data[2] = "More premises than secondary splitter ports"
#                             if data[0] == 3 and data[1] >= 6 and items[1] == data[4]:
#                                 data[2] = "More premises than secondary splitter ports"
#                             if data[0] == 4 and data[1] > 1 and items[1] == data[4]:
#                                 data[2] = "More premises than secondary splitter ports"
#                         cursor.updateRow(data)
#                     del cursor
#         except Exception as e:
#             print(e)
#         except arcpy.ExecuteError as ex:
#             print(ex)
#
#         arcpy.AddMessage("Item 2.3: Finished Checking: Are there too many premises in primary boundaries (level=2)? ")
#
#         try:
#             arcpy.AddMessage("Creating final numpy arrays for error updating.. ")
#             self.array_final_2point3 = arcpy.da.FeatureClassToNumPyArray(self.summarize_within_boundary_customer_buildings, ["name","Error"], skip_nulls = True)
#             arcpy.AddMessage(self.array_final_2point3)
#         except Exception as e:
#             arcpy.AddMessage("Issue in Creating final numpy arrays for error updating.. .. :  " + str(e))
#
#         try:
#             arcpy.AddMessage("Filtering numpy array")
#             self.filtered_array_final_2point3 = self.remove_duplicates_None_values(self.array_final_2point3)
#             arcpy.AddMessage(self.filtered_array_final_2point3)
#         except Exception as e:
#             arcpy.AddMessage("Issue in Filtering numpy array .. :  " + str(e))
#
#         try:
#             if arcpy.Exists(self.Output_name):
#                 lstFields_sc = arcpy.ListFields(self.Output_name)
#                 field_names_sc = [f.name for f in lstFields_sc]
#                 if "Error" not in field_names_sc:
#                     arcpy.AddField_management(self.Output_name, "Error", "TEXT", 100)
#                 else:
#                     pass
#         except Exception as e:
#             arcpy.AddMessage("Issue in adding field Error to customer_buildings_copy... :  " + str(e))
#
#         try:
#             arcpy.AddMessage("Updating errors in final primary boundary Log File- PremiseBoundary ")
#             for x in self.filtered_array_final_2point3:
#                 with arcpy.da.UpdateCursor(self.Output_name, ("name", "Error")) as cursor:
#                     for row in cursor:
#                         if row[0] == x[0]:
#                             arcpy.AddMessage(str(row[0]))
#                             if row[1] != None:
#                                 row[1] = str(row[1]) + "," + x[1]
#                                 print(x[1])
#                             elif row[1] == None:
#                                 row[1] = x[1]
#                                 print(x[1])
#                         else:
#                             pass
#                         cursor.updateRow(row)
#                     del cursor
#         except Exception as e:
#             arcpy.AddMessage(
#                 "Issue in Updating errors in final primary boundary Log File- PremiseBoundary :  " + str(e))
#
#     def __del__(self):
#         try:
#             arcpy.AddMessage("Deleting intermediate layers..")
#             if arcpy.Exists(self.summarize_within_boundary_customer_buildings):
#                 arcpy.Delete_management(self.summarize_within_boundary_customer_buildings)
#         except Exception as e:
#             arcpy.AddMessage("Issue in Deleting intermediate layers.. :  " + str(e))

class ParentItem7(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name
        self.primary_boundary = 'primary_boundary'
        self.spatial_join_primary_boundary_customer_buildings = 'spatial_join_primary_boundary_customer_buildings'

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 2.7')

        lstFields_bldng = arcpy.ListFields(Customer_Building)
        field_names_bldng = [f.name for f in lstFields_bldng]

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase

        if "spl_ratio" in field_names_bldng:
            p2 = p1 + "\\" + self.Output_name

            if not arcpy.Exists(self.Output_name):
                if not arcpy.Exists(self.Output_name):
                    feats = arcpy.FeatureSet(table=Boundaries)
                    feats.save(p2)

                lstFields_sc = arcpy.ListFields(self.Output_name)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error_1" not in field_names_sc:
                    arcpy.management.AddField(self.Output_name, "Error_1", 'TEXT', field_length='500',
                                              field_is_nullable='NULLABLE')
                if "Error" not in field_names_sc:
                    arcpy.AddField_management(self.Output_name, "Error", "TEXT", 100)
                else:
                    pass

            arcpy.AddMessage("Item 2.7: Started Checking: Have all the buildings within the boundary  same split ratio?")

            # try:
            #     arcpy.Select_analysis(Boundaries, self.primary_boundary, "level_ in (2)")
            #     arcpy.AddMessage("Selecting Primary Boundary ")
            # except Exception as e:
            #     arcpy.AddMessage("Issue in Selecting Primary Boundary : Line 24 " + str(e))

            try:
                arcpy.AddMessage("Spatial Join Primary Boundaries and Customer Buildings ..")
                arcpy.SpatialJoin_analysis(Boundaries, Customer_Building, self.spatial_join_primary_boundary_customer_buildings, "JOIN_ONE_TO_MANY", "KEEP_ALL", "", "INTERSECT")
            except Exception as e:
                arcpy.AddMessage("Issue in Spatial Join Primary Boundaries and Customer Buildings ..: " + str(e))

            try:
                arcpy.AddMessage("Removing others not primary ")
                with arcpy.da.UpdateCursor(self.spatial_join_primary_boundary_customer_buildings,
                                           ("level_")) as cursor:
                    for data in cursor:
                        if data[0] != None:
                            if int(data[0]) != 2:
                                cursor.deleteRow()
                    del cursor
            except Exception as e:
                arcpy.AddMessage("Issue in Removing others not primary  : " + str(e))

            try:
                self.arr_spatial_join_primary_boundary_customer_buildings = arcpy.da.FeatureClassToNumPyArray(self.spatial_join_primary_boundary_customer_buildings, ["TARGET_FID", "Spl_ratio"], skip_nulls=True)
                self.filtered_arr_spatial_join_primary_boundary_customer_buildings = self.remove_duplicates_None_values(self.arr_spatial_join_primary_boundary_customer_buildings)
                arcpy.AddMessage(self.filtered_arr_spatial_join_primary_boundary_customer_buildings)
                arcpy.AddMessage("Removing duplicate boundary names and errorless values from arrays ..")
            except Exception as e:
                arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))

            try:
                if arcpy.Exists(self.spatial_join_primary_boundary_customer_buildings):
                    lstFields_sc = arcpy.ListFields(self.spatial_join_primary_boundary_customer_buildings)
                    field_names_sc = [f.name for f in lstFields_sc]
                    if "Error" not in field_names_sc:
                        arcpy.AddField_management(self.spatial_join_primary_boundary_customer_buildings, "Error", "TEXT",100)
                    else:
                        pass
            except Exception as e:
                arcpy.AddMessage("Issue in adding field Error to output_spatial_join_boundaries_splice_closure :  " + str(e))

            try:
                arcpy.AddMessage("CALCULATING ERRORs ")
                with arcpy.da.UpdateCursor(self.spatial_join_primary_boundary_customer_buildings,("TARGET_FID", "Spl_ratio", "Error")) as cursor:
                    for data in cursor:
                        for items in self.filtered_arr_spatial_join_primary_boundary_customer_buildings:
                            if str(data[0]) == str(items[0]):
                                if data[1] != items[1]:
                                    data[2] = "Buildings within the ODP1 boundary does not have same split ratio "
                                cursor.updateRow(data)
                    del cursor
            except Exception as e:
                arcpy.AddMessage("Issue in CALCULATING ERRORs  : " + str(e))

            try:
                self.arr_spatial_join_primary_boundary_customer_buildings2 = arcpy.da.FeatureClassToNumPyArray(self.spatial_join_primary_boundary_customer_buildings, ["TARGET_FID", "Error"], skip_nulls=True)
                # print(arr_spatial_join_primary_boundary_customer_buildings2)
                self.filtered_arr_spatial_join_primary_boundary_customer_buildings2 = self.remove_duplicates_None_values(self.arr_spatial_join_primary_boundary_customer_buildings2)
                arcpy.AddMessage(self.filtered_arr_spatial_join_primary_boundary_customer_buildings2)
                arcpy.AddMessage("Removing duplicate boundary names and errorless values from arrays ..")
            except Exception as e:
                arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))

            # try:
            arcpy.AddMessage("Updating errors in final Log File- 2.7 ")
            for x in self.filtered_arr_spatial_join_primary_boundary_customer_buildings2 :
                with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Error", "Error_1")) as cursor:
                    for row in cursor:
                        if int(row[0]) == int(x[0]):
                            arcpy.AddMessage(str(row[0]) + " matched")
                            if row[1] != None:
                                arcpy.AddMessage(str(row[0]))
                                row[1] = str(row[1]) + "," + x[1]
                            if row[1] == None:
                                row[1] = x[1]
                                print(x[1])
                            if row[2] != None:
                                arcpy.AddMessage(str(row[0]))
                                row[2] = str(row[2]) + "," + x[1]
                            if row[2] == None:
                                row[2] = x[1]
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
            # except Exception as e:
            #     arcpy.AddMessage("Issue in Updating errors in final  Log File- 2.7:  " + str(e))
        else:
            arcpy.AddMessage("Item 2.7 did not work as spl_ratio filed is missing from customer building.")


    def __del__(self):
        try:
            if arcpy.Exists(self.spatial_join_primary_boundary_customer_buildings):
                arcpy.Delete_management(self.spatial_join_primary_boundary_customer_buildings)
        except Exception as e:
            arcpy.AddMessage("Issue in deleting intermediate layers :  " + str(e))

class BaseClass(ParentItem1, ParentItem2, ParentItem7):
    def __init__(self):
        from datetime import datetime

        now = datetime.now()
        start_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run Start's @" + start_time)

        if arcpy.Exists("BoundaryPremise"):
            arcpy.Delete_management("BoundaryPremise")

        ### Object Intialize Here ####
        if item2point1 == 'true':
            ParentItem1.__init__(self, 'BoundaryPremise')
            ParentItem1.ItemNo(self)
            ParentItem1.__del__(self)
        if item2point2 == 'true':
            ParentItem2.__init__(self, 'BoundaryPremise')
            ParentItem2.ItemNo(self)
            ParentItem2.__del__(self)

        if item2point7 == 'true':
            ParentItem7.__init__(self, 'BoundaryPremise')
            ParentItem7.ItemNo(self)
            ParentItem7.__del__(self)



    def remove_duplicates_None_values(self, array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[1] != 'None':
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1

    def remove_duplicates_zero_values(array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[2] != 0:
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1


    def __del__(self):
        #### Object Remove Here ######

        if item2point1 == 'true' or item2point2 == 'true'  or item2point7 == 'true':
            arcpy.AddMessage("IN IF")
            fieldList = arcpy.ListFields(self.Output_name)
            fieldsToBeDeleted = []
            for field in fieldList:
                if str(field.name) not in (["OBJECTID", "Shape", "name", "Warning_1", "Error_1", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area",
                         "SHAPE_Area", "Correct_Premise_Count_1", "Premise_Number", 'Shape__Area', 'Shape__Length']):
                    fieldsToBeDeleted.append(field.name)

            arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))

            try:
                if item2point1 == 'true' and 'true' != item2point2 and 'true' != item2point7:
                    with arcpy.da.UpdateCursor(self.Output_name, ("Warning")) as cursor:
                        for row in cursor:
                            if row[0] == None:
                                cursor.deleteRow()
                        del cursor
                elif item2point1 != 'true' and 'true' in (item2point2, item2point7):
                    with arcpy.da.UpdateCursor(self.Output_name, ("Error")) as cursor:
                        for row in cursor:
                            if row[0] == None:
                                cursor.deleteRow()
                        del cursor
                else:
                    with arcpy.da.UpdateCursor(self.Output_name, ("Error", "Warning")) as cursor:
                        for row in cursor:
                            if row[0] == None and row[1] == None:
                                cursor.deleteRow()
                        del cursor
            except Exception as ex:
                arcpy.AddMessage(ex)

            try:
                arcpy.DeleteField_management(self.Output_name, fieldsToBeDeleted)
            except Exception as e:
                 arcpy.AddMessage(e)
            except arcpy.ExecuteError as ex:
                 arcpy.AddMessage(ex)

            for fieldName in [f.name for f in arcpy.ListFields(self.Output_name)]:
                if fieldName == "Error_1":
                    arcpy.AlterField_management(self.Output_name, fieldName, "Error", "Error")
                if fieldName == "Warning_1":
                    arcpy.AlterField_management(self.Output_name, fieldName, "Warning", "Warning")
                if fieldName == "Correct_Premise_Count_1":
                    arcpy.AlterField_management(self.Output_name, fieldName, "Correct_Premise_Count", "Correct_Premise_Count")

            

            arcpy.AddMessage("Finished Deleting unwanted fields of error ")
            arcpy.AddMessage(str(self.Output_name) + " - Log File has been Generated")

        from datetime import datetime

        now = datetime.now()
        end_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run End's @" + end_time)


######### Get All Input Parameter ###########

arcpy.env.overwriteOutput = True

input_geodatabase = arcpy.GetParameterAsText(0)
Boundaries = arcpy.GetParameterAsText(1)
Customer_Building = arcpy.GetParameterAsText(2)

######## Checked Input #########

item2point1 = arcpy.GetParameterAsText(3)
item2point2 = arcpy.GetParameterAsText(4)
item2point7 = arcpy.GetParameterAsText(5)

######### Oject Creation #######
# main function
if __name__ == '__main__':
    # created the object for BaseClass
    if arcpy.Exists("BoundaryPremise"):
        arcpy.Delete_management("BoundaryPremise")

    ob = BaseClass()
