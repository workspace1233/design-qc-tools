import arcpy
arcpy.env.overwriteOutput = True
from datetime import datetime
now = datetime.now()
start_time = now.strftime("%H:%M:%S")

# arcpy.env.workspace = r'C:\Users\user\Documents\ArcGIS\Projects\QCtesting2\Waterford_Design_Layers_soumik_version\f73d3823ee3c4881910ce98bb91c46dc.gdb'


class ParentItem1(object):

    def __init__(self, name):
        ### Object Intialize Here ####
        arcpy.AddMessage("entered successfully inside ParentItem1 - 2.4")
        self.customer_buildings_copy = 'PremiseBoundary'
        self.primary_boundary = 'primary_boundary'
        self.buildings_outside_primary_boundary = 'buildings_outside_primary_boundary'

    # primary_boundary = 'primary_boundary'
    # buildings_outside_primary_boundary = 'buildings_outside_primary_boundary'
    # customer_buildings_copy = 'PremiseBoundary'
    ########################################## item 2.4 ##################################################3
    def ItemNo(self):
        arcpy.AddMessage("entered successfully ")
        arcpy.AddMessage("ItemNo. 2.4: Started Checking Are there any premises outside of a primary boundary?... ")
        try:
            if arcpy.Exists(self.customer_buildings_copy):
                arcpy.Delete_management(self.customer_buildings_copy)
            if not arcpy.Exists(self.customer_buildings_copy):
                arcpy.AddMessage("Copying customer_buildings..")
                arcpy.CopyFeatures_management(customer_buildings, self.customer_buildings_copy)

                lstFields_cb = arcpy.ListFields(self.customer_buildings_copy)
                field_names_cb = [f.name for f in lstFields_cb]

                if "OriginalOID" not in field_names_cb:
                    arcpy.AddField_management(self.customer_buildings_copy, "OriginalOID", "LONG", 10)
                    arcpy.CalculateField_management(self.customer_buildings_copy, "OriginalOID", "!OBJECTID!", "PYTHON3")
                else:
                    pass

        except Exception as e:
            arcpy.AddMessage("Issue in Copying customer_buildings  ... :  " + str(e))
        try:
            arcpy.AddMessage("Start selecting Primary Boundary ")
            arcpy.Select_analysis(boundaries, self.primary_boundary , "level_ in (2)")
            arcpy.AddMessage("Finished selecting features on attributes and making them into new layers ")
        except Exception as e:
            arcpy.AddMessage("Issue in selecting Primary Boundary  ... :  " + str(e))

        try:
            arcpy.AddMessage("Select by location buildings invert.. ")
            self.select_by_location_intersect_primary_boundaries_customer_buildings = arcpy.management.SelectLayerByLocation(customer_buildings, overlap_type = "INTERSECT", select_features = self.primary_boundary, invert_spatial_relationship = "INVERT")
        except Exception as e:
            arcpy.AddMessage("Issue in Select by location buildings invert..  :  " + str(e))
        try:
            self.matchcount = int(arcpy.GetCount_management(self.select_by_location_intersect_primary_boundaries_customer_buildings)[0])
            if self.matchcount == 0:
                print('No Customer building outside Primary Boundary')
            else:
                arcpy.CopyFeatures_management(self.select_by_location_intersect_primary_boundaries_customer_buildings, self.buildings_outside_primary_boundary)
                arcpy.AddMessage("Copying buildings_outside_primary_boundary ")
        except Exception as e:
            arcpy.AddMessage("Issue in Copying buildings_outside_primary_boundary...  :  " + str(e))

        try:
            if arcpy.Exists(self.buildings_outside_primary_boundary):
                lstFields_sc = arcpy.ListFields(self.buildings_outside_primary_boundary)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddField_management(self.buildings_outside_primary_boundary, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field Error to buildings_outside_primary_boundary :  " + str(e))

        try:
            arcpy.AddMessage("Updating Error message in buildings_outside_primary_boundary..")
            with arcpy.da.UpdateCursor(self.buildings_outside_primary_boundary, ("Error")) as cursor:
                for data in cursor:
                    data[0] = "Premise outside primary boundary "
                    cursor.updateRow(data)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating Error message in buildings_outside_primary_boundary..:  " + str(e))

        try:
            arcpy.AddMessage("Creating Numpy ARRAYS OF buildings_outside_primary_boundary")
            self.arr_buildings_outside_primary_boundary = arcpy.da.FeatureClassToNumPyArray(self.buildings_outside_primary_boundary, ["bldg_id", "Error"], skip_nulls=True)
            arcpy.AddMessage(self.arr_buildings_outside_primary_boundary)
        except Exception as e:
            arcpy.AddMessage("Issue in Creating Numpy ARRAYS OF buildings_outside_primary_boundary.. : " + str(e))

        try:
            if arcpy.Exists(self.customer_buildings_copy):
                lstFields_sc = arcpy.ListFields(self.customer_buildings_copy)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddField_management(self.customer_buildings_copy, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field Error to customer_buildings_copy... :  " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final primary boundary Log File- PremiseBoundary ")
            for x in self.arr_buildings_outside_primary_boundary:
                with arcpy.da.UpdateCursor(self.customer_buildings_copy, ("bldg_id", "Error", "OBJECTID")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            arcpy.AddMessage(str(row[2]))
                            if row[1] != None:
                                row[1] = str(row[1]) + "," + x[1]
                                print(x[1])
                            elif row[1] == None:
                                row[1] = x[1]
                                print(x[1])
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final primary boundary Log File- PremiseBoundary :  " + str(e))

        # ##################################3  deleting fields and rows 2.4 #############################################################
        #
        # try:
        #     fieldList = arcpy.ListFields(customer_buildings_copy)
        #     fieldsToBeDeleted = []
        #     for field in fieldList:
        #         if str(field.name) not in (
        #                 ["OBJECTID", "Shape", "bldg_id", "Error", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area", "SHAPE_Area"]):
        #             fieldsToBeDeleted.append(field.name)
        #     arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))
        #     arcpy.DeleteField_management(customer_buildings_copy, fieldsToBeDeleted)
        #     arcpy.AddMessage("Finished Deleting unwanted fields of error ")
        # except Exception as e:
        #     arcpy.AddMessage("No fields to delete " + str(e))
        #
        # try:
        #     with arcpy.da.UpdateCursor(customer_buildings_copy, ("Error")) as cursor:
        #         for row in cursor:
        #             if row[0] == None:
        #                 cursor.deleteRow()
        #         del cursor
        #     arcpy.AddMessage("Deletion of error less rows")
        # except Exception as e:
        #     arcpy.AddMessage("Issue in Deletion of error less rows : " + str(e))

        ###################################### deletion of intermediate layers 2.4 #######################################################################

    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("inside del 2.4 ")
        try:
            if arcpy.Exists(self.primary_boundary):
                arcpy.Delete_management(self.primary_boundary)
            if arcpy.Exists(self.buildings_outside_primary_boundary):
                arcpy.Delete_management(self.buildings_outside_primary_boundary)
        except Exception as e:
            print(e)
        except arcpy.ExecuteError as ex:
            print(ex)
        arcpy.AddMessage("Finished Deleting unwanted Layers ")

        arcpy.AddMessage("Finished ItemNo. 2.4: Checking Are there any premises outside of a primary boundary?... ,\n File has been generated: PremiseBoundary")

class ParentItem2(object):

    def __init__(self, name):
        ### Object Intialize Here ####
        arcpy.AddMessage("entered successfully inside ParentItem2 - 2.5")
        self.customer_buildings_copy = 'PremiseBoundary'
        self.secondary_boundary = 'secondary_boundary'
        self.buildings_outside_secondary_boundary = 'buildings_outside_secondary_boundary'

    # primary_boundary = 'primary_boundary'
    # buildings_outside_primary_boundary = 'buildings_outside_primary_boundary'
    # customer_buildings_copy = 'PremiseBoundary'
    ########################################## item 2.5 ##################################################3
    def ItemNo(self):
        arcpy.AddMessage("ItemNo. 2.5: Started Are there any premises outside of a Secondary boundary?... ")
        try:
            if arcpy.Exists(self.customer_buildings_copy):
                pass
            if not arcpy.Exists(self.customer_buildings_copy):
                arcpy.AddMessage("Copying customer_buildings..")
                arcpy.CopyFeatures_management(customer_buildings, self.customer_buildings_copy)
        except Exception as e:
            arcpy.AddMessage("Issue in Copying customer_buildings  ... :  " + str(e))
        try:
            arcpy.AddMessage("Start selecting Secondary Boundary ")
            arcpy.Select_analysis(boundaries, self.secondary_boundary , "level_ in (3)")
            arcpy.AddMessage("Finished selecting features on attributes and making them into new layers ")
        except Exception as e:
            arcpy.AddMessage("Issue in selecting Secondary Boundary  ... :  " + str(e))

        try:
            arcpy.AddMessage("Select by location buildings invert.. ")
            self.select_by_location_intersect_secondary_boundaries_customer_buildings = arcpy.management.SelectLayerByLocation(customer_buildings, overlap_type = "INTERSECT", select_features = self.secondary_boundary, invert_spatial_relationship = "INVERT")
        except Exception as e:
            arcpy.AddMessage("Issue in Select by location buildings invert..  :  " + str(e))
        try:
            matchcount2 = int(arcpy.GetCount_management(self.select_by_location_intersect_secondary_boundaries_customer_buildings)[0])
            if matchcount2 == 0:
                print('No Customer building outside Secondary Boundary')
            else:
                arcpy.CopyFeatures_management(self.select_by_location_intersect_secondary_boundaries_customer_buildings, self.buildings_outside_secondary_boundary)
                arcpy.AddMessage("Copying buildings_outside_secondary_boundary ")
        except Exception as e:
            arcpy.AddMessage("Issue in Copying buildings_outside_secondary_boundary...  :  " + str(e))

        try:
            if arcpy.Exists(self.buildings_outside_secondary_boundary):
                lstFields_sc = arcpy.ListFields(self.buildings_outside_secondary_boundary)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddField_management(self.buildings_outside_secondary_boundary, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field Error to buildings_outside_secondary_boundary :  " + str(e))

        try:
            arcpy.AddMessage("Updating Error message in buildings_outside_secondary_boundary..")
            with arcpy.da.UpdateCursor(self.buildings_outside_secondary_boundary, ("Error")) as cursor:
                for data in cursor:
                    data[0] = "Premise outside secondary boundary "
                    cursor.updateRow(data)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating Error message in buildings_outside_secondary_boundary..:  " + str(e))

        try:
            arcpy.AddMessage("Creating Numpy ARRAYS OF buildings_outside_secondary_boundary")
            self.arr_buildings_outside_secondary_boundary = arcpy.da.FeatureClassToNumPyArray(self.buildings_outside_secondary_boundary, ["bldg_id", "Error"], skip_nulls=True)
            arcpy.AddMessage(str(self.arr_buildings_outside_secondary_boundary))
        except Exception as e:
            arcpy.AddMessage("Issue in Creating Numpy ARRAYS OF buildings_outside_secondary_boundary.. : " + str(e))

        try:
            if arcpy.Exists(self.customer_buildings_copy):
                lstFields_sc = arcpy.ListFields(self.customer_buildings_copy)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddField_management(self.customer_buildings_copy, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field Error to customer_buildings_copy... :  " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final secondary boundary Log File- PremiseBoundary ")
            for x in self.arr_buildings_outside_secondary_boundary:
                with arcpy.da.UpdateCursor(self.customer_buildings_copy, ("bldg_id", "Error", "OBJECTID")) as cursor:
                    for row in cursor:
                        if row[0] == x[0] :
                            arcpy.AddMessage(str(row[2]))
                            if row[1] != None:
                                row[1] = str(row[1]) + "," + x[1]
                                print(x[1])
                            elif row[1] == None:
                                row[1] = x[1]
                                print(x[1])
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final secondary boundary Log File- PremiseBoundary :  " + str(e))

        ##################################3  deleting fields and rows 2.5 #############################################################
        #
        # try:
        #     fieldList = arcpy.ListFields(self.customer_buildings_copy)
        #     fieldsToBeDeleted = []
        #     for field in fieldList:
        #         if str(field.name) not in (
        #                 ["OBJECTID", "Shape", "bldg_id", "Error", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area", "SHAPE_Area"]):
        #             fieldsToBeDeleted.append(field.name)
        #     arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))
        #     arcpy.DeleteField_management(self.customer_buildings_copy, fieldsToBeDeleted)
        #     arcpy.AddMessage("Finished Deleting unwanted fields of error ")
        # except Exception as e:
        #     arcpy.AddMessage("No fields to delete " + str(e))
        #
        # try:
        #     with arcpy.da.UpdateCursor(self.customer_buildings_copy, ("Error")) as cursor:
        #         for row in cursor:
        #             if row[0] == None:
        #                 cursor.deleteRow()
        #         del cursor
        #     arcpy.AddMessage("Deletion of error less rows")
        # except Exception as e:
        #     arcpy.AddMessage("Issue in Deletion of error less rows : " + str(e))
        #
        # ###################################### deletion of intermediate layers 2.5 #######################################################################
        #
        # if arcpy.Exists(self.secondary_boundary):
        #     arcpy.Delete_management(self.secondary_boundary)
        # if arcpy.Exists(self.buildings_outside_secondary_boundary):
        #     arcpy.Delete_management(self.buildings_outside_secondary_boundary)

    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("inside del 2.5 ")
        try:
            if arcpy.Exists(self.secondary_boundary):
                arcpy.Delete_management(self.secondary_boundary)
            if arcpy.Exists(self.buildings_outside_secondary_boundary):
                arcpy.Delete_management(self.buildings_outside_secondary_boundary)
        except Exception as e:
            print(e)
        except arcpy.ExecuteError as ex:
            print(ex)
        arcpy.AddMessage("Finished Deleting unwanted Layers ")

        arcpy.AddMessage("Finished ItemNo. 2.5: Finished Are there any premises outside of a Secondary boundary?... ,\n File has been generated: PremiseBoundary")

class BaseClass(ParentItem1, ParentItem2):
    def __init__(self):

        if arcpy.Exists("PremiseBoundary"):
            arcpy.Delete_management("PremiseBoundary")

        ### Object Intialize Here ####
        if item2point4 == 'true':
            ParentItem1.__init__(self, 'PremiseBoundary')
            ParentItem1.ItemNo(self)
            ParentItem1.__del__(self)

        if item2point5 == 'true':
            ParentItem2.__init__(self, 'PremiseBoundary')
            ParentItem2.ItemNo(self)
            ParentItem2.__del__(self)

    # def hide_fileds(self, out_layer_file, field_list):
    #     desc = arcpy.Describe(out_layer_file)
    #     field_info = desc.fieldInfo
    #
    #     # List of fields to hide
    #     # desc.OIDFieldName is the name of the 'FID' field
    #     fieldsToHide = [desc.OIDFieldName, 'Shape', 'Shape_Length']
    #
    #     arcpy.AddMessage(fieldsToHide)
    #
    #     # for i in range(0, field_info.count):
    #     #     if field_info.getFieldName(i) in fieldsToHide:
    #     #         field_info.setVisible(i, "HIDDEN")
    #     #
    #     # arcpy.MakeFeatureLayer_management(out_layer_file, "New2", "", "", field_info)
    #     # return out_layer_file


    def __del__(self):
        ### Object Remove Here ######
        if item2point4 == 'true' or item2point5 == 'true':
            fieldList = arcpy.ListFields(self.customer_buildings_copy)
            fieldsToBeDeleted = []
            for field in fieldList:
                if item2point4 == 'true' and 'true' not in (item2point5) and str(field.name) not in (
                    ["OBJECTID", "Shape", "bldg_id", "Error", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area", "SHAPE_Area"]):
                    fieldsToBeDeleted.append(field.name)

                elif item2point4 != 'true' and 'true' in (item2point5) and str(field.name) not in (
                        ["OBJECTID", "Shape", "name", "Error", "bldg_id", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area",
                         "SHAPE_Area"]):
                    fieldsToBeDeleted.append(field.name)
                elif item2point4 == 'true' and 'true' in (item2point5) and str(field.name) not in (
                        ["OBJECTID", "Shape", "name", "Error", "bldg_id", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area",
                         "SHAPE_Area"]):
                    fieldsToBeDeleted.append(field.name)

                elif item2point4 != 'true' and 'true' not in (item2point5) and str(field.name) not in (
                        ["OBJECTID", "Shape", "name", "Error", "bldg_id", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area",
                         "SHAPE_Area"]):
                    fieldsToBeDeleted.append(field.name)

        try:
            if item2point4 == 'true' and 'true' not in (item2point5):
                with arcpy.da.UpdateCursor(self.customer_buildings_copy, ("Error")) as cursor:
                    for row in cursor:
                        if row[0] == None:
                            cursor.deleteRow()
                    del cursor
            elif item2point4 != 'true' and 'true' in (item2point5):
                with arcpy.da.UpdateCursor(self.customer_buildings_copy, ("Error")) as cursor:
                    for row in cursor:
                        if row[0] == None:
                            cursor.deleteRow()
                    del cursor
            elif item2point4 == 'true' and 'true' in (item2point5):
                with arcpy.da.UpdateCursor(self.customer_buildings_copy, ("Error")) as cursor:
                    for row in cursor:
                        if row[0] == None:
                            cursor.deleteRow()
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Deletion of error less rows : " + str(e))

        try:
            if arcpy.Exists(self.customer_buildings_copy):
                arcpy.DeleteField_management(self.customer_buildings_copy, fieldsToBeDeleted)
        except Exception as e:
            print(e)
        except arcpy.ExecuteError as ex:
            print(ex)
        arcpy.AddMessage("Finished Deleting unwanted fields of error ")
        arcpy.AddMessage(str(self.customer_buildings_copy) + " - Log File has been Generated")

    from datetime import datetime

    now = datetime.now()
    end_time = now.strftime("%H:%M:%S")

    arcpy.AddMessage("Script Run End's @" + end_time)


################################################## input 2.4, 2.5 ######################################
# boundaries = 'Boundaries'
# customer_buildings = 'Customer_Buildings'

input_geodatabase = arcpy.GetParameterAsText(0)
boundaries = arcpy.GetParameterAsText(1)
customer_buildings = arcpy.GetParameterAsText(2)

######## Checked Input #########
item2point4 = arcpy.GetParameterAsText(3)
item2point5 = arcpy.GetParameterAsText(4)
######### Oject Creation #######
# main function
if __name__ == '__main__':
    # created the object for BaseClass
    from datetime import datetime

    if arcpy.Exists("PremiseBoundary"):
        arcpy.Delete_management("PremiseBoundary")

    now = datetime.now()
    start_time = now.strftime("%H:%M:%S")

    arcpy.AddMessage("Script Run Start's @" + start_time)

    ob = BaseClass()

