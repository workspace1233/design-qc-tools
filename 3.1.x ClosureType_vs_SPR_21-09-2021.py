import arcpy

arcpy.env.overwriteOutput = True

class ParentItem1(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 3.1.1')

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase

        p2 = p1 + "\\" + self.Output_name
        if not arcpy.Exists(self.Output_name):
            #     arcpy.conversion.FeatureClassToFeatureClass(splice_closure, p1, self.Output_Layer, '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')

            feats = arcpy.FeatureSet(table=Splice_Closures)
            feats.save(p2)

        try:
            if arcpy.Exists(self.Output_name):
                lstFields_sc = arcpy.ListFields(self.Output_name)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error", "TEXT",100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        try:
            with arcpy.da.UpdateCursor(self.Output_name, ("enc_type", "sp1_type", "sp2_type", "Error")) as cursor:
                for row in cursor:
                    if str(row[0]) == "1" and str(row[1]) not in ('2', '3', '4') and str(row[2]) not in ('5', 'None'):
                        row[3] = "Incorrect ODP1 Splitter configuration"
                    cursor.updateRow(row)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors to self.Output_name.. 3.1.1 :  " + str(e))

        arcpy.AddMessage("Finished CheckingItem 3.1.1: Verify ODP1 closure type vs splitters inside...")


        try:
            with arcpy.da.UpdateCursor(self.Output_name, ("enc_type", "sp1_type", "sp2_type", "Error")) as cursor:
                for row in cursor:
                    if str(row[0]) == "2" and (str(row[1]) not in ('1', '3') or str(row[2]) not in ('1', '3' , '5')):
                        row[3] = "Incorrect ODP2 Splitter configuration"
                    cursor.updateRow(row)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors to self.Output_name.. 3.1.2 :  " + str(e))

        arcpy.AddMessage("Finished CheckingItem 3.1.2:  Verify ODP2 closure type vs splitters inside...")

        try:
            with arcpy.da.UpdateCursor(self.Output_name, ("enc_type", "sp1_type", "sp2_type", "Error")) as cursor:
                for row in cursor:
                    if str(row[0]) in ('3', '4') and str(row[1]) not in ('5', 'None') and str(row[2]) not in ('5', 'None'):
                        row[3] = "Incorrect ODP3/Joint Splitter configuration"
                    cursor.updateRow(row)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors to self.Output_name.. 3.1.3 :  " + str(e))

        arcpy.AddMessage("Finished CheckingItem 3.1.3:  Verify ODP3/Joints closure  type...")

        try:
            with arcpy.da.UpdateCursor(self.Output_name, ("enc_type", "sp1_type", "sp2_type", "Error")) as cursor:
                for row in cursor:
                    if str(row[0]) in ('5') and str(row[1]) not in ('2', '4') and str(row[2]) not in ('1', '3'):
                        if row[3] != None:
                            row[3] = str(row[3]) + ',' + "Incorrect ODP1/ODP2 Splitter configuration"
                        else:
                            row[3] = "Incorrect ODP1/ODP2 Splitter configuration"
                    cursor.updateRow(row)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors to self.Output_name.. 3.1.4 :  " + str(e))

        arcpy.AddMessage("Finished CheckingItem 3.1.4:  Verify ODP1/ODP2  closure type vs splitters inside...")

    def __del__(self):
        #### Object Remove Here ######
        pass

class ParentItem2(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name
        self.secondary_boundary = 'secondary_boundary'
        self.secondary_closure = 'secondary_closure'
        self.spatial_join_secondary_closure_secondary_boundary = 'spatial_join_secondary_closure_secondary_boundary'
        self.spatial_join_secondary_boundary_secondary_closure = 'spatial_join_secondary_boundary_secondary_closure'
        self.summarize_within_secondary_boundary_and_closures = 'summarize_within_secondary_boundary_and_closures'

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 3.1.5')
        arcpy.AddMessage('Tool Check Run For item 3.1.6')

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase

        p2 = p1 + "\\" + self.Output_name
        p3 = p1+ "\\" + self.secondary_closure

        if not arcpy.Exists(self.Output_name):
            #     arcpy.conversion.FeatureClassToFeatureClass(splice_closure, p1, self.Output_Layer, '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')

            feats = arcpy.FeatureSet(table=Splice_Closures)
            feats.save(p2)

        try:
            if arcpy.Exists(self.Output_name):
                lstFields_sc = arcpy.ListFields(self.Output_name)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        if not arcpy.Exists(self.secondary_closure):
            #     arcpy.conversion.FeatureClassToFeatureClass(splice_closure, p1, self.Output_Layer, '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')

            feats = arcpy.FeatureSet(table=Splice_Closures)
            feats.save(p3)

        with arcpy.da.UpdateCursor(self.secondary_closure,
                                   ("enc_type")) as cursor:
            for rows in cursor:
                if rows[0] not in (2,5):
                    cursor.deleteRow()
            del cursor

        try:
            arcpy.AddMessage("Selecting Secondary Boundary...")
            arcpy.Select_analysis(boundaries, self.secondary_boundary, "level_ in (3)")
        except Exception as e:
            arcpy.AddMessage("Issue in Selecting Secondary Boundary.. :  " + str(e))
        # try:
        #     arcpy.AddMessage("Selecting Secondary Closure...")
        #     arcpy.Select_analysis(Splice_Closures, self.secondary_closure, "enc_type in (2, 5)")
        # except Exception as e:
        #     arcpy.AddMessage("Issue in Selecting Secondary Closure.. :  " + str(e))

        try:
            arcpy.AddMessage("Spatial Join Analysis of Secondary Closure and Secondary Boundary.. :  ")
            arcpy.SpatialJoin_analysis(self.secondary_closure, self.secondary_boundary,
                                       self.spatial_join_secondary_closure_secondary_boundary, "JOIN_ONE_TO_ONE",
                                       "KEEP_ALL", "", "INTERSECT")
        except Exception as e:
            arcpy.AddMessage(
                "Issue in Spatial Join Analysis of Secondary Closure and Secondary Boundary.. :  " + str(e))

        try:
            if arcpy.Exists(self.spatial_join_secondary_closure_secondary_boundary):
                lstFields_sc = arcpy.ListFields(self.spatial_join_secondary_closure_secondary_boundary)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.spatial_join_secondary_closure_secondary_boundary..")
                    arcpy.AddField_management(self.spatial_join_secondary_closure_secondary_boundary, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        ################################################ item 3.1.5 ##################################################################

        arcpy.AddMessage("Started Checking Item 3.1.5:  Are there more premises than secondary splitter ports?...")
        array_splice = {
            1: 4,
            2: 4,
            3: 8,
            4: 8,
            5: 0
        }
        try:
            with arcpy.da.UpdateCursor(self.spatial_join_secondary_closure_secondary_boundary,
                                       ("homes", "enc_type", "sp1_type", "sp2_type", "Error")) as cursor:
                for rows in cursor:
                    index1 = rows[2] if rows[2] else ''
                    index2 = rows[3] if rows[3] else ''
                    if rows[1] != None and rows[0] != None and (index2 != 5 or index1 != 5):
                        if rows[1] == 2 and index1 != '' and rows[0] > (array_splice[index1] + array_splice[index2]):
                            rows[4] = "More premises than secondary splitter ports"
                        elif rows[1] == 5 and index2 != '' and rows[0] > array_splice[index2]:
                            rows[4] = "More premises than secondary splitter ports"
                        else:
                            pass
                        cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage(
                "Issue in Updating errors to self.spatial_join_secondary_boundary_secondary_closure.. 3.1.5 :  " + str(e))

        #Client has asked not to do by that time closure check has been finished checking(04-05-2021)
        # try:
        #     with arcpy.da.UpdateCursor(self.spatial_join_secondary_closure_secondary_boundary,
        #                                ("homes", "enc_type", "sp1_type", "sp2_type", "Error", "label")) as cursor:
        #         for rows in cursor:
        #             index1 = rows[2] if rows[2] else ''
        #             index2 = rows[3] if rows[3] else ''
        #             if rows[0] != None and index2 != '' and index1 != '':
        #                 if rows[0] > (array_splice[index1] + array_splice[index2]):
        #                     rows[4] = "More premises than secondary splitter ports"
        #                 else:
        #                     pass
        #                 cursor.updateRow(rows)
        #         del cursor
        # except Exception as e:
        #     arcpy.AddMessage(
        #         "Issue in Updating errors to self.spatial_join_secondary_boundary_secondary_closure.. 3.1.5 :  " + str(e))

        ############################################################## 3.1.6 #############################################################
        # try:
        #     arcpy.AddMessage("Counting number of Secondary Closures...")
        #     arcpy.AddField_management(self.secondary_closure, "count", "Long", field_length='500',
        #                               field_is_nullable='NULLABLE')
        #     with arcpy.da.UpdateCursor(self.secondary_closure, ("enc_type", "sp1_type", "sp2_type", "count")) as cursor:
        #         for rows in cursor:
        #             if rows[0] == 5:
        #                 if rows[2] != 5:
        #                     rows[3] = 1
        #             elif rows[0] == 2:
        #                 if rows[1] != 5 and rows[2] != 5:
        #                     rows[3] = 2
        #                 elif rows[1] == 5 and rows[2] != 5:
        #                     rows[3] = 1
        #                 elif rows[1] != 5 and rows[2] == 5:
        #                     rows[3] = 1
        #             cursor.updateRow(rows)
        # except Exception as e:
        #     arcpy.AddMessage("Issue in Counting number of Secondary Closures.. :  " + str(e))

        try:
            arcpy.AddMessage("Counting number of Secondary Closures...")
            arcpy.AddField_management(self.secondary_closure, "count", "Long", field_length='500',
                                      field_is_nullable='NULLABLE')
            with arcpy.da.UpdateCursor(self.secondary_closure, ("enc_type", "sp1_type", "sp2_type", "count")) as cursor:
                for rows in cursor:
                    if rows[1] != None and rows[2] != None:
                        count = 0
                        if rows[1] == 1 and rows[2] == 1:
                            count = 2
                        elif rows[1] == 4 and rows[2] == 1:
                            count = 1
                        elif rows[1] == 1 and rows[2] == 5:
                            count = 1
                        elif rows[1] == 2 and rows[2] == 3:
                            count = 2
                        elif rows[1] == 3 and rows[2] == 5:
                            count = 2
                        rows[3] = count
                        cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Counting number of Secondary Closures.. :  " + str(e))

        arcpy.SpatialJoin_analysis(self.secondary_boundary, self.secondary_closure,
                                   self.spatial_join_secondary_boundary_secondary_closure, "JOIN_ONE_TO_MANY",
                                   "KEEP_ALL", "", "INTERSECT")

        try:
            arcpy.AddMessage("Summarize within enter")
            arcpy.analysis.SummarizeWithin(self.spatial_join_secondary_boundary_secondary_closure, self.secondary_closure,
                                           self.summarize_within_secondary_boundary_and_closures,
                                           keep_all_polygons='KEEP_ALL', sum_fields=[['count', 'SUM']])
        except arcpy.ExecuteError as ex:
            arcpy.AddMessage("Issue in summarize within" + str(ex))

        try:
            arr_summarize_within_secondary_boundary_and_closures = arcpy.da.FeatureClassToNumPyArray(
                self.summarize_within_secondary_boundary_and_closures, ["JOIN_FID", "SUM_count"], skip_nulls=True)
            # arcpy.AddMessage(arr_summarize_within_secondary_boundary_and_closures)
        except Exception as e:
            arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))

        # try:
        with arcpy.da.UpdateCursor(self.spatial_join_secondary_closure_secondary_boundary,
                                   ("TARGET_FID", "homes", "Error")) as cursor:
            for rows in cursor:
                for data in arr_summarize_within_secondary_boundary_and_closures:
                    if str(rows[0]) == str(data[0]) and rows[1] != None:
                        # arcpy.AddMessage(str(rows[0]) + "//" + str(data[0]))
                        if rows[1] < 5 and data[1] > 1.0:
                            print(str(rows[1]) + " / " + str(data[1]))
                            rows[2] = "More secondary splitters than premises "
                            cursor.updateRow(rows)
            del cursor
        # except Exception as e:
        #     arcpy.AddMessage(
        #         "Issue in Updating errors to self.spatial_join_secondary_boundary_secondary_closure.. 3.1.5 :  " + str(e))

        try:
            arr_spatial_join_secondary_closure_secondary_boundary = arcpy.da.FeatureClassToNumPyArray(
                self.spatial_join_secondary_closure_secondary_boundary, ["TARGET_FID", "Error"], skip_nulls=True)
            arcpy.AddMessage("arr_spatial_join_secondary_closure_secondary_boundary")
            arcpy.AddMessage(arr_spatial_join_secondary_closure_secondary_boundary)
        except Exception as e:
            arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))

        # try:
            arcpy.AddMessage("Updating errors in final Log File-  ")
        for x in arr_spatial_join_secondary_closure_secondary_boundary:
            with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Error")) as cursor:
                for row in cursor:
                    if row[0] == x[0]:
                        arcpy.AddMessage("MESSAGE")
                        if row[1] != None:
                            arcpy.AddMessage(str(x[1]))
                            arcpy.AddMessage(str(row[1]))
                            row[1] = str(row[1]) + "," + x[1]
                        elif row[1] == None:
                            row[1] = x[1]
                            print(x[1])
                    else:
                        pass
                    cursor.updateRow(row)
                del cursor
        # except Exception as e:
        #     arcpy.AddMessage("Issue in Updating errors in final  Log File- Closure Type_vs_SPR:  " + str(e))
    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("item3point1point5..deleting")
        # try:
        if arcpy.Exists(self.secondary_boundary):
            arcpy.Delete_management(self.secondary_boundary)

        if arcpy.Exists(self.secondary_closure):
            arcpy.Delete_management(self.secondary_closure)

        if arcpy.Exists(self.spatial_join_secondary_closure_secondary_boundary):
            arcpy.Delete_management(self.spatial_join_secondary_closure_secondary_boundary)

        if arcpy.Exists(self.summarize_within_secondary_boundary_and_closures):
            arcpy.Delete_management(self.summarize_within_secondary_boundary_and_closures)

        if arcpy.Exists(self.spatial_join_secondary_boundary_secondary_closure):
            arcpy.Delete_management(self.spatial_join_secondary_boundary_secondary_closure)

        if arcpy.Exists(self.spatial_join_secondary_closure_secondary_boundary):
            arcpy.Delete_management(self.spatial_join_secondary_closure_secondary_boundary)
        # except Exception as e:
        #     arcpy.AddMessage("Issue in Updating errors to self.spatial_join_secondary_boundary_secondary_closure.. 3.1.5 :  " + str(e))


class BaseClass(ParentItem1, ParentItem2):
    def __init__(self):
        from datetime import datetime

        now = datetime.now()
        start_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run Start's @" + start_time)
        if arcpy.Exists("ClosureType_vs_SPR"):
            arcpy.Delete_management("ClosureType_vs_SPR")

        ### Object Intialize Here ####
        if item3point1point1to3point1point4 == 'true':
            ParentItem1.__init__(self, 'ClosureType_vs_SPR')
            ParentItem1.ItemNo(self)
            ParentItem1.__del__(self)

        if item3point1point5and6 == 'true':
            ParentItem2.__init__(self, 'ClosureType_vs_SPR')
            ParentItem2.ItemNo(self)
            ParentItem2.__del__(self)

        # if item3point1point7 == 'true':
        #     ParentItem3.__init__(self, 'ClosureType_vs_SPR')
        #     ParentItem3.ItemNo(self)
        #     ParentItem3.__del__(self)


    def remove_duplicates_None_values(self, array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[1] != 'None':
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1

    def remove_duplicates_zero_values(array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[2] != 0:
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1


    def __del__(self):
        ### Object Remove Here ######

        if item3point1point1to3point1point4 == 'true' or item3point1point5and6 == 'true':
            fieldList = arcpy.ListFields(self.Output_name)
            fieldsToBeDeleted = []
            for field in fieldList:
                if str(field.name) not in (["OBJECTID", "label", "Shape", "name", "Warning", "Error", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area",
                         "SHAPE_Area"]):
                    fieldsToBeDeleted.append(field.name)

            arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))

            # try:
            with arcpy.da.UpdateCursor(self.Output_name, ("Error")) as cursor:
                for row in cursor:
                    if row[0] == None:
                        cursor.deleteRow()
                del cursor
            #     # if item3point1point1to3point1point4 == 'true' and 'true' not in (item3point1point5and6):
            #     #
            #     # elif item3point1point1to3point1point4 != 'true' and 'true' in (item3point1point5and6):
            #     #     with arcpy.da.UpdateCursor(self.Output_name, ("Error")) as cursor:
            #     #         for row in cursor:
            #     #             if row[0] == None:
            #     #                 cursor.deleteRow()
            #     #         del cursor
            #     # else:
            #     #     with arcpy.da.UpdateCursor(self.Output_name, ("Error")) as cursor:
            #     #         for row in cursor:
            #     #             if row[0] == None:
            #     #                 cursor.deleteRow()
            #     #         del cursor
            # # except Exception as ex:
            # #     arcpy.AddMessage(ex)
            #
            # # try:
            if arcpy.Exists(self.Output_name):
                arcpy.DeleteField_management(self.Output_name, fieldsToBeDeleted)
            # except Exception as e:
            #     print(e)
            # except arcpy.ExecuteError as ex:
            #     print(ex)
            arcpy.AddMessage("Finished Deleting unwanted fields of error ")
            arcpy.AddMessage(str(self.Output_name) + " - Log File has been Generated")

        from datetime import datetime

        now = datetime.now()
        end_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run End's @" + end_time)


######### Get All Input Parameter ###########

arcpy.env.overwriteOutput = True

input_geodatabase = arcpy.GetParameterAsText(0)
Splice_Closures = arcpy.GetParameterAsText(1)
boundaries = arcpy.GetParameterAsText(2)

######## Checked Input #########

item3point1point1to3point1point4 = arcpy.GetParameterAsText(3)
item3point1point5and6 = arcpy.GetParameterAsText(4)
#item3point1point7 = arcpy.GetParameterAsText(4)

######### Oject Creation #######
# main function
if __name__ == '__main__':
    # created the object for BaseClass
    if arcpy.Exists("ClosureType_vs_SPR"):
        arcpy.AddMessage("main function")
        arcpy.Delete_management("ClosureType_vs_SPR")
    ob = BaseClass()
