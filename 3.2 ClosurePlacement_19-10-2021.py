import arcpy
arcpy.env.overwriteOutput = True
from datetime import datetime
now = datetime.now()
start_time = now.strftime("%H:%M:%S")

arcpy.env.preserveGlobalIds = True

#arcpy.env.workspace = r'C:\Users\user\Documents\ArcGIS\Projects\QCtesting2\Waterford_Design_Layers_soumik_version\f73d3823ee3c4881910ce98bb91c46dc.gdb'


class ParentItem3point2point1(object):
    ##### Intialise Class #####
    def __init__(self, name):
        ### Object Intialize Here ####
        arcpy.AddMessage("entered successfully inside ParentItem3point2point1 - 3.2.1")
        self.Output_Layer = name
        # self.Splice_Closure_copy = "Splice_Closure_copy"
        # self.Poles_copy = "Poles_copy"
        # self.Chambers_copy = "Chambers_copy"
        self.spatial_output_copy = "spatial_output_copy"

    ########################################## item 3.2.1 ##################################################3
    def ItemNo(self):
        arcpy.AddMessage("Tool Check: Is closure on an unsuitable pole?.")
        # arcpy.CopyFeatures_management(splice_closure, self.Splice_Closure_copy)
        # arcpy.CopyFeatures_management(poles, self.Poles_copy)
        # arcpy.CopyFeatures_management(chambers, self.Chambers_copy)

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase

        p2 = p1 + "\\" + self.Output_Layer
        if not arcpy.Exists(self.Output_Layer):
        #     arcpy.conversion.FeatureClassToFeatureClass(splice_closure, p1, self.Output_Layer, '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')

            feats = arcpy.FeatureSet(table=splice_closure)
            feats.save(p2)

        ############## Item Check Started #######################

        ########## Spatial Join Using Splice Closure and Pole with a distance 1 #########
        # arcpy.AlterField_management(self.Poles_copy, 'status', 'status_pole', 'Pole Status')

        arcpy.SpatialJoin_analysis(splice_closure, poles, self.spatial_output_copy, match_option = "WITHIN_A_DISTANCE", search_radius = 1)

        if arcpy.Exists(self.spatial_output_copy):
            arcpy.AddField_management(self.spatial_output_copy, "Error", "Text", 500)
            arcpy.AddField_management(self.spatial_output_copy, "Warning", "Text", 500)

        ############## Item No 10 Update ###################
        with arcpy.da.UpdateCursor(self.spatial_output_copy, ["Join_Count", "dp_capacit", "Error"]) as cursor:
            for rows in cursor:
                if rows[0] != None and rows[1] != None:
                    if int(rows[0]) != 0:
                        if rows[1] == 2:
                            rows[2] = "Closure on Pole with no additional capacity"
                            cursor.updateRow(rows)
            del cursor

        ################# Update on Output Layer ##########################
        if arcpy.Exists(self.Output_Layer):
            arcpy.AddField_management(self.Output_Layer, "Error", "Text", 500)
            arcpy.AddField_management(self.Output_Layer, "Warning", "Text", 500)

        spatial_array = arcpy.da.FeatureClassToNumPyArray(self.spatial_output_copy, ["TARGET_FID", "Error"], skip_nulls=True)

        arcpy.AddMessage("spatial_array")
        arcpy.AddMessage(spatial_array)

        for data in spatial_array:
            with arcpy.da.UpdateCursor(self.Output_Layer, ["OBJECTID", "Error"]) as cursor:
                for rows in cursor:
                    if str(data[0]) == str(rows[0]) and str(data[1]) != "None":
                        if rows[1] != None:
                            rows[1] = str(rows[1]) +','+ data[1]
                        else:
                            rows[1] = data[1]
                    cursor.updateRow(rows)
                del cursor

    def __del__(self):
        # pass
        if arcpy.Exists(self.spatial_output_copy):
            arcpy.Delete_management(self.spatial_output_copy)

class ParentItem3point2point2(object):

    def __init__(self, name):
        ### Object Intialize Here ####
        arcpy.AddMessage("entered successfully inside - 3.2.2")
        self.Output_Layer = name
        # self.splice_closure_copy = "splice_closure_copy"
        # self.chambers = "chambers_copy"


    ########################################## item 3.2.2 ##################################################3
    def ItemNo(self):
        arcpy.AddMessage("Tool Check: Is closure in an unsuitable chamber type?")

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase

        p2 = p1 + "\\" + self.Output_Layer
        if not arcpy.Exists(self.Output_Layer):
            #     arcpy.conversion.FeatureClassToFeatureClass(splice_closure, p1, self.Output_Layer, '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')

            feats = arcpy.FeatureSet(table=splice_closure)
            feats.save(p2)

        lstFields_sc = arcpy.ListFields(self.Output_Layer)
        field_names_sc = [f.name for f in lstFields_sc]
        if "Error" not in field_names_sc:
            arcpy.AddMessage("adding field (Error) in self.Output_name..")
            arcpy.AddField_management(self.Output_Layer, "Error", "Text", 500)

        if "Warning" not in field_names_sc:
            arcpy.AddMessage("adding field (Warning) in self.Output_name..")
            arcpy.AddField_management(self.Output_Layer, "Warning", "Text", 500)



        arcpy.SpatialJoin_analysis(splice_closure, chambers, "common_layer_splice_chamber", "JOIN_ONE_TO_MANY", "KEEP_ALL", "","INTERSECT")

        if arcpy.Exists("common_layer_splice_chamber"):
            arcpy.AddField_management("common_layer_splice_chamber", "Error", "Text", 500)
            arcpy.AddField_management("common_layer_splice_chamber", "Warning", "Text", 500)


        array_check = [12, 16, 17, 30, 31, 32, 34, 35, 36, 37, 39, 44, 45]
        # for data in array_chamber_splice:
        with arcpy.da.UpdateCursor("common_layer_splice_chamber", ("Join_Count", "type", "Error", "Warning")) as cursor:
            for row in cursor:
               if row[0] != None and row[1] != None:
                   if row[0] != 0:
                       if row[1] not in array_check and row[1] != 9 and row[1] != 42:
                           row[2] = "Closure in unsuitable chamber Type: [" + str(row[1]) + "]"
                       if row[1] not in array_check and row[1] == 9:
                           row[3] = "Closure in unsuitable chamber Type: [9]"
                       if row[1] not in array_check and row[1] == 42:
                           row[3] = "Closure in unsuitable chamber Type: [42]"
                       cursor.updateRow(row)
            del cursor

        array_chamber_splice = arcpy.da.FeatureClassToNumPyArray("common_layer_splice_chamber", ["TARGET_FID", "Error", "Warning"],skip_nulls=False)
        arcpy.AddMessage("array_chamber_splice")
        arcpy.AddMessage(array_chamber_splice)


        for data in array_chamber_splice:
            with arcpy.da.UpdateCursor(self.Output_Layer, ["OBJECTID", "Error", "Warning"]) as cursor:
                for rows in cursor:
                    if str(data[0]) == str(rows[0]) and str(data[1]) != "None":
                        if rows[1] != None:
                            rows[1] = str(rows[1]) + ',' + data[1]
                        else:
                            rows[1] = data[1]
                    if str(data[0]) == str(rows[0]) and str(data[2]) != "None":
                        if rows[2] != None:
                            rows[2] = str(rows[2]) + ',' + data[2]
                        else:
                            rows[2] = data[2]
                    cursor.updateRow(rows)
                del cursor

    def __del__(self):

        if arcpy.Exists("common_layer_splice_chamber"):
            arcpy.Delete_management("common_layer_splice_chamber")

class ParentItem3point2point3(object):

    def __init__(self, name):
        ### Object Intialize Here ####
        arcpy.AddMessage("entered successfully inside - 3.2.3")
        self.Output_Layer = name

    ########################################## item 3.2.3 ##################################################3
    def ItemNo(self):
        arcpy.AddMessage("Tool Check: Is closure in an unsuitable chamber (condition)?")

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase
        #
        p2 = p1 + "\\" + self.Output_Layer
        if not arcpy.Exists(self.Output_Layer):
            #     arcpy.conversion.FeatureClassToFeatureClass(splice_closure, p1, self.Output_Layer, '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')

            feats = arcpy.FeatureSet(table=splice_closure)
            feats.save(p2)

        lstFields_sc = arcpy.ListFields(self.Output_Layer)
        field_names_sc = [f.name for f in lstFields_sc]
        if "Error" not in field_names_sc:
            arcpy.AddMessage("adding field (Error) in self.Output_name..")
            arcpy.AddField_management(self.Output_Layer, "Error", "Text", 500)

        if "Warning" not in field_names_sc:
            arcpy.AddMessage("adding field (Warning) in self.Output_name..")
            arcpy.AddField_management(self.Output_Layer, "Warning", "Text", 500)

        arcpy.SpatialJoin_analysis(splice_closure, chambers, "common_layer_splice_chamber1",
                                   "JOIN_ONE_TO_ONE", "KEEP_ALL", "", "INTERSECT")

        if arcpy.Exists("common_layer_splice_chamber1"):
            arcpy.AddField_management("common_layer_splice_chamber1", "Error", "Text", 500)

        # for data in array_chamber_splice:
        with arcpy.da.UpdateCursor("common_layer_splice_chamber1", ("Join_Count", "condition", "Error")) as cursor:
            for row in cursor:
                if row[0] != None and row[1] != None:
                    if row[0] != 0:
                        if int(row[1]) == 4 or int(row[1]) == 5:
                            row[2] = "Closure in chamber with poor condition."
                        cursor.updateRow(row)
            del cursor

        array_chamber_splice = arcpy.da.FeatureClassToNumPyArray("common_layer_splice_chamber1", ["TARGET_FID", "Error"],
                                                                 skip_nulls=True)
        arcpy.AddMessage("array_chamber_splice")
        arcpy.AddMessage(array_chamber_splice)

        for data in array_chamber_splice:
            with arcpy.da.UpdateCursor(self.Output_Layer, ["OBJECTID", "Error"]) as cursor:
                for rows in cursor:
                    if str(data[0]) == str(rows[0]) and str(data[1]) != "None":
                        if rows[1] != None:
                            rows[1] = str(rows[1]) + ',' + data[1]
                        else:
                            rows[1] = data[1]
                    cursor.updateRow(rows)
                del cursor


    def __del__(self):
        # pass
        if arcpy.Exists("common_layer_splice_chamber1"):
            arcpy.Delete_management("common_layer_splice_chamber1")

class ParentItem3point2point4(object):

    def __init__(self, name):
        ### Object Intialize Here ####
        arcpy.AddMessage("entered successfully inside - 3.2.4")
        self.Output_Layer = name

    ########################################## item 3.2.4 ##################################################3
    def ItemNo(self):
        arcpy.AddMessage("Tool Check: Is closure in an unsuitable chamber (lid condition)?")

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase

        p2 = p1 + "\\" + self.Output_Layer
        if not arcpy.Exists(self.Output_Layer):
            #     arcpy.conversion.FeatureClassToFeatureClass(splice_closure, p1, self.Output_Layer, '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')

            feats = arcpy.FeatureSet(table=splice_closure)
            feats.save(p2)

        lstFields_sc = arcpy.ListFields(self.Output_Layer)
        field_names_sc = [f.name for f in lstFields_sc]
        if "Error" not in field_names_sc:
            arcpy.AddMessage("adding field (Error) in self.Output_name..")
            arcpy.AddField_management(self.Output_Layer, "Error", "Text", 500)

        if "Warning" not in field_names_sc:
            arcpy.AddMessage("adding field (Warning) in self.Output_name..")
            arcpy.AddField_management(self.Output_Layer, "Warning", "Text", 500)

        arcpy.SpatialJoin_analysis(splice_closure, chambers, "common_layer_splice_chamber2",
                                   "JOIN_ONE_TO_MANY", "KEEP_ALL", "", "INTERSECT")

        if arcpy.Exists("common_layer_splice_chamber2"):
            arcpy.AddField_management("common_layer_splice_chamber2", "Error", "Text", 500)

        # for data in array_chamber_splice:
        with arcpy.da.UpdateCursor("common_layer_splice_chamber2", ("Join_Count", "lid_con", "Error")) as cursor:
            for row in cursor:
                if row[0] != None and row[1] != None:
                    if row[0] != 0:
                        if row[1] == 2:
                            row[2] = "Closure in chamber with failed lid."
                        cursor.updateRow(row)
            del cursor

        array_chamber_splice = arcpy.da.FeatureClassToNumPyArray("common_layer_splice_chamber2", ["TARGET_FID", "Error"],
                                                                 skip_nulls=True)

        arcpy.AddMessage("array_chamber_splice")
        arcpy.AddMessage(array_chamber_splice)

        for data in array_chamber_splice:
            with arcpy.da.UpdateCursor(self.Output_Layer, ["OBJECTID", "Error"]) as cursor:
                for rows in cursor:
                    if str(data[0]) == str(rows[0]) and str(data[1]) != "None":
                        if rows[1] != None:
                            rows[1] = str(rows[1]) + ',' + data[1]
                        else:
                            rows[1] = data[1]
                    cursor.updateRow(rows)
                del cursor


    def __del__(self):
        # pass
        if arcpy.Exists("common_layer_splice_chamber2"):
            arcpy.Delete_management("common_layer_splice_chamber2")

class ParentItem3point2point5(object):

    def __init__(self, name):
        ### Object Intialize Here ####
        arcpy.AddMessage("entered successfully inside - 3.2.5")
        self.Output_Layer = name

    ########################################## item 3.2.5 ##################################################3
    def ItemNo(self):
        arcpy.AddMessage("Tool Check: Is closure in an unsuitable chamber (dp capacity)?")

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase

        p2 = p1 + "\\" + self.Output_Layer
        if not arcpy.Exists(self.Output_Layer):
            #     arcpy.conversion.FeatureClassToFeatureClass(splice_closure, p1, self.Output_Layer, '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')

            feats = arcpy.FeatureSet(table=splice_closure)
            feats.save(p2)

        lstFields_sc = arcpy.ListFields(self.Output_Layer)
        field_names_sc = [f.name for f in lstFields_sc]
        if "Error" not in field_names_sc:
            arcpy.AddMessage("adding field (Error) in self.Output_name..")
            arcpy.AddField_management(self.Output_Layer, "Error", "Text", 500)
        elif "Warning" not in field_names_sc:
            arcpy.AddMessage("adding field (Warning) in self.Output_name..")
            arcpy.AddField_management(self.Output_Layer, "Warning", "Text", 500)

        else:
            pass

        arcpy.SpatialJoin_analysis(splice_closure, chambers, "common_layer_splice_chamber3",
                                   "JOIN_ONE_TO_MANY", "KEEP_ALL", "", "INTERSECT")

        if arcpy.Exists("common_layer_splice_chamber3"):
            arcpy.AddField_management("common_layer_splice_chamber3", "Error", "Text", 500)
            arcpy.AddField_management("common_layer_splice_chamber3", "Warning", "Text", 500)

        # for data in array_chamber_splice:
        with arcpy.da.UpdateCursor("common_layer_splice_chamber3", ("Join_Count", "dp_capacit", "Error", "Warning")) as cursor:
            for row in cursor:
               if row[0] and row[1] != None:
                   if row[0] != 0:
                       if row[1] == 2:
                           row[2] = "Closure in chamber without capacity for DP. "
                       if row[1] == 3:
                           row[3] = "Closure in chamber Unable to Determine. "
                       cursor.updateRow(row)
            del cursor

        array_chamber_splice = arcpy.da.FeatureClassToNumPyArray("common_layer_splice_chamber3", ["TARGET_FID", "Error", "Warning"],
                                                                 skip_nulls=False)
        arcpy.AddMessage("array_chamber_splice")
        arcpy.AddMessage(array_chamber_splice)

        for data in array_chamber_splice:
            with arcpy.da.UpdateCursor(self.Output_Layer, ["OBJECTID", "Error", "Warning"]) as cursor:
                for rows in cursor:
                    if str(data[0]) == str(rows[0]) and str(data[1]) != "None":
                        if rows[1] != None:
                            rows[1] = str(rows[1]) + ',' + data[1]
                        else:
                            rows[1] = data[1]
                    if str(data[0]) == str(rows[0]) and str(data[2]) != "None":
                        if rows[2] != None:
                            rows[2] = str(rows[2]) + ',' + data[2]
                        else:
                            rows[2] = data[2]
                    cursor.updateRow(rows)
                del cursor


    def __del__(self):
        # pass
        if arcpy.Exists("common_layer_splice_chamber3"):
            arcpy.Delete_management("common_layer_splice_chamber3")

# class ParentItem3point2point6(object):
#
#     def __init__(self, name):
#         ### Object Intialize Here ####
#         arcpy.AddMessage("entered successfully inside - 3.2.6")
#         self.Output_Layer = name
#
#     ########################################## item 3.2.3 ##################################################3
#     def ItemNo(self):
#         arcpy.AddMessage("Tool Check: Is closure in an unsuitable chamber (hazard)?")
#
#         p = arcpy.mp.ArcGISProject("CURRENT")
#         p1 = p.defaultGeodatabase
#
#         p2 = p1 + "\\" + self.Output_Layer
#         if not arcpy.Exists(self.Output_Layer):
#             #     arcpy.conversion.FeatureClassToFeatureClass(splice_closure, p1, self.Output_Layer, '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')
#
#             feats = arcpy.FeatureSet(table=splice_closure)
#             feats.save(p2)
#
#         filed_list = arcpy.ListFields(self.Output_Layer)
#         if "Warning" not in filed_list:
#             arcpy.AddField_management(self.Output_Layer, "Warning", "Text", 500)
#
#         arcpy.SpatialJoin_analysis(splice_closure, chambers, "common_layer_splice_chamber4",
#                                    "JOIN_ONE_TO_MANY", "KEEP_ALL", "", "INTERSECT")
#
#         if arcpy.Exists("common_layer_splice_chamber4"):
#             arcpy.AddField_management("common_layer_splice_chamber4", "Warning", "Text", 500)
#
#         # for data in array_chamber_splice:
#         with arcpy.da.UpdateCursor("common_layer_splice_chamber4", ("Join_Count", "hazard", "Warning")) as cursor:
#             for row in cursor:
#                 if row[0] != None and row[1] != None:
#                     if row[0] != 0:
#                         if row[1] == 2:
#                             row[2] = "Closure in traffic sensitive chamber. "
#                         cursor.updateRow(row)
#             del cursor
#
#         array_chamber_splice = arcpy.da.FeatureClassToNumPyArray("common_layer_splice_chamber4", ["TARGET_FID", "Warning"],
#                                                                  skip_nulls=True)
#
#         arcpy.AddMessage("array_chamber_splice")
#         arcpy.AddMessage(array_chamber_splice)
#
#         for data in array_chamber_splice:
#             with arcpy.da.UpdateCursor(self.Output_Layer, ["OBJECTID", "Warning"]) as cursor:
#                 for rows in cursor:
#                     if str(data[0]) == str(rows[0]) and str(data[1]) != "None":
#                         if rows[1] != None:
#                             rows[1] = str(rows[1]) + ',' + data[1]
#                         else:
#                             rows[1] = data[1]
#                     cursor.updateRow(rows)
#                 del cursor
#
#
#     def __del__(self):
#         # pass
#         if arcpy.Exists("common_layer_splice_chamber4"):
#             arcpy.Delete_management("common_layer_splice_chamber4")

class ParentItem3point2point7(object):

    def __init__(self, name):
        ### Object Intialize Here ####
        arcpy.AddMessage("entered successfully inside - 3.2.7")
        self.Output_Layer = name

    ########################################## item 3.2.3 ##################################################3
    def ItemNo(self):
        arcpy.AddMessage("Tool Check: Is closure on an unsuitable pole (hazard)?")

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase

        p2 = p1 + "\\" + self.Output_Layer
        if not arcpy.Exists(self.Output_Layer):
            #     arcpy.conversion.FeatureClassToFeatureClass(splice_closure, p1, self.Output_Layer, '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')

            feats = arcpy.FeatureSet(table=splice_closure)
            feats.save(p2)

        filed_list = arcpy.ListFields(self.Output_Layer)
        if "Warning" not in filed_list:
            arcpy.AddField_management(self.Output_Layer, "Warning", "Text", 500)

        arcpy.SpatialJoin_analysis(splice_closure, poles, "common_layer_splice_poles1",
                                   "JOIN_ONE_TO_MANY", "KEEP_ALL", "", "INTERSECT")

        if arcpy.Exists("common_layer_splice_poles1"):
            arcpy.AddField_management("common_layer_splice_poles1", "Error", "Text", 500)



        # for data in array_chamber_splice:
        with arcpy.da.UpdateCursor("common_layer_splice_poles1", ("Join_Count", "hazard", "Error")) as cursor:
            for row in cursor:
                if row[0] != None and row[1] != None:
                    if row[0] != 0:
                        if row[1] == 5:
                            row[2] = "Closure on Pole in Power Exclusion Zone"
                        cursor.updateRow(row)
            del cursor

        array_pole_splice = arcpy.da.FeatureClassToNumPyArray("common_layer_splice_poles1", ["TARGET_FID", "Error"],
                                                                 skip_nulls=True)

        arcpy.AddMessage("array_pole_splice")
        arcpy.AddMessage(array_pole_splice)

        for data in array_pole_splice:
            with arcpy.da.UpdateCursor(self.Output_Layer, ["OBJECTID", "Error"]) as cursor:
                for rows in cursor:
                    if str(data[0]) == str(rows[0]) and str(data[1]) != "None":
                        if rows[1] != None:
                            rows[1] = str(rows[1]) + ',' + data[1]
                        else:
                            rows[1] = data[1]
                    cursor.updateRow(rows)
                del cursor


    def __del__(self):
        # pass
        if arcpy.Exists("common_layer_splice_chamber5"):
            arcpy.Delete_management("common_layer_splice_chamber5")
        if arcpy.Exists("common_layer_splice_poles1"):
            arcpy.Delete_management("common_layer_splice_poles1")

class ParentItem3point2point8(object):

    def __init__(self, name):
        ### Object Intialize Here ####
        arcpy.AddMessage("entered successfully inside - 3.2.8")
        self.Output_Layer = name
        # self.splice_closure_copy = "splice_closure_copy"
        # self.chambers_copy = "chambers_copy"
        # self.pole_copy = "pole_copy"

    ########################################## item 3.2.8 ##################################################3
    def ItemNo(self):
        arcpy.AddMessage("Tool Check: Is the closure not snapped to a pole or chamber?")

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase

        p2 = p1 + "\\" + self.Output_Layer
        if not arcpy.Exists(self.Output_Layer):
            #     arcpy.conversion.FeatureClassToFeatureClass(splice_closure, p1, self.Output_Layer, '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')

            feats = arcpy.FeatureSet(table=splice_closure)
            feats.save(p2)

        filed_list = arcpy.ListFields(self.Output_Layer)
        if "Error" not in filed_list:
            arcpy.AddField_management(self.Output_Layer, "Error", "Text", 500)

        arcpy.Merge_management([poles, chambers], "pole_chamber_merged")

        arcpy.SpatialJoin_analysis(splice_closure, "pole_chamber_merged", "common_layer", join_operation='JOIN_ONE_TO_MANY', join_type='KEEP_ALL', field_mapping='', match_option='INTERSECT')


        if arcpy.Exists("common_layer"):
            lstFields_sc = arcpy.ListFields("common_layer")
            field_names_sc = [f.name for f in lstFields_sc]
            if "Error" not in field_names_sc:
                arcpy.AddField_management("common_layer", "Error", "Text", 10)
            else:
                pass


        with arcpy.da.UpdateCursor("common_layer", ("Join_Count", "Error", "label")) as cursor:
            for rows in cursor:
                if rows[0] == 0:
                    arcpy.AddMessage("Closure not snapped to structure")
                    rows[1] = "Closure not snapped to structure"
                    cursor.updateRow(rows)
                else:
                    cursor.deleteRow()

        array_common_layer = arcpy.da.FeatureClassToNumPyArray("common_layer", ["TARGET_FID", "Error"],skip_nulls=False)
        arcpy.AddMessage("array_common_layer")
        arcpy.AddMessage(array_common_layer)

        for data in array_common_layer:
            with arcpy.da.UpdateCursor(self.Output_Layer, ("OBJECTID", "Error")) as cursor:
                for row in cursor:
                    if row[0] != None:
                        if str(row[0]) == str(data[0]):
                            if row[1] != None:
                                row[1] = str(row[1]) + ',' + str(data[1])
                            else:
                                row[1] = str(data[1])
                            cursor.updateRow(row)
                del cursor

    def __del__(self):
        # pass
        if arcpy.Exists("common_layer"):
            arcpy.Delete_management("common_layer")
        if arcpy.Exists("pole_chamber_merged"):
            arcpy.Delete_management("pole_chamber_merged")

class ParentItem3point2point9(object):

    def __init__(self, name):
        ### Object Intialize Here ####
        arcpy.AddMessage("entered successfully inside - 3.2.9")
        self.Output_Layer = name

    ########################################## item 3.2.3 ##################################################3
    def ItemNo(self):
        arcpy.AddMessage("Tool Check: Flag any coincident splice closures ?")

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase

        p2 = p1 + "\\" + self.Output_Layer
        if not arcpy.Exists(self.Output_Layer):
            #     arcpy.conversion.FeatureClassToFeatureClass(splice_closure, p1, self.Output_Layer, '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')

            feats = arcpy.FeatureSet(table=splice_closure)
            feats.save(p2)

        filed_list = arcpy.ListFields(self.Output_Layer)
        if "Error" not in filed_list:
            arcpy.AddField_management(self.Output_Layer, "Error", "Text", 500)
        if "Warning" not in filed_list:
            arcpy.AddField_management(self.Output_Layer, "Warning", "Text", 500)

        arcpy.SpatialJoin_analysis(chambers, splice_closure, "common_layer_splice_chamber7", join_operation='JOIN_ONE_TO_MANY', join_type ='KEEP_ALL', field_mapping = '', match_option = 'INTERSECT')

        arcpy.SpatialJoin_analysis(poles, splice_closure, "common_layer_splice_pole1", join_operation='JOIN_ONE_TO_MANY', join_type ='KEEP_ALL', field_mapping = '', match_option = 'INTERSECT')


        # if arcpy.Exists(self.splice_closure_copy):
        #     arcpy.AddField_management(self.splice_closure_copy, "Error", "Text", 500)

        if arcpy.Exists("common_layer_splice_chamber7"):
            lstFields_sc = arcpy.ListFields("common_layer_splice_chamber7")
            field_names_sc = [f.name for f in lstFields_sc]
            if "Count" not in field_names_sc:
                arcpy.AddField_management("common_layer_splice_chamber7", "Count", "Long", 10)
            if "Warning" not in field_names_sc:
                arcpy.AddField_management("common_layer_splice_chamber7", "Warning", "Text", 10)

        with arcpy.da.UpdateCursor("common_layer_splice_chamber7", ("Count")) as cursor:
            for rows in cursor:
                rows[0] = 1
                cursor.updateRow(rows)
            del cursor

        array = []
        with arcpy.da.UpdateCursor("common_layer_splice_chamber7", ("Count", "TARGET_FID")) as cursor:
            for rows in cursor:
                if rows[1] != None:
                    if str(rows[1]) in array and str(rows[0]) != "None":
                        rows[0] = int(rows[0]) + 1
                        array.append(rows[1])
                    else:
                        rows[0] = 1
                        array.append(rows[1])
                    cursor.updateRow(rows)
            del cursor

        with arcpy.da.UpdateCursor("common_layer_splice_chamber7", ("Count", "Warning")) as cursor:
            for rows in cursor:
                if rows[0] != None:
                    if int(rows[0]) > 1:
                        rows[1] = "More than 1 enclosure in the same  chamber"
                        cursor.updateRow(rows)
            del cursor

        if arcpy.Exists("common_layer_splice_pole1"):
            lstFields_sc = arcpy.ListFields("common_layer_splice_pole1")
            field_names_sc = [f.name for f in lstFields_sc]
            if "Count" not in field_names_sc:
                arcpy.AddField_management("common_layer_splice_pole1", "Count", "Long", 100)
            if "Error" not in field_names_sc:
                arcpy.AddField_management("common_layer_splice_pole1", "Error", "Text", 10)

        with arcpy.da.UpdateCursor("common_layer_splice_pole1", ("Count")) as cursor:
            for rows in cursor:
                rows[0] = 1
                cursor.updateRow(rows)
            del cursor

        array1 = []
        with arcpy.da.UpdateCursor("common_layer_splice_pole1", ("Count", "TARGET_FID")) as cursor:
            for rows in cursor:
                if rows[1] != None:
                    if str(rows[1]) in array1:
                        rows[0] = int(rows[0]) + 1
                        array1.append(str(rows[1]))
                    else:
                        rows[0] = 1
                        array1.append(str(rows[1]))
                    cursor.updateRow(rows)
            del cursor

        with arcpy.da.UpdateCursor("common_layer_splice_pole1", ("Count", "Error")) as cursor:
            for rows in cursor:
                if rows[0] != None:
                    if int(rows[0]) > 1:
                        rows[1] = "More than 1 enclosure in the same  pole "
                    cursor.updateRow(rows)
            del cursor

        array_chamber = arcpy.da.FeatureClassToNumPyArray("common_layer_splice_chamber7", ["JOIN_FID", "Warning"], skip_nulls=True)
        arcpy.AddMessage("array_chamber")
        arcpy.AddMessage(array_chamber)

        array_pole = arcpy.da.FeatureClassToNumPyArray("common_layer_splice_pole1", ["JOIN_FID", "Error"], skip_nulls=True)

        arcpy.AddMessage("array_pole")
        arcpy.AddMessage(array_pole)

        if arcpy.Exists(self.Output_Layer):
            lstFields_sc = arcpy.ListFields(self.Output_Layer)
            field_names_sc = [f.name for f in lstFields_sc]
            if "Error" not in field_names_sc:
                arcpy.AddField_management(self.Output_Layer, "Error", "Text", 10)
            if "Warning" not in field_names_sc:
                arcpy.AddField_management(self.Output_Layer, "Warning", "Text", 10)

        for data in array_pole:
            with arcpy.da.UpdateCursor(self.Output_Layer, ("OBJECTID", "Error")) as cursor:
                for row in cursor:
                    if str(row[0]) != "None":
                        if str(row[0]) == str(data[0]):
                            if row[1] != None:
                                row[1] = str(row[1]) + ',' + str(data[1])
                            else:
                                row[1] = str(data[1])
                        cursor.updateRow(row)
                del cursor

        for data1 in array_chamber:
            with arcpy.da.UpdateCursor(self.Output_Layer, ("OBJECTID", "Warning")) as cursor:
                for row in cursor:
                    if str(row[0]) != "None":
                        if str(row[0]) == str(data1[0]):
                            if row[1] != None:
                                row[1] = str(row[1]) + ',' + str(data1[1])
                            else:
                                row[1] = str(data1[1])
                        cursor.updateRow(row)
                del cursor

    def __del__(self):
        # pass
        if arcpy.Exists("common_layer_splice_chamber7"):
            arcpy.Delete_management("common_layer_splice_chamber7")
        if arcpy.Exists("common_layer_splice_pole1"):
            arcpy.Delete_management("common_layer_splice_pole1")

class ParentItem3point2point10(object):

    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_Layer = name

    ########################################## item 3.2.10 & 3.2.11 ##################################################3
    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 3.2.10')
        arcpy.AddMessage('Tool Check Run For item 3.2.11')

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase

        p2 = p1 + "\\" + self.Output_Layer
        if not arcpy.Exists(self.Output_Layer):
            #     arcpy.conversion.FeatureClassToFeatureClass(splice_closure, p1, self.Output_Layer, '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')

            feats = arcpy.FeatureSet(table=splice_closure)
            feats.save(p2)

        try:
            if arcpy.Exists(self.Output_Layer):
                lstFields_sc = arcpy.ListFields(self.Output_Layer)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_Layer..")
                    arcpy.AddField_management(self.Output_Layer, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_Layer.. :  " + str(e))

        arcpy.CopyFeatures_management(boundaries, "primaryB")

        with arcpy.da.UpdateCursor("primaryB", ("level_"))as cursor:
            for row in cursor:
                if row[0] != 2:
                    cursor.deleteRow()
            del cursor

        arcpy.SpatialJoin_analysis(splice_closure, "primaryB" , "SJ_sc_bound_pri",
                                       join_operation='JOIN_ONE_TO_MANY', join_type='KEEP_ALL', field_mapping='',
                                       match_option='INTERSECT')
        if arcpy.Exists("SJ_sc_bound_pri"):
            lstFields_sc = arcpy.ListFields("SJ_sc_bound_pri")
            field_names_sc = [f.name for f in lstFields_sc]
            if "Error" not in field_names_sc:
                arcpy.AddField_management("SJ_sc_bound_pri", "Error", "Text", 100)


        with arcpy.da.UpdateCursor("SJ_sc_bound_pri", ("Join_Count", "enc_type", "level_", "Error"))as cursor:
            for row in cursor:
                    if row[0] != 1:
                        if row[1] in (1,5):
                            arcpy.AddMessage("gaya 1")
                            row[3] = "ODP1 or ODP1/ODP2 outside of a primary boundary. "
                        cursor.updateRow(row)
            del cursor

        array_chamber1 = arcpy.da.FeatureClassToNumPyArray("SJ_sc_bound_pri", ["TARGET_FID", "Error"],
                                                           skip_nulls=True)
        arcpy.AddMessage("array_chamber1")
        arcpy.AddMessage(array_chamber1)

        for data1 in array_chamber1:
            with arcpy.da.UpdateCursor(self.Output_Layer, ("OBJECTID", "Error")) as cursor:
                for row in cursor:
                    if str(row[0]) != "None":
                        if str(row[0]) == str(data1[0]):
                            if row[1] != None:
                                row[1] = str(row[1]) + ',' + str(data1[1])
                            else:
                                row[1] = str(data1[1])
                        cursor.updateRow(row)
                del cursor

        arcpy.CopyFeatures_management(boundaries, "secondaryB")

        with arcpy.da.UpdateCursor("secondaryB", ("level_"))as cursor:
            for row in cursor:
                if row[0] != 3:
                    cursor.deleteRow()
            del cursor

        arcpy.SpatialJoin_analysis(splice_closure, "secondaryB", "SJ_sc_bound_sec",
                                   join_operation='JOIN_ONE_TO_MANY', join_type='KEEP_ALL', field_mapping='',
                                   match_option='INTERSECT')

        if arcpy.Exists("SJ_sc_bound_sec"):
            lstFields_sc = arcpy.ListFields("SJ_sc_bound_sec")
            field_names_sc = [f.name for f in lstFields_sc]
            if "Error" not in field_names_sc:
                arcpy.AddField_management("SJ_sc_bound_sec", "Error", "Text", 100)

        with arcpy.da.UpdateCursor("SJ_sc_bound_sec", ("Join_Count", "enc_type", "level_", "Error"))as cursor:
            for row in cursor:
                    if row[0] != 1:
                        if row[1] in (2, 3, 5):
                            arcpy.AddMessage("gaya2")
                            row[3] = "ODP2s , ODP1/ODP2 or ODP3 outside of a secondary boundary. "
                        cursor.updateRow(row)
            del cursor

        array_chamber2 = arcpy.da.FeatureClassToNumPyArray("SJ_sc_bound_sec", ["TARGET_FID", "Error"],
                                                          skip_nulls=True)
        arcpy.AddMessage("array_chamber2")
        arcpy.AddMessage(array_chamber2)


        for data2 in array_chamber2:
            with arcpy.da.UpdateCursor(self.Output_Layer, ("OBJECTID", "Error")) as cursor:
                for row in cursor:
                    if str(row[0]) != "None":
                        if str(row[0]) == str(data2[0]):
                            if row[1] != None:
                                row[1] = str(row[1]) + ',' + str(data2[1])
                            else:
                                row[1] = str(data2[1])
                        cursor.updateRow(row)
                del cursor

    def __del__(self):
        # pass
        if arcpy.Exists("SJ_sc_bound_sec"):
            arcpy.Delete_management("SJ_sc_bound_sec")
        if arcpy.Exists("SJ_sc_bound_pri"):
            arcpy.Delete_management("SJ_sc_bound_pri")
        if arcpy.Exists("secondaryB"):
            arcpy.Delete_management("secondaryB")
        if arcpy.Exists("primaryB"):
            arcpy.Delete_management("primaryB")


class BaseClass(ParentItem3point2point1, ParentItem3point2point2, ParentItem3point2point3, ParentItem3point2point4, ParentItem3point2point5, ParentItem3point2point8, ParentItem3point2point9):
    def __init__(self):
        if arcpy.Exists("ClosurePlacement"):
            arcpy.Delete_management("ClosurePlacement")

        ### Object Intialize Here ####
        if item3point1 == 'true':
            ParentItem3point2point1.__init__(self, 'ClosurePlacement')
            ParentItem3point2point1.ItemNo(self)
            ParentItem3point2point1.__del__(self)

        if item3point1 == 'true':
            ParentItem3point2point2.__init__(self, 'ClosurePlacement')
            ParentItem3point2point2.ItemNo(self)
            ParentItem3point2point2.__del__(self)

        if item3point1 == 'true':
            ParentItem3point2point3.__init__(self, 'ClosurePlacement')
            ParentItem3point2point3.ItemNo(self)
            ParentItem3point2point3.__del__(self)

        if item3point1 == 'true':
            ParentItem3point2point4.__init__(self, 'ClosurePlacement')
            ParentItem3point2point4.ItemNo(self)
            ParentItem3point2point4.__del__(self)

        if item3point1 == 'true':
            ParentItem3point2point5.__init__(self, 'ClosurePlacement')
            ParentItem3point2point5.ItemNo(self)
            ParentItem3point2point5.__del__(self)

        # if item3point1 == 'true':
        #     ParentItem3point2point6.__init__(self, 'ClosurePlacement')
        #     ParentItem3point2point6.ItemNo(self)
        #     ParentItem3point2point6.__del__(self)

        if item3point1 == 'true':
            ParentItem3point2point7.__init__(self, 'ClosurePlacement')
            ParentItem3point2point7.ItemNo(self)
            ParentItem3point2point7.__del__(self)

        if item3point2 == 'true':
            ParentItem3point2point8.__init__(self, 'ClosurePlacement')
            ParentItem3point2point8.ItemNo(self)
            ParentItem3point2point8.__del__(self)

        if item3point3 == 'true':
            ParentItem3point2point9.__init__(self, 'ClosurePlacement')
            ParentItem3point2point9.ItemNo(self)
            ParentItem3point2point9.__del__(self)

        if item3point4 == 'true':
            ParentItem3point2point10.__init__(self, 'ClosurePlacement')
            ParentItem3point2point10.ItemNo(self)
            ParentItem3point2point10.__del__(self)

    def __del__(self):
        ### Object Remove Here ######
        arcpy.AddMessage("del 1")
        if arcpy.Exists(self.Output_Layer):
            fieldList = arcpy.ListFields(self.Output_Layer)
            fieldsToBeDeleted = []
            for field in fieldList:
                if str(field.name) not in (
                        ["OBJECTID", "label", "Shape", "name", "Warning", "Error", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area", "SHAPE_Area"]):
                    fieldsToBeDeleted.append(field.name)

            arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))

            try:
                arcpy.DeleteField_management(self.Output_Layer, fieldsToBeDeleted)
                arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))
            except Exception as ex:
                arcpy.AddMessage(ex)
            if item3point1 == 'true':
                arcpy.AddMessage('1')
                with arcpy.da.UpdateCursor(self.Output_Layer, ("Error", "Warning")) as cursor:
                    for row in cursor:
                        if row[0] == None and row[1] == None:
                            cursor.deleteRow()
                    del cursor
            elif item3point2 == 'true' and item3point4 != 'true' and item3point1 != 'true' and item3point3 != 'true':
                arcpy.AddMessage('2')
                with arcpy.da.UpdateCursor(self.Output_Layer, ("Error")) as cursor:
                    for row in cursor:
                        if row[0] == None:
                            cursor.deleteRow()
                    del cursor
            elif item3point3 == 'true':
                arcpy.AddMessage('3')
                with arcpy.da.UpdateCursor(self.Output_Layer, ("Error", "Warning")) as cursor:
                    for row in cursor:
                        if row[0] == None:
                            cursor.deleteRow()
                    del cursor
            elif item3point4 == 'true' and item3point1 != 'true' and item3point2 != 'true' and item3point3 != 'true':
                arcpy.AddMessage('4')
                with arcpy.da.UpdateCursor(self.Output_Layer, ("Error")) as cursor:
                    for row in cursor:
                        if row[0] == None:
                            cursor.deleteRow()
                    del cursor
            else:
                arcpy.AddMessage('else')
                with arcpy.da.UpdateCursor(self.Output_Layer, ("Error", "Warning")) as cursor:
                    for row in cursor:
                        if row[0] == None and row[1] == None:
                            cursor.deleteRow()
                    del cursor


        from datetime import datetime

        now = datetime.now()
        end_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run End's @" + end_time)


################################################## input 2.4, 2.5 ######################################

input_geodatabase = arcpy.GetParameterAsText(0)
splice_closure = arcpy.GetParameterAsText(1)
poles = arcpy.GetParameterAsText(2)
chambers = arcpy.GetParameterAsText(3)
boundaries = arcpy.GetParameterAsText(4)

######## Checked Input #########
item3point1 = arcpy.GetParameterAsText(5)
item3point2 = arcpy.GetParameterAsText(6)
item3point3 = arcpy.GetParameterAsText(7)
item3point4 = arcpy.GetParameterAsText(8)

# item2point1 = 'true'
# item2point2 = 'false'
# item2point3 = 'false'
######### Oject Creation #######
# main function
if __name__ == '__main__':
    # created the object for BaseClass
    if arcpy.Exists("ClosurePlacement"):
        arcpy.Delete_management("ClosurePlacement")

    from datetime import datetime

    now = datetime.now()
    start_time = now.strftime("%H:%M:%S")

    arcpy.AddMessage("Script Run Start's @" + start_time)

    ob = BaseClass()

