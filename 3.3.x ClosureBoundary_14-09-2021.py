import arcpy

arcpy.env.overwriteOutput = True

class ParentItem1(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 3.3.1')

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase
        p3 = p1 + "\\" + "primary_boundary"
        p2 = p1 + "\\" + self.Output_name
        if not arcpy.Exists(self.Output_name):
            # arcpy.conversion.FeatureClassToFeatureClass(boundaries,
            #                                             p1,
            #                                             self.Output_name, '',
            #                                             r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,GlobalID,-1,-1;level_ "Level" true true false 0 Long 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,level_,-1,-1;name "Name" true true false 20 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,name,0,20;homes "Number of Homes in Boundary" true true false 0 Long 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,homes,-1,-1;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Editor,0,128;Shape__Area "Shape__Area" false true true 0 Double 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Shape__Area,-1,-1;Shape__Length "Shape__Length" false true true 0 Double 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Shape__Length,-1,-1;CreationDate_1 "CreationDate" false true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,CreationDate_1,-1,-1;Creator_1 "Creator" false true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Creator_1,0,128;EditDate_1 "EditDate" false true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,EditDate_1,-1,-1;Editor_1 "Editor" false true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Editor_1,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,OBJECTID,-1,-1',
            #                                             '')
            feats = arcpy.FeatureSet(table=boundaries)
            feats.save(p2)

        try:
            if arcpy.Exists(self.Output_name):
                lstFields_sc = arcpy.ListFields(self.Output_name)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error", "TEXT",100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))


        if not arcpy.Exists("primary_boundary"):
            feats = arcpy.FeatureSet(table=boundaries)
            feats.save(p3)


        with arcpy.da.UpdateCursor("primary_boundary", ("level_"))as cursor:
            for row in cursor:
                if row[0] != None:
                    if row[0] != 2:
                        cursor.deleteRow()
            del cursor
        if not arcpy.Exists("primary_splice_closure"):
            arcpy.CopyFeatures_management(Splice_Closures,"primary_splice_closure")
            # arcpy.conversion.FeatureClassToFeatureClass(Splice_Closures, p1, "primary_splice_closure", '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ2 "OBJ2" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')

        with arcpy.da.UpdateCursor("primary_splice_closure", ("enc_type"))as cursor:
            for row in cursor:
                if row[0] != None:
                    if row[0] not in (1,5):
                        cursor.deleteRow()
            del cursor

        arcpy.SpatialJoin_analysis("primary_boundary", "primary_splice_closure", "SJ_primary_boundary",
                                   join_operation='JOIN_ONE_TO_ONE', join_type='KEEP_ALL', field_mapping='',
                                   match_option='INTERSECT')
        if arcpy.Exists("SJ_primary_boundary"):
            lstFields_sc = arcpy.ListFields("SJ_primary_boundary")
            field_names_sc = [f.name for f in lstFields_sc]
            if "Error" not in field_names_sc:
                arcpy.AddField_management("SJ_primary_boundary", "Error", "Text", 100)

        with arcpy.da.UpdateCursor("SJ_primary_boundary", ("Join_Count", "Error"))as cursor:
            for row in cursor:
                if row[0] != None:
                    if row[0] != 1:
                        row[1] = "More/Less than one ODP1 in Primary Boundary"
                        cursor.updateRow(row)
            del cursor
        try:
            arr_SJ_primary_boundary = arcpy.da.FeatureClassToNumPyArray("SJ_primary_boundary", ["TARGET_FID", "Error"], skip_nulls=True)
            arcpy.AddMessage("arr_SJ_primary_boundary")
            arcpy.AddMessage(arr_SJ_primary_boundary)
        except Exception as e:
            arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final primary boundary Log File-  ")
            for data1 in arr_SJ_primary_boundary:
                with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Error")) as cursor:
                    for row in cursor:
                        if str(row[0]) != "None":
                            if str(row[0]) == str(data1[0]):
                                if row[1] != None:
                                    row[1] = str(row[1]) + ',' + str(data1[1])
                                else:
                                    row[1] = str(data1[1])
                            cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final primary boundary Log File- BoundaryPremise:  " + str(e))

    def __del__(self):
        #### Object Remove Here ######

        if arcpy.Exists("primary_splice_closure"):
            arcpy.Delete_management("primary_splice_closure")

        if arcpy.Exists("primary_boundary"):
            arcpy.Delete_management("primary_boundary")

        if arcpy.Exists("SJ_primary_boundary"):
            arcpy.Delete_management("SJ_primary_boundary")


class ParentItem2(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 3.3.2')

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase
        p3 = p1 + "\\" + "secondary_boundary"

        p2 = p1 + "\\" + self.Output_name
        if not arcpy.Exists(self.Output_name):
            feats = arcpy.FeatureSet(table=boundaries)
            feats.save(p2)

        try:
            if arcpy.Exists(self.Output_name):
                lstFields_sc = arcpy.ListFields(self.Output_name)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error", "TEXT",100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))


        if not arcpy.Exists("secondary_boundary"):
            feats = arcpy.FeatureSet(table=boundaries)
            feats.save(p3)

        with arcpy.da.UpdateCursor("secondary_boundary", ("level_"))as cursor:
            for row in cursor:
                if row[0] != None:
                    if row[0] != 3:
                        cursor.deleteRow()
            del cursor
        if not arcpy.Exists("secondary_splice_closure"):
            arcpy.CopyFeatures_management(Splice_Closures,"secondary_splice_closure")
            # arcpy.conversion.FeatureClassToFeatureClass(Splice_Closures, p1, "primary_splice_closure", '', r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,GlobalID,-1,-1;label "Label" true true false 8 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,label,0,8;enc_type "Enclosure Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_type,-1,-1;enc_size "Enclosure Size " true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,enc_size,-1,-1;stru_label "Structure Label" true true false 15 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,stru_label,0,15;placement "Placement" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,placement,-1,-1;sp1_type "SP1 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_type,-1,-1;sp1_label "SP1 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp1_label,0,16;sp2_type "SP2 Type" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_type,-1,-1;status "Status" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,status,-1,-1;reel_end "Reel End?" true true false 0 Long 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,reel_end,-1,-1;sp2_label "SP2 Label" true true false 16 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,sp2_label,0,16;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,Editor,0,128;OBJ2 "OBJ2" true true false 255 Text 0 0,First,#,Dunboyne_Test_0309\Splice_Closure,OBJECTID,-1,-1', '')

        with arcpy.da.UpdateCursor("secondary_splice_closure", ("enc_type"))as cursor:
            for row in cursor:
                if row[0] != None:
                    if row[0] not in (2,5):
                        cursor.deleteRow()
            del cursor

        arcpy.SpatialJoin_analysis("secondary_boundary", "secondary_splice_closure", "SJ_secondary_boundary",
                                   join_operation='JOIN_ONE_TO_ONE', join_type='KEEP_ALL', field_mapping='',
                                   match_option='INTERSECT')
        if arcpy.Exists("SJ_secondary_boundary"):
            lstFields_sc = arcpy.ListFields("SJ_secondary_boundary")
            field_names_sc = [f.name for f in lstFields_sc]
            if "Error" not in field_names_sc:
                arcpy.AddField_management("SJ_secondary_boundary", "Error", "Text", 100)

        with arcpy.da.UpdateCursor("SJ_secondary_boundary", ("Join_Count", "Error"))as cursor:
            for row in cursor:
                if row[0] != None:
                    if row[0] != 1:
                        row[1] = "More/Less than one ODP2 in Secondary Boundary"
                        cursor.updateRow(row)
            del cursor
        try:
            arr_SJ_secondary_boundary = arcpy.da.FeatureClassToNumPyArray("SJ_secondary_boundary", ["TARGET_FID", "Error"], skip_nulls=True)
            arcpy.AddMessage("arr_SJ_secondary_boundary")
            arcpy.AddMessage(arr_SJ_secondary_boundary)
        except Exception as e:
            arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final secondary boundary Log File-  ")
            for data1 in arr_SJ_secondary_boundary:
                with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Error")) as cursor:
                    for row in cursor:
                        if str(row[0]) != "None":
                            if str(row[0]) == str(data1[0]):
                                if row[1] != None:
                                    row[1] = str(row[1]) + ',' + str(data1[1])
                                else:
                                    row[1] = str(data1[1])
                            cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final secondary boundary Log File- ClosureBoundary:  " + str(e))

    def __del__(self):
        #### Object Remove Here ######
        # pass
        if arcpy.Exists("secondary_splice_closure"):
            arcpy.Delete_management("secondary_splice_closure")

        if arcpy.Exists("secondary_boundary"):
            arcpy.Delete_management("secondary_boundary")

        if arcpy.Exists("SJ_secondary_boundary"):
            arcpy.Delete_management("SJ_secondary_boundary")

class ParentItem3(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name
        self.splice_closure_copy = 'splice_closure_copy'
        self.primary_boundary = 'primary_boundary'
        self.primary_closure = 'primary_closure'
        self.secondary_closure = 'secondary_closure'
        self.QC3_Boundaries1 = 'QC3_Boundaries1'
        self.QC3_Boundaries2 = 'QC3_Boundaries2'
        self.QC3_Boundaries_copy = 'QC3_Boundaries_copy'
        self.QC3_Boundaries = 'QC3_Boundaries'

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 3.3.3')

        arcpy.CopyFeatures_management(Splice_Closures, self.splice_closure_copy)

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase
        p2 = p1 + '\\' + self.primary_boundary

        p3 = p1 + "\\" + self.Output_name
        if not arcpy.Exists(self.Output_name):
            feats = arcpy.FeatureSet(table=boundaries)
            feats.save(p3)
        try:
            if arcpy.Exists(self.Output_name):
                lstFields_sc = arcpy.ListFields(self.Output_name)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        array_splice = {
            1: 4,
            2: 4,
            3: 8,
            4: 8,
            5: 0
        }


        # arcpy.Select_analysis(boundaries, self.primary_boundary, "level_ in (2)")
        if not arcpy.Exists(self.primary_boundary):
            feats = arcpy.FeatureSet(table=boundaries)
            feats.save(p2)

        with arcpy.da.UpdateCursor("primary_boundary", ("level_"))as cursor:
            for row in cursor:
                if row[0] != None:
                    if row[0] != 2:
                        cursor.deleteRow()
            del cursor

        arcpy.Select_analysis(Splice_Closures, self.primary_closure, "enc_type in (1, 5)")

        arcpy.AddField_management(self.primary_closure, "count1", "Long", field_length='500',
                                  field_is_nullable='NULLABLE')

        arcpy.Select_analysis(Splice_Closures, self.secondary_closure, "enc_type in (2, 5)")

        arcpy.AddField_management(self.secondary_closure, "count2", "Long", field_length='500',
                                  field_is_nullable='NULLABLE')
        arcpy.AddField_management(self.secondary_closure, "sp_count", "Long", field_length='500',
                                  field_is_nullable='NULLABLE')

        with arcpy.da.UpdateCursor(self.secondary_closure, ("sp_count", "enc_type", "sp1_type", "sp2_type", "OBJECTID"))as cursor:
            for rows in cursor:
                if rows[1] == 5:
                    rows[0] = 1
                    cursor.updateRow(rows)
                if rows[1] == 2:
                    if str(rows[2]) != '5' and str(rows[3]) != '5':
                        arcpy.AddMessage(str(rows[2]) + ";" + str(rows[3]) + "--"+ str(rows[4]))
                        rows[0] = 2
                    elif str(rows[2]) == '5':
                        rows[0] = 0
                    elif str(rows[2]) != '5' and str(rows[3]) == '5':
                        rows[0] = 1
                    cursor.updateRow(rows)
            del cursor
        #
        arcpy.SpatialJoin_analysis(self.primary_boundary, self.primary_closure, self.QC3_Boundaries1, "JOIN_ONE_TO_ONE", "KEEP_ALL", "", "INTERSECT")
        arcpy.SpatialJoin_analysis(self.primary_boundary, self.secondary_closure, self.QC3_Boundaries2, "JOIN_ONE_TO_MANY", "KEEP_ALL", "", "INTERSECT")

        with arcpy.da.UpdateCursor(self.QC3_Boundaries1, ("enc_type", "sp1_type", "sp2_type", "count1"))as cursor:
            for rows in cursor:
                if rows[0] == 1 or rows[0] == 5:
                    if rows[1] != None:
                        rows[3] = array_splice[rows[1]]
                    cursor.updateRow(rows)
            del cursor

        try:
            with arcpy.da.UpdateCursor(self.QC3_Boundaries2, ("enc_type", "sp1_type", "sp2_type", "count2", "name"))as cursor:
                for rows in cursor:
                    if rows[0] == 2:
                        if rows[1] != None:
                            rows[3] = array_splice[rows[1]]
                        cursor.updateRow(rows)
                    if rows[0] == 5:
                        if rows[2] != None:
                            rows[3] = array_splice[rows[2]]
                        cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Line 324: "+ str(e))
        #
        #
        try:
            arcpy.analysis.SummarizeWithin(self.QC3_Boundaries1, self.QC3_Boundaries2, self.QC3_Boundaries_copy, keep_all_polygons='KEEP_ALL',  sum_fields=[['sp_count', 'SUM']])
            arcpy.AddField_management(self.QC3_Boundaries_copy, "sec_split_port", "Text", field_length='500', field_is_nullable='NULLABLE')
        except arcpy.ExecuteError as ex:
            status = False
            print(ex)

        # Make One To One Relation & Update Cursor With Error Log#
        # try:
        arcpy.SpatialJoin_analysis(self.QC3_Boundaries_copy, self.QC3_Boundaries2, self.QC3_Boundaries, "JOIN_ONE_TO_ONE",
                                   "KEEP_ALL", "", "INTERSECT")

        if arcpy.Exists(self.QC3_Boundaries):
            lstFields_sc = arcpy.ListFields(self.QC3_Boundaries)
            field_names_sc = [f.name for f in lstFields_sc]
            if "Error" not in field_names_sc:
                arcpy.AddField_management(self.QC3_Boundaries, "Error", "Text", 100)

        '''with arcpy.da.UpdateCursor(self.QC3_Boundaries,
                                   ("Error", "enc_type_1", "enc_type", "count1", "count2", "SUM_count2"))as cursor:
            for rows in cursor:
                if rows[3] == 8 and rows[4] == 4 and rows[5] > 32:
                    rows[0] = "More secondary splitters than outputs of primary splitter"
                elif rows[3] == 4 and rows[4] == 8 and rows[5] > 32:
                    rows[0] = "More secondary splitters than outputs of primary splitter"
                elif rows[3] == 4 and rows[4] == 4 and rows[5] > 16:
                    rows[0] = "More secondary splitters than outputs of primary splitter"
                elif rows[3] == 8 and rows[4] == None and rows[5] == 0:
                    rows[0] = "More secondary splitters than outputs of primary splitter"
                cursor.updateRow(rows)
            del cursor'''

        with arcpy.da.UpdateCursor(self.QC3_Boundaries,
                                   ("Error", "enc_type_1", "enc_type", "count1", "count2", "SUM_sp_count"))as cursor:
            for rows in cursor:
                if rows[3] != None and rows[5] != None:
                    if rows[3] == 8 and rows[4] == 4 and rows[5] > rows[3]:
                        rows[0] = "More secondary splitters than outputs of primary splitter"
                    elif rows[3] == 4 and rows[4] == 8 and rows[5] > rows[3]:
                        rows[0] = "More secondary splitters than outputs of primary splitter"
                    elif rows[3] == 4 and rows[4] == 4 and rows[5] > rows[3]:
                        rows[0] = "More secondary splitters than outputs of primary splitter"
                    elif rows[3] == 8 and rows[4] == None and rows[5] > rows[3]:
                        rows[0] = "More secondary splitters than outputs of primary splitter"
                    cursor.updateRow(rows)
            del cursor

        fieldList = arcpy.ListFields(self.QC3_Boundaries)
        fieldsToBeDeleted = []
        for field in fieldList:
            if str(field.name) not in (
            ["OBJECTID", "Error", "enc_type_1", "enc_type", "count1", "count2", "sum_sp_count", "OBJ1", "Shape_Length", "SHAPE", "Shape", "SHAPE_Length",
             "Shape_Area",
             "SHAPE_Area"]):
                fieldsToBeDeleted.append(field.name)
        # if arcpy.Exists(self.QC3_Boundaries):
        #     arcpy.DeleteField_management(self.QC3_Boundaries, fieldsToBeDeleted)

        # try:
        #     arcpy.SpatialJoin_analysis(self.splice_closure_copy, self.QC3_Boundaries, "Spatial_JOIN_Output", "JOIN_ONE_TO_ONE", "KEEP_ALL", "", "INTERSECT")
        #
        # except Exception as e:
        #     arcpy.AddMessage(e)

        '''try:
            with arcpy.da.UpdateCursor("Spatial_JOIN_Output",
                                       ("Error", "enc_type_1", "enc_type", "count1", "count2", "SUM_count2"))as cursor:
                for rows in cursor:
                    if rows[3] == 8 and rows[4] == 4 and rows[5] > 32:
                        rows[0] = "More secondary splitters than outputs of primary splitter"
                    elif rows[3] == 4 and rows[4] == 8 and rows[5] > 32:
                        rows[0] = "More secondary splitters than outputs of primary splitter"
                    elif rows[3] == 4 and rows[4] == 4 and rows[5] > 16:
                        rows[0] = "More secondary splitters than outputs of primary splitter"
                    elif rows[3] == 8 and rows[4] == None and rows[5] == 0:
                        rows[0] = "More secondary splitters than outputs of primary splitter"
                    cursor.updateRow(rows)
                del cursor

        except arcpy.ExecuteError as ex:
            status = False
            print(ex)
        except Exception as e:
            status = False
            print(e)'''

        try:
            self.arr_QC3_Boundaries = arcpy.da.FeatureClassToNumPyArray(self.QC3_Boundaries, ["TARGET_FID", "Error"], skip_nulls=True)
            #arcpy.AddMessage(self.arr_QC3_Boundaries)
            self.filtered_arr_QC3_Boundaries = self.remove_duplicates_None_values(self.arr_QC3_Boundaries)
            arcpy.AddMessage(self.filtered_arr_QC3_Boundaries)
        except Exception as e:
            arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final Log File-  ")
            for x in self.filtered_arr_QC3_Boundaries:
                with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Error")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            arcpy.AddMessage(str(row[0]))
                            if row[1] != None:
                                msg = str(row[1])
                                row[1] = msg + "," + x[1]
                            elif row[1] == None:
                                row[1] = x[1]
                                print(x[1])
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final  Log File- ClosureType_vs_SPR:  " + str(e))

    def __del__(self):
        # #### Object Remove Here ######
        # pass
        # try:
        if arcpy.Exists(self.primary_boundary):
            arcpy.Delete_management(self.primary_boundary)

        if arcpy.Exists(self.secondary_closure):
            arcpy.Delete_management(self.secondary_closure)

        if arcpy.Exists(self.primary_closure):
            arcpy.Delete_management(self.primary_closure)

        if arcpy.Exists(self.QC3_Boundaries1):
            arcpy.Delete_management(self.QC3_Boundaries1)

        if arcpy.Exists(self.QC3_Boundaries2):
            arcpy.Delete_management(self.QC3_Boundaries2)

        if arcpy.Exists(self.QC3_Boundaries_copy):
            arcpy.Delete_management(self.QC3_Boundaries_copy)

        if arcpy.Exists(self.QC3_Boundaries):
            arcpy.Delete_management(self.QC3_Boundaries)

        if arcpy.Exists(self.splice_closure_copy):
            arcpy.Delete_management(self.splice_closure_copy)


        # except Exception as e:
        #     arcpy.AddMessage("Issue in Updating errors to self.spatial_join_secondary_boundary_secondary_closure.. 3.1.5 :  " + str(e))


class BaseClass(ParentItem1, ParentItem2):
    def __init__(self):
        from datetime import datetime

        now = datetime.now()
        start_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run Start's @" + start_time)

        if arcpy.Exists("ClosureBoundary"):
            arcpy.Delete_management("ClosureBoundary")

        ### Object Intialize Here ####
        if item3point3point1 == 'true':
            ParentItem1.__init__(self, 'ClosureBoundary')
            ParentItem1.ItemNo(self)
            ParentItem1.__del__(self)
        if item3point3point2 == 'true':
            ParentItem2.__init__(self, 'ClosureBoundary')
            ParentItem2.ItemNo(self)
            ParentItem2.__del__(self)
        if item3point3point3 == 'true':
            ParentItem3.__init__(self, 'ClosureBoundary')
            ParentItem3.ItemNo(self)
            ParentItem3.__del__(self)

    def remove_duplicates_None_values(self, array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[1] != 'None':
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1

    def remove_duplicates_zero_values(array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[2] != 0:
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1


    def __del__(self):
        ### Object Remove Here ######

        if item3point3point1 == 'true' or item3point3point2 == 'true' or item3point3point3 == 'true':
            fieldList = arcpy.ListFields(self.Output_name)
            fieldsToBeDeleted = []
            for field in fieldList:
                if str(field.name) not in (["OBJECTID", "label", "Shape", "name", "Warning", "Error", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area",
                         "SHAPE_Area", 'Shape__Area', 'Shape__Length']):
                    fieldsToBeDeleted.append(field.name)

            arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))

            # try:
            if arcpy.Exists(self.Output_name):
                arcpy.DeleteField_management(self.Output_name, fieldsToBeDeleted)
            # except Exception as e:
            #     print(e)
            # except arcpy.ExecuteError as ex:
            #     print(ex)
            arcpy.AddMessage("Finished Deleting unwanted fields of error ")

            try:
                with arcpy.da.UpdateCursor(self.Output_name, ("Error")) as cursor:
                    for row in cursor:
                        if row[0] == None:
                            cursor.deleteRow()
                    del cursor
                # if item3point3point1 == 'true' and 'true' not in (item3point3point2):
                #
                # elif item3point3point1 != 'true' and 'true' in (item3point3point2):
                #     with arcpy.da.UpdateCursor(self.Output_name, ("Error")) as cursor:
                #         for row in cursor:
                #             if row[0] == None:
                #                 cursor.deleteRow()
                #         del cursor
                # else:
                #     with arcpy.da.UpdateCursor(self.Output_name, ("Error")) as cursor:
                #         for row in cursor:
                #             if row[0] == None:
                #                 cursor.deleteRow()
                #         del cursor
            except Exception as ex:
                arcpy.AddMessage(ex)


            arcpy.AddMessage(str(self.Output_name) + " - Log File has been Generated")

        from datetime import datetime

        now = datetime.now()
        end_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run End's @" + end_time)


######### Get All Input Parameter ###########

arcpy.env.overwriteOutput = True

input_geodatabase = arcpy.GetParameterAsText(0)
Splice_Closures = arcpy.GetParameterAsText(1)
boundaries = arcpy.GetParameterAsText(2)

######## Checked Input #########

item3point3point1 = arcpy.GetParameterAsText(3)
item3point3point2 = arcpy.GetParameterAsText(4)
item3point3point3 = arcpy.GetParameterAsText(5)

######### Oject Creation #######
# main function
if __name__ == '__main__':
    if arcpy.Exists("ClosureBoundary"):
        arcpy.Delete_management("ClosureBoundary")
    # created the object for BaseClass
    ob = BaseClass()
