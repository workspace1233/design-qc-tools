import arcpy
import math
import sys
from datetime import datetime
x=10000
sys.setrecursionlimit(x)

now = datetime.now()
start_time = now.strftime("%H:%M:%S")


# arcpy.env.workspace = "E:\\Rajesh\\Development_Internal\\Development.gdb"
arcpy.env.overwriteOutput = True

input_geodatabase = arcpy.GetParameterAsText(0)
Splice_Closures_copy = arcpy.GetParameterAsText(1)
Fibre_Sheath_copy = arcpy.GetParameterAsText(2)

inputCheckfor4point1 = arcpy.GetParameterAsText(3)

Fibre_Sheath = "Fibre_Sheath_copy"
Splice_Closures = "Splice_Closures_copy"

Fibre_Sheath_New = "CableSize"
#####################

if inputCheckfor4point1 == 'true':
    if arcpy.Exists(Fibre_Sheath_New):
        arcpy.Delete_management(Fibre_Sheath_New)

    try:
        arcpy.CopyFeatures_management(Splice_Closures_copy, Splice_Closures)
        arcpy.CopyFeatures_management(Fibre_Sheath_copy, Fibre_Sheath)
    except Exception as e:
        print(e)

    fields_sc = ("OriginalOID", "label", "sp1_label", "sp2_label", "SHAPE@XY", "enc_type")
    fields_fs = ("OriginalOID", "label", "from_", "SHAPE@XY")
    try:
        SR = arcpy.Describe(Fibre_Sheath).spatialReference
    except Exception as e:
        arcpy.AddMessage(e)


    try:
        lstFields_fs = arcpy.ListFields(Fibre_Sheath)
        field_names_fs = [f.name for f in lstFields_fs]
        if "OriginalOID" not in field_names_fs:
            arcpy.AddField_management(Fibre_Sheath, "OriginalOID", "LONG", 10)
            arcpy.CalculateField_management(Fibre_Sheath, "OriginalOID", "!OBJECTID!", "PYTHON3")
        else:
            pass

        lstFields_sc = arcpy.ListFields(Splice_Closures)
        field_names_sc = [f.name for f in lstFields_sc]
        if "OriginalOID" not in field_names_sc:
            arcpy.AddField_management(Splice_Closures, "OriginalOID", "LONG", 10)
            arcpy.CalculateField_management(Splice_Closures, "OriginalOID", "!OBJECTID!", "PYTHON3")
        else:
            pass

        arr_sc = arcpy.da.FeatureClassToNumPyArray(Splice_Closures, fields_sc, skip_nulls=False)
        # arcpy.AddMessage("arr_sc")
        # arcpy.AddMessage(arr_sc)

        arr_fs = arcpy.da.FeatureClassToNumPyArray(Fibre_Sheath, fields_fs, skip_nulls=True, explode_to_points=True)
        # print(arr_fs)

    except Exception as e:
        arcpy.AddMessage(e)

        ###########################
    def distance(p0, p1):
        # p0, p1 = points
        return math.sqrt((p0[0] - p1[0])**2 + (p0[1] - p1[1])**2)

    #Flattening list of lists into a list
    def flatt_list(list_of_list):
        List_flat = []

        for i in range(len(list_of_list)):  # Traversing through the main list
            for j in range(len(list_of_list[i])):  # Traversing through each sublist
                List_flat.append(tuple(list_of_list[i][j]))
        return List_flat

    #Find TO splice closure label from label of fiber sheath
    def find_sc_from_fs(label_fs1):
        label_sc1 = label_fs1[0:8]
        return label_sc1

    #Find last splice closer label from label of fiber sheath
    def find_from_sc_from_fs(label_fs1, arr_fs1):
        for x in arr_fs1:
            if label_fs1 == x[1]:
                label_sc1 = x[2]
        return label_sc1

    #Find out going fiber sheaths from label of splice closure
    # in downward direction
    def find_front_fs_from_sc(label_sc1, arr_fs1):
        #create a blank array
        label_fs1_list = []
        ##check equality with 'from' field of fiber sheath array
        for x in arr_fs1:
            if label_sc1 == x[2]:
                if x[1] not in label_fs1_list:
                    label_fs1_list.append(x[1])
        return label_fs1_list

    #Find last fiber sheath from label of splice closure
    #in upward direction
    def find_back_fs_from_sc(label_sc1, arr_fs1):
        ##match with fiber sheath array
        label_fs1 = ""
        for x in arr_fs1:
            if label_sc1 in x[1]:
                label_fs1 = x[1]
        return label_fs1


    #Find SP1 splitter fitted in the splice closure
    def find_SP1_splitter(label_sc1,arr_sc1):
        global label_sp1
        for x in arr_sc1:
            if x[1] == label_sc1:
                label_sp1 = x[2]
                #if label_sp1 == None:
                    #label_sp1 = 'raj'
                return label_sp1

    #Find SP1 splitter and SP2 splitter fitted in the splice closure
    #create a list
    def find_SP12_splitter(label_sc1,arr_sc1):
        global label_sp1
        global label_sp2
        for x in arr_sc1:
            if x[1] == label_sc1:
                #label_sp1 = x[2][9:15]
                #label_sp2 = x[3][9:15]
                if x[2] != None:
                    label_sp1 = x[2][9:15]
                elif x[2] == None:
                    label_sp1 = ""
                if x[3] != None:
                    label_sp2 = x[3][9:15]
                elif x[3] == None:
                    label_sp2 = ""
                return (label_sp1, label_sp2)
    #################

    #Find Enc_type of the splice closure
    def find_enc_type(label_sc1,arr_sc1):
        global enc_type1
        for x in arr_sc1:
            if x[1] == label_sc1:
                enc_type1 = x[5]
                #if label_sp1 == None:
                    #label_sp1 = 'raj'
                return enc_type1

    ################ To Find Recommended Cable Size ############
    def NewSize(backhaul, feeder):
        # Calculate total of backhaul/feeder fibres
        if backhaul == 1:
            total = feeder + 12
        elif backhaul == 2:
            total = feeder

        # Calculate fibre size based on feeder
        if feeder == 0 and backhaul == 2:
            return 12
        elif feeder == 0 and backhaul == 1:
            return 24
        elif feeder > 0 and feeder <= 9 and backhaul == 2:
            return 24
        elif feeder > 0 and feeder <= 9 and backhaul == 1:
            return 48
        elif feeder > 9 and total <= 24:
            return 48
        elif total > 24 and total <= 60:
            return 96
        elif total > 60 and total <= 96:
            return 144
        elif total > 96:
            return 999

    ##########################

    # Recursive function to collect all ODP1 splice clolsure
    # it will take input of splice closure and find the sheaths connected with from Label and From fields
    def find_next(fiber_list_checked, label_sc1, arr_sc1, arr_fs1, ODP1_list):
        # input("Press Enter to continue.")
        #print("**********************************")
        #print(label_sc1)
        # drop_path_list = drop_path_list[:(drop_path_list.index(label_sc1) + 1)]
        #print(ODP1_list)
        fiber_front_list = find_front_fs_from_sc(label_sc1, arr_fs1)
        #print(fiber_front_list)
        #remove fibers which are already covered
        for x in fiber_front_list:
            if x in fiber_list_checked:
                fiber_front_list.remove(x)
        #drop_path_list = drop_path_list1
        if len(fiber_front_list) > 0:
            for x in fiber_front_list:
                if x not in fiber_list_checked:
                    fiber_list_checked.append(x)
                    label_sc1 = find_sc_from_fs(x)
                    #print(label_sc1)
                    if label_sc1 != 'OLT':
                        enclosure_type = find_enc_type(label_sc1, arr_sc)
                        if enclosure_type == 1 or enclosure_type == 5:
                            ODP1_list.append(label_sc1)
                        ODP1_list = find_next(fiber_list_checked, label_sc1, arr_sc1, arr_fs1, ODP1_list)
        #print(ODP1_list)
        #print("check")
        return ODP1_list


    def list_sp1(arr_sc1, ODP1_list):
        splitter1_name = []
        for items in ODP1_list:
            for data in arr_sc1:
                if data[0] != None and data[1] != None and data[2] != 'None':
                    if str(data[1]) == str(items):
                        sp1_label = str(data[2])
                        ribbon_letter = (sp1_label[4:5])
                        complete_name = str(ribbon_letter)
                        splitter1_name.append(str(complete_name))
                        splitter1_name.sort()


        # arcpy.AddMessage("splitter1_name : ")
        # arcpy.AddMessage(splitter1_name)

        return splitter1_name

    def getValue(data , id):
        array = []
        # arcpy.AddMessage("id")
        # arcpy.AddMessage(id)
        for j in data:
            # arcpy.AddMessage("j[1]")
            # arcpy.AddMessage(j[1])
            if str(j[1]) == str(id):
                array.append(j[0])
        sum1 = sum(array)
        # arcpy.AddMessage("sum")
        # arcpy.AddMessage(sum1)
        # arcpy.AddMessage("array")
        # arcpy.AddMessage(array)

        return sum1


    def remove_duplicate_items_from_ODP1_list_all(array):
        label_array = []
        rest_items_array = []

        if len(array) != 0:
            for items in array:
                if len(label_array) == 0:
                    label_array.append(items[0])
                    rest_items_array.append(items)
                elif items[0] not in label_array:
                    label_array.append(items[0])
                    rest_items_array.append(items)
                else:
                    pass
        return rest_items_array


    def count_labels(arr_sc):
        list = []
        new_list = []
        key_list = []
        for items in range(len(arr_sc)):
            # print(arr_sc[items])
            if arr_sc[items][5] in (1, 5):
                # print( arr_sc[items][1])
                list.append(arr_sc[items][1][4])
        print(list)
        from collections import Counter
        a = dict(Counter(list))
        print(a)
        import math
        for key in a:
            # print(key)
            n = a[key]
            print(n)
            gap = int(12 - math.fmod(n, 12))
            new_list.append([key, gap])
        print(new_list)
        arcpy.AddMessage("new_list")
        arcpy.AddMessage(new_list)
        for z in new_list:
            key_list.append(z[0])
        print(key_list)
        arcpy.AddMessage("key_list")
        arcpy.AddMessage(key_list)

        return [new_list, key_list]


    def finding2Ribbons(array1):
        asf = remove_duplicate_items_from_ODP1_list_all(array1)

        new_list = count_labels(arr_sc)[0]
        key_list = count_labels(arr_sc)[1]

        arcpy.AddMessage("asf")
        arcpy.AddMessage(asf)

        for_OLT_list = []
        asf_part2 = []

        print(asf)
        if len(asf) > 0:
            for items in asf:
                ribbon_splitter1 = items[2]
                if len(ribbon_splitter1) > 1:
                    # print(ribbon_splitter1)
                    from collections import Counter
                    d = dict(Counter(ribbon_splitter1))
                    # print(a)
                    # print(len(a.keys()))
                    if len(d.keys()) > 1:
                        if items[3] == 'OLT':
                            for key, value in d.items():
                                if key == items[0][4]:
                                    # print(str(key) + "  ,, "+ str(value))
                                    n = value
                                    fmod = int(math.fmod(n, 12))
                                    # print(fmod)
                                    if fmod != 0:
                                        gap_from_OLT = int(12 - fmod)
                                    else:
                                        gap_from_OLT = 0
                                    for_OLT_list.append([items[0], gap_from_OLT, key])
                                    asf_part2.append([items[0], gap_from_OLT])
                        elif items[3] != 'OLT':
                            arcpy.AddMessage("NOT OLT " + str(items[0]))
                            for data in for_OLT_list:
                                # print(data[2])
                                if str(items[0][4]) == str(data[2]):
                                    arcpy.AddMessage(str(data[2]) + " , "+ str(data[1]))
                                    asf_part2.append([items[0], data[1]])
                    if len(d.keys()) == 1:
                        gap_val = 0
                        asf_part2.append([items[0], gap_val])
        # print(for_OLT_list)
        # print(asf_part2)
        arcpy.AddMessage("for_OLT_list")
        arcpy.AddMessage(for_OLT_list)
        arcpy.AddMessage("asf_part2")
        arcpy.AddMessage(asf_part2)

        return asf_part2


    arcpy.AddMessage("Step 1: ODP 1 tracing begin...")

    ODP1_list_all = []
    for x in arr_fs:
        #print(x)
        fiber_list_checked = []
        ODP1_list = []
        next_splice_closure = x[1][0:8]
        enclosure_type = find_enc_type(next_splice_closure,arr_sc)
        #print(next_splice_closure)
        if enclosure_type == 1 or enclosure_type == 5:
            ODP1_list.append(next_splice_closure)
        ODP1_list = find_next(fiber_list_checked, next_splice_closure, arr_sc, arr_fs, ODP1_list)
            #print(ODP1_list)
        # arcpy.AddMessage("ODP1_list")
        # arcpy.AddMessage(ODP1_list)
    # ODP1_list_all.append([x[1], len(ODP1_list)])
        ODP1_list_all.append([x[1], len(ODP1_list), list_sp1(arr_sc, ODP1_list) , x[2]])

    # arcpy.AddMessage("ODP1_list_all")
    # arcpy.AddMessage(ODP1_list_all)

    # finding2Ribbons(ODP1_list_all)

    arcpy.AddMessage("Step1: ODP 1 tracing end...")

    arcpy.FeatureClassToFeatureClass_conversion(Fibre_Sheath, arcpy.env.workspace, Fibre_Sheath_New)
    if not arcpy.Exists(Fibre_Sheath_New):
        arcpy.AddMessage("Fibre_Sheath_New not there ")
        arcpy.management.CopyFeatures(Fibre_Sheath, Fibre_Sheath_New)
    if arcpy.Exists(Fibre_Sheath_New):
        arcpy.AddField_management(Fibre_Sheath_New, "type_copy", "Text", 4)
        arcpy.AddField_management(Fibre_Sheath_New, "backhaul_copy", "Text", 4)
        arcpy.AddField_management(Fibre_Sheath_New, "ODP1_GAP", "LONG", 4)
        arcpy.AddField_management(Fibre_Sheath_New, "ODP1_Total", "LONG", 4)
        arcpy.AddField_management(Fibre_Sheath_New, "ODP1_Count", "LONG", 4)

    # writing geometries to drop_path feature class
    arcpy.AddMessage("Step2: Writing ODP1 count in Fibre_Sheath_New...")
    for x in ODP1_list_all:
        #print(x)
        SC_label = x[0]
        with arcpy.da.UpdateCursor(Fibre_Sheath_New, ['label', 'ODP1_Count', 'ODP1_GAP' ,'ODP1_Total']) as cur:
            for row in cur:
                if row[0] == SC_label:
                    row[1] = x[1]
                    row[2] = 0
                    row[3] = 0
                cur.updateRow(row)
        del cur

    arcpy.AddMessage("Step2: Finished Writing ODP1 count in Fibre_Sheath_New...")

    arcpy.AddMessage("Step3: Calling Gap function and writing values in output file...")
    ribbon = finding2Ribbons(ODP1_list_all)
    arcpy.AddMessage("ribbon")
    arcpy.AddMessage(len(ribbon))
    if ribbon != None:
        if len(ribbon) > 0:
            for data in ribbon:
                if data != None:
                    # arcpy.AddMessage("data")
                    # arcpy.AddMessage(data)
                    # arcpy.AddMessage("data[0]")
                    # arcpy.AddMessage(data[0])
                    with arcpy.da.UpdateCursor(Fibre_Sheath_New, ['label', 'ODP1_Count', 'ODP1_GAP', 'ODP1_Total']) as cur:
                        for row in cur:
                            if len(ribbon) != 0:
                                if str(row[0]) == str(data[0]):
                                    arcpy.AddMessage("inside match " + str(row[0]) + ' ' + str(data[1]))
                                    row[2] = int(data[1])
                                cur.updateRow(row)
                        del cur
    else:
        arcpy.AddMessage("empty ribbon")

    with arcpy.da.UpdateCursor(Fibre_Sheath_New, ['OriginalOID', 'ODP1_Count', 'ODP1_GAP', 'ODP1_Total']) as cur:
        for row in cur:
            if row[1] != None and row[2] != None and row[3] != None:
                row[3] = int(row[1]) + int(row[2])
                cur.updateRow(row)
        del cur

        arcpy.AddMessage("Step3: Finished Calling Gap function and writing values in output file...")

        ##remove added field
    lstFields_fs1 = arcpy.ListFields(Fibre_Sheath)
    field_names_fs1 = [f.name for f in lstFields_fs1]
    if "OriginalOID" in field_names_fs1:
        arcpy.DeleteField_management(Fibre_Sheath, ["OriginalOID"])
    else:
        pass

    lstFields_sc1 = arcpy.ListFields(Splice_Closures)
    field_names_sc1 = [f.name for f in lstFields_sc1]
    if "OriginalOID" in field_names_sc1:
        arcpy.DeleteField_management(Splice_Closures, ["OriginalOID"])
    else:
        pass

    ################ Code To Chek Recomended Cable Size ################
    arcpy.AddMessage("Step 4 (Final step) : Processing Error and finishing output file generation...")

    if arcpy.Exists(Fibre_Sheath_New):
        arcpy.AddField_management(Fibre_Sheath_New, "Error", "Text", 400)
        arcpy.AddField_management(Fibre_Sheath_New, "Recommended_cable_size", "Text", 4)

    try:
        array_qc4_fibre = arr_sc = arcpy.da.FeatureClassToNumPyArray(Fibre_Sheath_New,
                                                                     ["ODP1_Total", "backhaul", "label"],
                                                                     skip_nulls=True)
    except Exception as e:
        arcpy.AddMessage(e)

    # print(array_qc4_fibre)
    cable_size = {
        1: 12,
        2: 24,
        3: 48,
        4: 96,
        5: 144,
        6: 12,
        7: 24,
        8: 48,
        9: 96,
        10: 144
    }

    cable_type = {
        1: 'FCA12',
        2: 'FCA24',
        3: 'FCA48',
        4: 'FCA96',
        5: 'FCA144',
        6: 'FCB12',
        7: 'FCB24',
        8: 'FCB48',
        9: 'FCB96',
        10: 'FCB144'
    }
    try:
        for data in array_qc4_fibre:
            if data[0] != 'None' and data[1] != 'None':
                # recommended_size = NewSize(int(data[1]), int(data[0]))

                with arcpy.da.UpdateCursor(Fibre_Sheath_New, ['label', 'ODP1_Total', 'type', 'backhaul', 'Error',
                                                              'Recommended_cable_size', 'type_copy',
                                                              'backhaul_copy']) as cur:
                    for row in cur:
                        if str(row[0]) == str(data[2]):
                            recommended_size = NewSize(int(data[1]), int(data[0]))
                            if row[3] == 1:
                                # c_size = "FCA"
                                backhaul = "Yes"
                            else:
                                # c_size = "FCB"
                                backhaul = "No"
                            n_type = cable_type[row[2]]
                            ###### Update Recommended Size #########
                            row[5] = n_type[:3] + str(recommended_size)
                            ########## Update Type & Backhaul #####
                            row[6] = n_type
                            row[7] = backhaul
                            if row[2] != None:
                                if recommended_size != cable_size[row[2]] and row[3] == 1:
                                    if row[1] == 0:
                                        if recommended_size > cable_size[row[2]]:
                                            row[4] = "Fibre Sheath size  too small, No ODP1 downstream but Backhaul in"
                                        else:
                                            row[4] = "Fibre Sheath size too large, No ODP1 downstream but Backhaul in"
                                    else:
                                        if recommended_size > cable_size[row[2]]:
                                            row[4] = "Fibre Sheath with Backhaul, too small for count of ODP downstream"
                                        else:
                                            row[4] = "Fibre Sheath with Backhaul, too large for count of ODP downstream"
                                elif recommended_size != cable_size[row[2]] and row[3] == 2:
                                    if row[1] == 0:
                                        if recommended_size > cable_size[row[2]]:
                                            row[4] = "Fibre Sheath size  too small, No ODP1 downstream"
                                        else:
                                            row[4] = "Fibre Sheath size too large, No ODP1 downstream"
                                    else:
                                        if recommended_size > cable_size[row[2]]:
                                            row[4] = "Fibre Sheath too small for count of ODP downstream"
                                        else:
                                            row[4] = "Fibre Sheath too large for count of ODP downstream"

                            cur.updateRow(row)
                    del cur
    except Exception as e:
        arcpy.AddMessage(e)

    arcpy.AddMessage("Step 4 (Final step) : Processing Error and finishing output file generation complete...")

    # try:
    #     fieldList = arcpy.ListFields(Fibre_Sheath_New)
    #     fieldsToBeDeleted = []
    #     for field in fieldList:
    #         if str(field.name) not in (
    #                 ["OBJECTID", "OriginalOID", "Shape", "ODP1_GAP", "ODP1_Total",  "Shape_Area", "type_copy", "backhaul_copy","Error", "SHAPE_Length", "Shape_Length", "label", "SHAPE", "Error", "ODP1_Count", "Recommended_cable_size"]):
    #             fieldsToBeDeleted.append(field.name)
    #     arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))
    #     arcpy.DeleteField_management(Fibre_Sheath_New, fieldsToBeDeleted)
    #     arcpy.AddMessage("Finished Deleting unwanted fields from "+ str(Fibre_Sheath_New))
    #
    #     with arcpy.da.UpdateCursor(Fibre_Sheath_New, ("Error")) as cursor:
    #         for row in cursor:
    #             if row[0] == None:
    #                 cursor.deleteRow()
    #         del cursor
    # except Exception as e:
    #     status = False
    #     arcpy.AddMessage("Issue Occour while Deleting Fields " + str(e))

    # if arcpy.Exists(Fibre_Sheath):
    #     arcpy.Delete_management(Fibre_Sheath)
    # if arcpy.Exists(Splice_Closures):
    #     arcpy.Delete_management(Splice_Closures)
    #
    # if arcpy.Exists(Fibre_Sheath_New):
    #     for fieldName in [f.name for f in arcpy.ListFields(Fibre_Sheath_New)]:
    #         if fieldName == "type_copy":
    #             arcpy.AlterField_management(Fibre_Sheath_New, fieldName, "Type", "Type")
    #         if fieldName == "backhaul_copy":
    #             arcpy.AlterField_management(Fibre_Sheath_New, fieldName, "Backhaul", "Backhaul")


    now = datetime.now()
    end_time = now.strftime("%H:%M:%S")
    print("\nStart Time =", start_time)
    print("\nEnd Time =", end_time)
