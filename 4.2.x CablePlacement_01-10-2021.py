import arcpy

arcpy.env.overwriteOutput = True

########### The Output should come To Point Layer ############
class ParentItem2point1(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        arcpy.AddMessage("enter into Fibre Check in 4.2.1")
        self.Output_name = name
        self.temp_file = 'temp_file'
        self.olt_copy = "olt_copy"
        self.splice_closure_copy = "splice_closure_copy"
        self.fibre_sheath_copy = "fibre_sheath_copy"

    def ItemNo(self):
        arcpy.CopyFeatures_management(fibre_sheath, self.fibre_sheath_copy)
        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase
        p2 = p1 + "\\" + self.Output_name
        if not arcpy.Exists(self.Output_name):
            # arcpy.conversion.FeatureClassToFeatureClass(boundaries,
            #                                             p1,
            #                                             self.Output_name, '',
            #                                             r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,GlobalID,-1,-1;level_ "Level" true true false 0 Long 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,level_,-1,-1;name "Name" true true false 20 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,name,0,20;homes "Number of Homes in Boundary" true true false 0 Long 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,homes,-1,-1;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Editor,0,128;Shape__Area "Shape__Area" false true true 0 Double 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Shape__Area,-1,-1;Shape__Length "Shape__Length" false true true 0 Double 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Shape__Length,-1,-1;CreationDate_1 "CreationDate" false true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,CreationDate_1,-1,-1;Creator_1 "Creator" false true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Creator_1,0,128;EditDate_1 "EditDate" false true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,EditDate_1,-1,-1;Editor_1 "Editor" false true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Editor_1,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,OBJECTID,-1,-1',
            #                                             '')
            feats = arcpy.FeatureSet(table=fibre_sheath)
            feats.save(p2)
        arcpy.CopyFeatures_management(OLT, self.olt_copy)
        arcpy.CopyFeatures_management(splice_closure, self.splice_closure_copy)

        # lstFields_fibre = arcpy.ListFields(fibre_sheath)
        # field_names_fibre = [f.name for f in lstFields_fibre]
        # if "OriginalFID_fs" not in field_names_fibre:
        #     arcpy.AddField_management(self.fibre_sheath_copy, "OriginalFID_fs", "LONG", 10)
        #     arcpy.CalculateField_management(self.fibre_sheath_copy, "OriginalFID_fs", "!OBJECTID!", "PYTHON3")
        #
        # lstFields_splice = arcpy.ListFields(splice_closure)
        # field_names_splice = [f.name for f in lstFields_splice]
        # if "OriginalFID_sc" not in field_names_splice:
        #     arcpy.AddField_management(self.splice_closure_copy, "OriginalFID_sc", "LONG", 10)
        #     arcpy.CalculateField_management(self.splice_closure_copy, "OriginalFID_sc", "!OBJECTID!", "PYTHON3")

        # arcpy.analysis.CountOverlappingFeatures(self.splice_closure_copy, "splice_closure_copy_overlaps", 2,
        #                                         "splice_closure_copy_overlaps_table")
        #
        # arcpy.management.JoinField(self.splice_closure_copy, "OBJECTID", "splice_closure_copy_overlaps_table", "ORIG_OID",  ["OVERLAP_OID"])
        #
        # try:
        #     if arcpy.Exists(self.splice_closure_copy):
        #         lstFields_sc = arcpy.ListFields(self.splice_closure_copy)
        #         field_names_sc = [f.name for f in lstFields_sc]
        #         if "Count" not in field_names_sc:
        #             arcpy.AddField_management(self.splice_closure_copy, "Count", "Long", 10)
        # except Exception as e:
        #     arcpy.AddMessage("Issue in Adding field (Count) in self.splice_closure_copy.. :  " + str(e))
        #
        # try:
        #     with arcpy.da.UpdateCursor(self.splice_closure_copy, ("Count")) as cursor:
        #         for rows in cursor:
        #             rows[0] = 1
        #             cursor.updateRow(rows)
        #         del cursor
        # except Exception as e:
        #     arcpy.AddMessage("Issue in (Count) in self.splice_closure_copy.. :  " + str(e))
        #
        # try:
        #     array1 = []
        #     with arcpy.da.UpdateCursor(self.splice_closure_copy, ("Count", "OVERLAP_OID")) as cursor:
        #         for rows in cursor:
        #             if rows[1] != None:
        #                 if str(rows[1]) in array1:
        #                     rows[0] = int(rows[0]) + 1
        #                     array1.append(str(rows[1]))
        #                 else:
        #                     rows[0] = 1
        #                     array1.append(str(rows[1]))
        #                 cursor.updateRow(rows)
        #         del cursor
        # except Exception as e:
        #     arcpy.AddMessage("Issue in Updating Cursor (OVERLAP_OID) self.splice_closure_copy.. :  " + str(e))

        # with arcpy.da.UpdateCursor(self.splice_closure_copy, ["Count"]) as cursor:
        #     for row in cursor:
        #         if row[0] != None:
        #             if row[0] == 2:
        #                 cursor.deleteRow()
        #     del cursor

        if arcpy.Exists(self.fibre_sheath_copy):
            arcpy.AddField_management(self.fibre_sheath_copy, "start_point_x", "Text", 50)
            arcpy.AddField_management(self.fibre_sheath_copy, "start_point_y", "Text", 50)
            arcpy.AddField_management(self.fibre_sheath_copy, "end_point_x", "Text", 50)
            arcpy.AddField_management(self.fibre_sheath_copy, "end_point_y", "Text", 50)

        with arcpy.da.UpdateCursor(self.fibre_sheath_copy, ["start_point_x", "start_point_y", "end_point_x", "end_point_y", "SHAPE@"]) as cursor:
            for row in cursor:
                if row[4] != None:
                    start_point = row[4].firstPoint
                    end_point = row[4].lastPoint
                    row[0] = str(start_point.X)
                    row[1] = str(start_point.Y)
                    row[2] = str(end_point.X)
                    row[3] = str(end_point.Y)
                    cursor.updateRow(row)
            del cursor

        arcpy.SpatialJoin_analysis(self.splice_closure_copy, self.fibre_sheath_copy, "spat_join_output", join_operation='JOIN_ONE_TO_MANY', join_type='KEEP_ALL', match_option='INTERSECT')

        with arcpy.da.UpdateCursor("spat_join_output", ["Join_Count"]) as cursor:
            for row in cursor:
                if row[0] != None:
                    if row[0] == 0:
                        cursor.deleteRow()
            del cursor


        array_fibre = arcpy.da.FeatureClassToNumPyArray("spat_join_output", ["TARGET_FID", "end_point_x", "end_point_y", "JOIN_FID", "start_point_x"], skip_nulls = True)
        # arcpy.AddMessage(array_fibre)
        # for data in array_fibre:
        #     arcpy.AddMessage(str(data))

        if arcpy.Exists("spat_join_output"):
            arcpy.AddField_management("spat_join_output", "Warning", "Text", 100)

        with arcpy.da.UpdateCursor("spat_join_output",["JOIN_FID", "end_point_x", "end_point_y", "Warning", "TARGET_FID"]) as cursor:
            for row in cursor:
                if row[0] != None and row[1] != None and row[2] != None:
                    data = self.find_endpoints(array_fibre, row[4])
                    # arcpy.AddMessage(data)
                    if len(data) > 0:
                        # arcpy.AddMessage(data[0])
                        row[3] = ""
                        for i in data:
                            if str(i) == str(row[0]):
                                row[3] = "More than 1 Fibre Sheath end point connecting to same enclosure"
                                cursor.updateRow(row)
            del cursor

        array_spat_join_output = arcpy.da.FeatureClassToNumPyArray("spat_join_output", ["JOIN_FID", "Warning"],
                                                                   skip_nulls=True)
        # arcpy.AddMessage(array_spat_join_output)

        filtered_array_spat_join_output = self.remove_duplicates_None_values(array_spat_join_output)
        # arcpy.AddMessage(filtered_array_spat_join_output)

        #arcpy.AddField_management(self.Output_name, "Warning", "Text", 500)

        lstFields_sc = arcpy.ListFields(self.Output_name)
        field_names_fs = [f.name for f in lstFields_sc]

        if "Error1" not in field_names_fs:
            arcpy.AddMessage("adding field (Error1) in self.Output_name..")
            arcpy.AddField_management(self.Output_name, "Error1", "TEXT", 100)
        if "Warning1" not in field_names_fs:
            arcpy.AddMessage("adding field (Warning1) in self.Output_name..")
            arcpy.AddField_management(self.Output_name, "Warning1", "TEXT", 100)
        if "Warning" not in field_names_fs:
            arcpy.AddMessage("adding field (Warning) in self.Output_name..")
            arcpy.AddField_management(self.Output_name, "Warning", "TEXT", 100)

        for data in filtered_array_spat_join_output:
            with arcpy.da.UpdateCursor(self.Output_name, ["OBJECTID", "Warning", "Warning1"]) as cursor:
                for row in cursor:
                    if row[0] != None:
                        if str(row[0]) == str(data[0]):
                            if str(data[1]) != 'None' and row[1] != None:
                                msg = str(row[1]) + ',' + data[1]
                                row[1] = msg
                                row[2] = msg
                            elif str(data[1]) != 'None' and row[1] == None:
                                row[1] = data[1]
                                row[2] = data[1]
                        cursor.updateRow(row)
                del cursor

    def __del__(self):
        #### Object Remove Here ######
        if arcpy.Exists("fibre_sheath_copy"):
            arcpy.Delete_management("fibre_sheath_copy")
        if arcpy.Exists("olt_copy"):
            arcpy.Delete_management("olt_copy")
        if arcpy.Exists("spat_join_output"):
            arcpy.Delete_management("spat_join_output")
        if arcpy.Exists("splice_closure_copy"):
            arcpy.Delete_management("splice_closure_copy")
        if arcpy.Exists("splice_closure_copy_overlaps"):
            arcpy.Delete_management("splice_closure_copy_overlaps")
        if arcpy.Exists("splice_closure_copy_overlaps_table"):
            arcpy.Delete_management("splice_closure_copy_overlaps_table")

class ParentItem2(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name
        self.fibre_copy = 'fibre_copy'
        self.span_copy = 'span_copy'

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 4.2.2: Is there FCB (type == 6, 7, 8, 9, 10) in an aerial span?...')

        if not arcpy.Exists(self.Output_name):
            arcpy.CopyFeatures_management(fibre_sheath, self.Output_name)
        try:
            if arcpy.Exists(self.Output_name):
                lstFields_sc = arcpy.ListFields(self.Output_name)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error", "TEXT", 1500)
                if "Error1" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error1) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error1", "TEXT", 100)
                if "Warning1" not in field_names_sc:
                    arcpy.AddMessage("adding field (Warning1) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Warning1", "TEXT", 100)

                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        try:
            arcpy.CopyFeatures_management(span, self.span_copy)
            arcpy.CopyFeatures_management(fibre_sheath, self.fibre_copy)
            arcpy.AddField_management(self.fibre_copy, "Error", "TEXT", 100)
            arcpy.AddField_management(self.span_copy, "Mid_Point_X", "TEXT", 200)
            arcpy.AddField_management(self.span_copy, "Mid_Point_Y", "TEXT", 200)

            arcpy.Select_analysis(self.fibre_copy, "fibre_output", '"type" in (6,7,8,9,10)')
        except Exception as ex:
            arcpy.AddMessage(ex)

        try:
            with arcpy.da.UpdateCursor(self.span_copy, ("SHAPE@", "Mid_Point_X", "Mid_Point_Y")) as cursor:
                for row in cursor:
                    if row[0] != None:
                        midpoint = row[0].positionAlongLine(0.50, True).firstPoint
                        row[1] = str(midpoint.X)
                        row[2] = str(midpoint.Y)
                        cursor.updateRow(row)
                del cursor
        except Exception as ex:
            arcpy.AddMessage(ex)

        try:
            array_fibre = arcpy.da.FeatureClassToNumPyArray(self.span_copy, ["Mid_Point_X", "Mid_Point_Y"], skip_nulls=False)
        except Exception as ex:
            arcpy.AddMessage(ex)

        spatial_ref = arcpy.Describe(self.span_copy).spatialReference
        if spatial_ref.name == "Unknown":
            arcpy.AddMessage("{0} has an unknown spatial reference".format(self.span_copy))

        else:
            arcpy.AddMessage("{0} : {1}".format(self.span_copy, spatial_ref.PCSCode))

        try:
            pointList_mid = arcpy.Array()
            for record in array_fibre:
                if str(record[0]) != 'None':
                    mid_point = arcpy.Point(record[0], record[1])
                    pointList_mid.append([mid_point])
        except Exception as e:
            arcpy.AddMessage(e)
        # Create an empty Point object
        point = arcpy.Point()

        # A list to hold the PointGeometry objects
        pointGeometryList1 = []

        sr = arcpy.SpatialReference(spatial_ref.PCSCode)

        # For each coordinate pair, populate the Point object and create a new
        # PointGeometry object
        try:
            for pt in pointList_mid:
                for data in pt:
                    pointGeometry = arcpy.PointGeometry(data, sr)
                    pointGeometryList1.append(pointGeometry)
        except Exception as e:
            arcpy.AddMessage(e)

        # Create a copy of the PointGeometry objects, by using pointGeometryList as
        # input to the CopyFeatures tool.
        try:
            arcpy.CopyFeatures_management(pointGeometryList1, "mid_test_Geometry")
        except Exception as ex:
            arcpy.AddMessage(ex)

        ######### Spatial JOIN Mid Point And Fibre Shaeth #########
        try:

            arcpy.SpatialJoin_analysis("mid_test_Geometry", self.span_copy, "spatial_mid_span_copy", join_operation='JOIN_ONE_TO_ONE', join_type='KEEP_ALL', match_option='INTERSECT')
            arcpy.SpatialJoin_analysis("spatial_mid_span_copy", "fibre_output", "spatial_mid_copy", join_operation='JOIN_ONE_TO_MANY', join_type='KEEP_ALL', match_option='INTERSECT')

            array_mid_point = arcpy.da.FeatureClassToNumPyArray("spatial_mid_copy", ["label", "span_type", "type"], skip_nulls=True)
            filtered_array_mid_point = self.remove_duplicates_Span_values(array_mid_point)
        except Exception as ex:
            arcpy.AddMessage(ex)

        try:
            for data in filtered_array_mid_point:
                with arcpy.da.UpdateCursor(self.Output_name, ["label", "Error", "type", "Error1"]) as cursor:
                    for row in cursor:
                        if row[0] != None and str(data[0]) != "None":
                            if str(row[0]) == data[0] and str(row[2]) in ('6', '7', '8', '9', '10') and str(
                                    data[1]) == "1":
                                if row[1] != None:
                                    msg = str(row[1]) + ',' + "FCB cable on aerial span"
                                    row[1] = msg
                                    row[3] = msg
                                else:
                                    row[1] = "FCB cable on aerial span"
                                    row[3] = "FCB cable on aerial span"
                            cursor.updateRow(row)
                    del cursor
        except Exception as ex:
            arcpy.AddMessage(ex)


    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("Deleting Copy Features")
        if arcpy.Exists(self.fibre_copy):
            arcpy.Delete_management(self.fibre_copy)
        if arcpy.Exists(self.span_copy):
            arcpy.Delete_management(self.span_copy)
        if arcpy.Exists("mid_test_Geometry"):
            arcpy.Delete_management("mid_test_Geometry")
        if arcpy.Exists("spatial_mid_span_copy"):
            arcpy.Delete_management("spatial_mid_span_copy")
        if arcpy.Exists("spatial_mid_copy"):
            arcpy.Delete_management("spatial_mid_copy")
        if arcpy.Exists("fibre_output"):
            arcpy.Delete_management("fibre_output")

class ParentItem3(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name


    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 4.2.3')
        arcpy.AddMessage('Tool Check Run For item 4.2.4')

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase
        p2 = p1 + "\\" + self.Output_name
        if not arcpy.Exists(self.Output_name):
            # arcpy.conversion.FeatureClassToFeatureClass(boundaries,
            #                                             p1,
            #                                             self.Output_name, '',
            #                                             r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,GlobalID,-1,-1;level_ "Level" true true false 0 Long 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,level_,-1,-1;name "Name" true true false 20 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,name,0,20;homes "Number of Homes in Boundary" true true false 0 Long 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,homes,-1,-1;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Editor,0,128;Shape__Area "Shape__Area" false true true 0 Double 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Shape__Area,-1,-1;Shape__Length "Shape__Length" false true true 0 Double 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Shape__Length,-1,-1;CreationDate_1 "CreationDate" false true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,CreationDate_1,-1,-1;Creator_1 "Creator" false true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Creator_1,0,128;EditDate_1 "EditDate" false true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,EditDate_1,-1,-1;Editor_1 "Editor" false true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Editor_1,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,OBJECTID,-1,-1',
            #                                             '')
            feats = arcpy.FeatureSet(table= fibre_sheath)
            feats.save(p2)

        try:
            if arcpy.Exists(self.Output_name):
                lstFields_sc = arcpy.ListFields(self.Output_name)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error", "TEXT", 100)

                if "Error1" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error1) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error1", "TEXT", 100)
                if "Warning1" not in field_names_sc:
                    arcpy.AddMessage("adding field (Warning1) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Warning1", "TEXT", 100)
                if "Warning" not in field_names_sc:
                    arcpy.AddMessage("adding field (Warning) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Warning", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        arcpy.SpatialJoin_analysis(Fibre_Duct, pole, "SJ_DUCT_POLE", "JOIN_ONE_TO_ONE",
                                   "KEEP_ALL", "", "INTERSECT")
        arcpy.SpatialJoin_analysis("SJ_DUCT_POLE", chambers, "SJ_DUCT_POLE_CHAMBERS", "JOIN_ONE_TO_ONE",
                                   "KEEP_ALL", "", "INTERSECT")

        try:
            with arcpy.da.UpdateCursor("SJ_DUCT_POLE_CHAMBERS",
                                       ("Join_Count", "Join_Count_1"))as cursor:
                for rows in cursor:
                   if rows[0] > 0 and rows[1] > 0:
                    cursor.deleteRow()
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in SJ_DUCT_POLE_CHAMBERS.. :  " + str(e))

        try:
            arcpy.SpatialJoin_analysis(fibre_sheath, "SJ_DUCT_POLE_CHAMBERS", "SPATIAL_JOIN_OUTPUT_SHEATH_DUCT", "JOIN_ONE_TO_MANY",
                                       "KEEP_ALL", "", "SHARE_A_LINE_SEGMENT_WITH")
        except Exception as e:
            arcpy.AddMessage("Issue in SpatialJoin of Fibre_Duct and Duct_centre_points.. :  " + str(e))


        try:
            if arcpy.Exists("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT"):
                lstFields_sc = arcpy.ListFields("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (SPATIAL_JOIN_OUTPUT_SHEATH_DUCT) in joined_table..")
                    arcpy.AddField_management("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT", "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to SPATIAL_JOIN_OUTPUT_SHEATH_DUCT.. :  " + str(e))


        try:
            with arcpy.da.UpdateCursor("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT",("Join_Count_1"))as cursor:
                for rows in cursor:
                    if rows[0] != None :
                            if rows[0] == 0:
                                cursor.deleteRow()
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in deleting Join_Count = 0 SPATIAL_JOIN_OUTPUT_SHEATH_DUCT.. :  " + str(e))


        try:
            with arcpy.da.UpdateCursor("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT",("cont_cable"))as cursor:
                for rows in cursor:
                    import re

                    # txt = rows[0]
                    # x = re.findall("F$", txt)
                    if not (re.findall("F$", rows[0])):
                        cursor.deleteRow()
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in deleting cont_cable = F$ SPATIAL_JOIN_OUTPUT_SHEATH_DUCT.. :  " + str(e))


        try:
            with arcpy.da.UpdateCursor("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT",("Error" , "type", "owner", "duct_type"))as cursor:
                for rows in cursor:
                    if rows[1] != None:
                            if rows[1] in (6,7,8,9,10) and rows[2] == 2 and rows[3] == 2:
                                rows[0] = 'Cable ' + '"' +  str(rows[1]) +  '"' ' in unstructured duct'
                                cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Calculating Error 4.2.4 at SPATIAL_JOIN_OUTPUT_SHEATH_DUCT.. :  " + str(e))

        try:
            with arcpy.da.UpdateCursor("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT", ("type", "Error", "owner", "duct_type"))as cursor:
                for rows in cursor:
                    if rows[0] in (2,3,4,5) and rows[2] == 2 and rows[3] == 1:
                        if rows[1] != None:
                            rows[1] = str(rows[1]) + ", FCA over 24F can't go inside Eir Structured Duct "
                        if rows[1] == None:
                            rows[1] = "FCA over 24F can't go inside Eir Structured Duct"
                    cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Calculating Error 4.2.3 at fibre_sheath_copy.. :  " + str(e))

        fieldList = arcpy.ListFields("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT")
        fieldsToBeDeleted = []
        for field in fieldList:
            if str(field.name) not in (
            ["TARGET_FID", "TARGET_FID_1", "TARGET_FID_12", "OBJECTID", "Shape", "Shape_Area", "SHAPE_Length", "Shape_Length", "SHAPE",  "type", "sub_size", "Error", "status", "owner", "duct_type", "Join_Count_1", "Join_Count", "Join_Count_12"]):
                fieldsToBeDeleted.append(field.name)
        arcpy.DeleteField_management("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT", fieldsToBeDeleted)

        try:
            spatial_join_sheath_duct = arcpy.da.FeatureClassToNumPyArray("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT", ["TARGET_FID_1", "Error"], skip_nulls=True)
            # arcpy.AddMessage(spatial_join_sheath_duct)
            filtered_arr_QC3_Boundaries = self.remove_duplicates_None_values(spatial_join_sheath_duct)
            arcpy.AddMessage(filtered_arr_QC3_Boundaries)
        except Exception as e:
            arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))
        #
        try:
            arcpy.AddMessage("Updating errors in final Log File- 4.2.3, 4.2.4 ")
            for x in filtered_arr_QC3_Boundaries:
                with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Error", "Error1")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            # arcpy.AddMessage(str(rows[0]) + " / " + str(x[0]))
                            if row[1] != None:
                                # arcpy.AddMessage(str(row[1]))
                                msg = str(row[1]) + "," + x[1]
                                row[1] = msg
                                row[2] = msg
                            elif row[1] == None:
                                row[1] = x[1]
                                row[2] = x[1]
                                print(x[1])
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final  Log File- 4.2.3, 4.2.4:  " + str(e))



    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("item4point2point3..deleting intermediate layers")
        try:
            if arcpy.Exists("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT"):
                arcpy.Delete_management("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT")
            if arcpy.Exists("SJ_DUCT_POLE"):
                arcpy.Delete_management("SJ_DUCT_POLE")
            if arcpy.Exists("SJ_DUCT_POLE_CHAMBERS"):
                arcpy.Delete_management("SJ_DUCT_POLE_CHAMBERS")
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors :  " + str(e))

class ParentItem4(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 4.2.5')


        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase
        p2 = p1 + "\\" + self.Output_name
        if not arcpy.Exists(self.Output_name):
            # arcpy.conversion.FeatureClassToFeatureClass(boundaries,
            #                                             p1,
            #                                             self.Output_name, '',
            #                                             r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,GlobalID,-1,-1;level_ "Level" true true false 0 Long 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,level_,-1,-1;name "Name" true true false 20 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,name,0,20;homes "Number of Homes in Boundary" true true false 0 Long 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,homes,-1,-1;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Editor,0,128;Shape__Area "Shape__Area" false true true 0 Double 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Shape__Area,-1,-1;Shape__Length "Shape__Length" false true true 0 Double 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Shape__Length,-1,-1;CreationDate_1 "CreationDate" false true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,CreationDate_1,-1,-1;Creator_1 "Creator" false true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Creator_1,0,128;EditDate_1 "EditDate" false true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,EditDate_1,-1,-1;Editor_1 "Editor" false true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Editor_1,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,OBJECTID,-1,-1',
            #                                             '')
            feats = arcpy.FeatureSet(table=fibre_sheath)
            feats.save(p2)
        try:
            if arcpy.Exists(self.Output_name):
                lstFields_sc = arcpy.ListFields(self.Output_name)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Warning" not in field_names_sc:
                    arcpy.AddMessage("adding field (Warning) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Warning", "TEXT", 100)
                if "Error1" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error1) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error1", "TEXT", 100)
                if "Warning1" not in field_names_sc:
                    arcpy.AddMessage("adding field (Warning1) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Warning1", "TEXT", 100)

        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        # if arcpy.Exists(self.fibre_copy):
        #     lstFields = arcpy.ListFields(self.fibre_copy)
        #     field_names = [f.name for f in lstFields]
        #     if "OriginalFID" not in field_names:
        #         arcpy.AddField_management(self.fibre_copy, "OriginalFID", "LONG", 10)
        #         arcpy.CalculateField_management(self.fibre_copy, "OriginalFID", "!OBJECTID!", "PYTHON3")
        #     else:
        #         pass

        # try:
        #     arcpy.GeneratePointsAlongLines_management(Fibre_Duct, 'Duct_centre_points', 'PERCENTAGE', Percentage=50)
        # except Exception as e:
        #     arcpy.AddMessage("Issue in GeneratePointsAlongLines_management span_centre_points .. :  " + str(e))

        arcpy.SpatialJoin_analysis(Fibre_Duct, pole, "SJ_DUCT_POLE", "JOIN_ONE_TO_ONE",
                                   "KEEP_ALL", "", "INTERSECT")
        arcpy.SpatialJoin_analysis("SJ_DUCT_POLE", chambers, "SJ_DUCT_POLE_CHAMBERS", "JOIN_ONE_TO_ONE",
                                   "KEEP_ALL", "", "INTERSECT")

        try:
            with arcpy.da.UpdateCursor("SJ_DUCT_POLE_CHAMBERS",
                                       ("Join_Count", "Join_Count_1"))as cursor:
                for rows in cursor:
                   if rows[0] > 0 and rows[1] > 0:
                    cursor.deleteRow()
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in SJ_DUCT_POLE_CHAMBERS.. :  " + str(e))

        try:
            arcpy.SpatialJoin_analysis(fibre_sheath, "SJ_DUCT_POLE_CHAMBERS", "SPATIAL_JOIN_OUTPUT_SHEATH_DUCT", "JOIN_ONE_TO_MANY",
                                       "KEEP_ALL", "", "SHARE_A_LINE_SEGMENT_WITH")
        except Exception as e:
            arcpy.AddMessage("Issue in SpatialJoin_analysis of Fibre_Duct and self.Output_name.. :  " + str(e))

        try:
            if arcpy.Exists("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT"):
                lstFields_sc = arcpy.ListFields("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Warning" not in field_names_sc:
                    arcpy.AddMessage("adding field (Warning) in SPATIAL_JOIN_OUTPUT_SHEATH_DUCT..")
                    arcpy.AddField_management("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT", "Warning", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        try:
            with arcpy.da.UpdateCursor("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT",("duct_type", "Warning", "backhaul"))as cursor:
                for rows in cursor:
                    if rows[0] != None and rows[2] != None:
                        # arcpy.AddMessage(str(rows[0]))
                        if rows[0] == 2 and rows[2] == 1:
                            arcpy.AddMessage("message")
                            rows[1] = 'Backhaul not allowed in unstructured duct'
                            cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Calculating Error at SPATIAL_JOIN_OUTPUT_SHEATH_DUCT.. :  " + str(e))

        fieldList = arcpy.ListFields("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT")
        fieldsToBeDeleted = []
        for field in fieldList:
            if str(field.name) not in (
            ["TARGET_FID", "TARGET_FID_1", "TARGET_FID_12", "OBJECTID", "Shape", "Shape_Area", "SHAPE_Length", "Shape_Length", "SHAPE",  "duct_type", "Warning", "backhaul", "Join_Count_1", "Join_Count", "Join_Count_12"]):
                fieldsToBeDeleted.append(field.name)
        arcpy.DeleteField_management("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT", fieldsToBeDeleted)


        try:
            spatial_join_sheath_duct = arcpy.da.FeatureClassToNumPyArray("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT", ["TARGET_FID_1", "Warning"], skip_nulls=True)
            # arcpy.AddMessage(spatial_join_sheath_duct)
            filtered_arr_QC3_Boundaries = self.remove_duplicates_None_values(spatial_join_sheath_duct)
            arcpy.AddMessage("filtered_arr_QC3_Boundaries")
            arcpy.AddMessage(filtered_arr_QC3_Boundaries)
        except Exception as e:
            arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final Log File- 4.2.5")
            for x in filtered_arr_QC3_Boundaries:
                with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Warning", "Warning1")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            # arcpy.AddMessage(str(rows[0]) + " / " + str(x[0]))
                            if row[1] != None:
                                # arcpy.AddMessage(str(row[1]))
                                msg = str(row[1]) + "," + x[1]
                                row[1] = msg
                                row[2] = msg
                            elif row[1] == None:
                                row[1] = x[1]
                                row[2] = x[1]
                                print(x[1])
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final  Log File- 4.2.5:  " + str(e))

    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("item4point2point3..deleting intermediate layers")
        try:
            if arcpy.Exists("SJ_DUCT_POLE"):
                arcpy.Delete_management("SJ_DUCT_POLE")
            if arcpy.Exists("SJ_DUCT_POLE_CHAMBERS"):
                arcpy.Delete_management("SJ_DUCT_POLE_CHAMBERS")
            if arcpy.Exists("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT"):
                arcpy.Delete_management("SPATIAL_JOIN_OUTPUT_SHEATH_DUCT")
        except Exception as e:
            arcpy.AddMessage("Issue in Deleting Layers.. 4.2.5 :  " + str(e))

class ParentItem2point6(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        arcpy.AddMessage("enter into Fibre Check in 4.2.6")
        self.Output_name = name
        self.temp_file = 'temp_file'
        self.olt_copy = "olt_copy"
        self.splice_closure_copy = "splice_closure_copy"
        self.fibre_sheath_copy = "fibre_sheath_copy"

    def ItemNo(self):
        arcpy.CopyFeatures_management(fibre_sheath, self.fibre_sheath_copy)
        arcpy.Merge_management([splice_closure, OLT], "splice_olt_merged_record")

        if arcpy.Exists(self.splice_closure_copy):
            arcpy.AddField_management(self.splice_closure_copy, "Error", "Text", 50)

        if arcpy.Exists(self.fibre_sheath_copy):
            arcpy.AddField_management(self.fibre_sheath_copy, "start_point_x", "Text", 50)
            arcpy.AddField_management(self.fibre_sheath_copy, "start_point_y", "Text", 50)
            arcpy.AddField_management(self.fibre_sheath_copy, "end_point_x", "Text", 50)
            arcpy.AddField_management(self.fibre_sheath_copy, "end_point_y", "Text", 50)

        if not arcpy.Exists("output"):
            arcpy.CopyFeatures_management(fibre_sheath, "output")
            arcpy.AddField_management("output", "Error", "Text", 50)

        with arcpy.da.UpdateCursor(self.fibre_sheath_copy, ["start_point_x", "start_point_y", "end_point_x", "end_point_y", "SHAPE@"]) as cursor:
            for row in cursor:
                if row[4] != None:
                    start_point = row[4].firstPoint
                    end_point = row[4].lastPoint
                    row[0] = str(start_point.X)
                    row[1] = str(start_point.Y)
                    row[2] = str(end_point.X)
                    row[3] = str(end_point.Y)
                    cursor.updateRow(row)
            del cursor

        array_fibre = arcpy.da.FeatureClassToNumPyArray(self.fibre_sheath_copy, ["start_point_x", "start_point_y", "end_point_x", "end_point_y"])

        spatial_ref = arcpy.Describe(self.fibre_sheath_copy).spatialReference
        if spatial_ref.name == "Unknown":
            arcpy.AddMessage("{0} has an unknown spatial reference".format(self.fibre_sheath_copy))

        else:
            arcpy.AddMessage("{0} : {1}".format(self.fibre_sheath_copy, spatial_ref.PCSCode))

        pointList = arcpy.Array()
        for record in array_fibre:
            if str(record[0]) != 'None' and str(record[1]) != 'None' and str(record[2]) != 'None' and str(record[3]) != 'None':
                start_point = arcpy.Point(record[0], record[1])
                end_point = arcpy.Point(record[2], record[3])
                pointList.append([start_point, end_point])


        # Create an empty Point object
        point = arcpy.Point()

        # A list to hold the PointGeometry objects
        pointGeometryList = []

        sr = arcpy.SpatialReference(spatial_ref.PCSCode)

        # For each coordinate pair, populate the Point object and create a new
        # PointGeometry object
        for pt in pointList:
            for data in pt:
                pointGeometry = arcpy.PointGeometry(data, sr)
                pointGeometryList.append(pointGeometry)

        # Create a copy of the PointGeometry objects, by using pointGeometryList as
        # input to the CopyFeatures tool.

        arcpy.CopyFeatures_management(pointGeometryList, "Test_Geometry")


        ########## Spatial Join OF ONE TO ONE  ###############

        arcpy.SpatialJoin_analysis("Test_Geometry", "splice_olt_merged_record", "final_output", join_operation='JOIN_ONE_TO_ONE', join_type ='KEEP_ALL', field_mapping = '', match_option = 'INTERSECT')

        arcpy.AddField_management("final_output", "Error", "Text", 50)


        with arcpy.da.UpdateCursor("final_output", ["Join_Count", "Error"]) as cursor:
             for row in cursor:
                 if row[0] != None:
                     if int(row[0]) == 0:
                         row[1] = "Fibre sheath not snapped to the OLT/ enclosure"
                     cursor.updateRow(row)
             del cursor

        arcpy.SpatialJoin_analysis(self.fibre_sheath_copy, "final_output", "final_output_fibre", join_operation='JOIN_ONE_TO_ONE', join_type='KEEP_ALL', field_mapping='', match_option='CONTAINS')

        with arcpy.da.UpdateCursor("final_output_fibre", ["Error"]) as cursor:
             for row in cursor:
                 if row[0] == None:
                     cursor.deleteRow()
             del cursor

        array_fibre = arcpy.da.FeatureClassToNumPyArray("final_output_fibre", ["label", "Error"], skip_nulls=False)

        if not arcpy.Exists(self.Output_name):
            arcpy.CopyFeatures_management(fibre_sheath, self.Output_name)

        if arcpy.Exists(self.Output_name):
            lstFields = arcpy.ListFields(self.Output_name)
            field_names = [f.name for f in lstFields]
            if "Error" not in field_names:
                arcpy.AddField_management(self.Output_name, "Error", "Text", 100)
            if "Error1" not in field_names:
                arcpy.AddField_management(self.Output_name, "Error1", "TEXT", 100)
            if "Warning1" not in field_names:
                arcpy.AddField_management(self.Output_name, "Warning1", "TEXT", 100)

        for data in array_fibre:
            with arcpy.da.UpdateCursor(self.Output_name, ["label", "Error", "Error1"]) as cursor:
                for row in cursor:
                    if row[0] != None:
                        if str(row[0]) == str(data[0]):
                            if row[1] != None:
                                if str(row[1]) != data[1]:
                                    msg = str(row[1]) +','+ data[1]
                                    row[1] = msg
                                    row[2] = msg
                            else:
                                row[1] = data[1]
                                row[2] = data[1]
                        cursor.updateRow(row)
                del cursor



    def __del__(self):
        #### Object Remove Here ######
        if arcpy.Exists(self.fibre_sheath_copy):
            arcpy.Delete_management(self.fibre_sheath_copy)
        if arcpy.Exists(self.splice_closure_copy):
            arcpy.Delete_management(self.splice_closure_copy)
        if arcpy.Exists("Test_Geometry"):
            arcpy.Delete_management("Test_Geometry")
        if arcpy.Exists("splice_olt_merged_record"):
            arcpy.Delete_management("splice_olt_merged_record")
        if arcpy.Exists("final_output"):
            arcpy.Delete_management("final_output")
        if arcpy.Exists("final_output_fibre"):
            arcpy.Delete_management("final_output_fibre")
        if arcpy.Exists(self.olt_copy):
            arcpy.Delete_management(self.olt_copy)
        if arcpy.Exists("output"):
            arcpy.Delete_management("output")

# class ParentItem2point7(object):
#     def __init__(self, name):
#         ### Object Intialize Here ####
#         self.Output_name = name
#
#
#     def ItemNo(self):
#         arcpy.AddMessage('Tool Check Run For item 4.2.7')
#
#         if not arcpy.Exists(self.Output_name):
#             arcpy.CopyFeatures_management(fibre_sheath, self.Output_name)
#
#         if not arcpy.Exists("span_copy"):
#             arcpy.CopyFeatures_management(span, self.Output_name)
#         try:
#             if arcpy.Exists(self.Output_name):
#                 lstFields_sc = arcpy.ListFields(self.Output_name)
#                 field_names_sc = [f.name for f in lstFields_sc]
#                 if "Error" not in field_names_sc:
#                     arcpy.AddMessage("adding field (Error) in self.Output_name..")
#                     arcpy.AddField_management(self.Output_name, "Error", "TEXT", 100)
#                 else:
#                     pass
#         except Exception as e:
#             arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))
#
#         try:
#             arcpy.GeneratePointsAlongLines_management(span, 'span_centre_points', 'PERCENTAGE', Percentage=50)
#         except Exception as e:
#             arcpy.AddMessage("Issue in GeneratePointsAlongLines_management span_centre_points .. :  " + str(e))
#
#         try:
#             arcpy.SpatialJoin_analysis('span_centre_points', fibre_sheath, "SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH", "JOIN_ONE_TO_MANY",
#                                        "KEEP_ALL", "", "INTERSECT")
#         except Exception as e:
#             arcpy.AddMessage("Issue in SpatialJoin_analysis of Fibre_Duct and self.Output_name.. :  " + str(e))
#
#
#         try:
#             with arcpy.da.UpdateCursor('SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH', ("Join_Count", "span_type")) as cursor:
#                 for rows in cursor:
#                     if rows[0] == 0 or rows[1] != 1:
#                         cursor.deleteRow()
#                 del cursor
#         except Exception as e:
#             arcpy.AddMessage("Issue in deleting 0 Join_Count SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH.. :  " + str(e))
#
#         # try:
#         #     array1 = []
#         #     with arcpy.da.UpdateCursor("SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH", ("Join_Count", "ORIG_FID")) as cursor:
#         #         for rows in cursor:
#         #             if rows[1] != None:
#         #                 if str(rows[1]) in array1:
#         #                     rows[0] = int(rows[0]) + 1
#         #                     array1.append(str(rows[1]))
#         #                 else:
#         #                     rows[0] = 1
#         #                     array1.append(str(rows[1]))
#         #                 cursor.updateRow(rows)
#         #         del cursor
#         # except Exception as e:
#         #     arcpy.AddMessage("Issue in Updating Cursor (Join_Count) SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH.. :  " + str(e))
#
#         try:
#             if arcpy.Exists("SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH"):
#                 lstFields_sc = arcpy.ListFields("SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH")
#                 field_names_sc = [f.name for f in lstFields_sc]
#                 if "Error" not in field_names_sc:
#                     arcpy.AddMessage("adding field (Error) in SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH..")
#                     arcpy.AddField_management("SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH", "Error", "TEXT", 100)
#                 else:
#                     pass
#         except Exception as e:
#             arcpy.AddMessage("Issue in adding field (Error) to SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH.. :  " + str(e))
#
#         # try:
#         #     with arcpy.da.UpdateCursor("SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH", ("Join_Count", "Error")) as cursor:
#         #         for rows in cursor:
#         #             if rows[0] != None:
#         #                 if int(rows[0]) > 1:
#         #                     rows[1] = "Over 1 Fibre Sheath  on aerial span is prohibited"
#         #                 cursor.updateRow(rows)
#         #         del cursor
#         # except Exception as e:
#         #     arcpy.AddMessage("Issue in writing error message in SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH. :  " + str(e))
#
#         try:
#             lst_values = [r[0] for r in arcpy.da.SearchCursor("SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH", ("TARGET_FID"))]
#             lst_values1 = [r[0] for r in arcpy.da.SearchCursor("SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH", ("Join_Count"))]
#         except Exception as e:
#             arcpy.AddMessage("Issue in Search Cursor TARGET_FID Join_Count: ..." + str(e))
#
#         # fields_sc = ["Error"]
#         #
#         # array_for_error_field_message_append = arcpy.da.FeatureClassToNumPyArray(output_spatial_join_pole_splice_closures, fields_sc, skip_nulls=False)
#         #
#         # arcpy.AddMessage(array_for_error_field_message_append[0])
#
#         try:
#             index = 0
#             # arcpy.AddMessage("Item No. 27: Started Checking More than 1 enclosure in the same  pole ")
#             with arcpy.da.UpdateCursor("SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH", ("TARGET_FID", "Join_Count", "Error")) as cursor:
#                 for row in cursor:
#                     # for data in array_for_error_field_message_append:
#                     span = row[0]
#                     count_fibre_sheath = row[1]
#                     error = row[2]
#                     if index == len(lst_values) - 1:
#                         next_span = None
#                     else:
#                         next_span = lst_values[index + 1]
#                     if span == next_span:
#                         # arcpy.AddMessage(pole)
#                         row[1] = lst_values1[index] + 1
#                         # arcpy.AddMessage(row[1])
#                         row[2] = "Over 1 Fibre Sheath  on aerial span is prohibited "
#                         # arcpy.AddMessage(row[2])
#                     else:
#                         pass
#                     cursor.updateRow(row)
#                     index += 1
#                 del cursor
#         except Exception as e:
#             arcpy.AddMessage("Issue in Checking More than 1 sheath in the same span:...." + str(e))
#
#         try:
#             spatial_join_sheath_duct = arcpy.da.FeatureClassToNumPyArray("SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH", ["JOIN_FID", "Error"], skip_nulls=True)
#             # arcpy.AddMessage(spatial_join_sheath_duct)
#             filtered_arr_QC3_Boundaries = self.remove_duplicates_None_values(spatial_join_sheath_duct)
#             # arcpy.AddMessage(filtered_arr_QC3_Boundaries)
#         except Exception as e:
#             arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))
#
#         try:
#             arcpy.AddMessage("Updating errors in final Log File- 4.2.7")
#             for x in filtered_arr_QC3_Boundaries:
#                 with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Error")) as cursor:
#                     for row in cursor:
#                         if str(row[0]) == str(x[0]):
#                             # arcpy.AddMessage(str(rows[0]) + " / " + str(x[0]))
#                             if row[1] != None:
#                                 # arcpy.AddMessage(str(row[1]))
#                                 row[1] = str(row[1]) + "," + x[1]
#                             elif row[1] == None:
#                                 row[1] = x[1]
#                                 print(x[1])
#                         else:
#                             pass
#                         cursor.updateRow(row)
#                     del cursor
#         except Exception as e:
#             arcpy.AddMessage("Issue in Updating errors in final  Log File- 4.2.7:  " + str(e))
#
#     def __del__(self):
#         #### Object Remove Here ######
#         arcpy.AddMessage("item4point2point7..deleting intermediate layers")
#         try:
#             # if arcpy.Exists("SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH"):
#             #     arcpy.Delete_management("SPATIAL_JOIN_OUTPUT_span_centre_points_SHEATH")
#
#             if arcpy.Exists("span_centre_points"):
#                 arcpy.Delete_management("span_centre_points")
#         except Exception as e:
#             arcpy.AddMessage("Issue in item4point2point7..deleting intermediate layers :  " + str(e))

class ParentItem2point7(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = name


    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 4.2.7')

        SR = arcpy.Describe(span).spatialReference

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase
        p2 = p1 + "\\" + self.Output_name
        if not arcpy.Exists(self.Output_name):
            # arcpy.conversion.FeatureClassToFeatureClass(boundaries,
            #                                             p1,
            #                                             self.Output_name, '',
            #                                             r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,GlobalID,-1,-1;level_ "Level" true true false 0 Long 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,level_,-1,-1;name "Name" true true false 20 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,name,0,20;homes "Number of Homes in Boundary" true true false 0 Long 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,homes,-1,-1;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Editor,0,128;Shape__Area "Shape__Area" false true true 0 Double 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Shape__Area,-1,-1;Shape__Length "Shape__Length" false true true 0 Double 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Shape__Length,-1,-1;CreationDate_1 "CreationDate" false true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,CreationDate_1,-1,-1;Creator_1 "Creator" false true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Creator_1,0,128;EditDate_1 "EditDate" false true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,EditDate_1,-1,-1;Editor_1 "Editor" false true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Editor_1,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,OBJECTID,-1,-1',
            #                                             '')
            feats = arcpy.FeatureSet(table=fibre_sheath)
            feats.save(p2)

        try:
            if arcpy.Exists(self.Output_name):
                lstFields_sc = arcpy.ListFields(self.Output_name)
                field_names_fs = [f.name for f in lstFields_sc]
                if "Error" not in field_names_fs:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.Output_name, "Error", "TEXT", 100)

                if "Error1" not in field_names_fs:
                    arcpy.AddField_management(self.Output_name, "Error1", "TEXT", 100)
                if "Warning1" not in field_names_fs:
                    arcpy.AddField_management(self.Output_name, "Warning1", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        p = arcpy.mp.ArcGISProject("CURRENT")
        p1 = p.defaultGeodatabase
        p2 = p1 + "\\" + "span_copy1"
        if not arcpy.Exists("span_copy1"):
            # arcpy.conversion.FeatureClassToFeatureClass(boundaries,
            #                                             p1,
            #                                             self.Output_name, '',
            #                                             r'GlobalID "GlobalID" false false true 38 GlobalID 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,GlobalID,-1,-1;level_ "Level" true true false 0 Long 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,level_,-1,-1;name "Name" true true false 20 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,name,0,20;homes "Number of Homes in Boundary" true true false 0 Long 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,homes,-1,-1;CreationDate "CreationDate" true true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,CreationDate,-1,-1;Creator "Creator" true true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Creator,0,128;EditDate "EditDate" true true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,EditDate,-1,-1;Editor "Editor" true true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Editor,0,128;Shape__Area "Shape__Area" false true true 0 Double 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Shape__Area,-1,-1;Shape__Length "Shape__Length" false true true 0 Double 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Shape__Length,-1,-1;CreationDate_1 "CreationDate" false true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,CreationDate_1,-1,-1;Creator_1 "Creator" false true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Creator_1,0,128;EditDate_1 "EditDate" false true false 8 Date 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,EditDate_1,-1,-1;Editor_1 "Editor" false true false 128 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,Editor_1,0,128;OBJ1 "OBJ1" true true false 255 Text 0 0,First,#,TestLayer_Cashel_gdb\Boundaries,OBJECTID,-1,-1',
            #                                             '')
            feats = arcpy.FeatureSet(table=span)
            feats.save(p2)

        with arcpy.da.UpdateCursor('span_copy1',
                                   ("span_type")) as cursor:
            for rows in cursor:
                    if rows[0] != 1:
                        cursor.deleteRow()
            del cursor

        arcpy.SpatialJoin_analysis("span_copy1", fibre_sheath, "SJ_span_FS",
                                   "JOIN_ONE_TO_ONE",
                                   "KEEP_ALL", "", "SHARE_A_LINE_SEGMENT_WITH")

        with arcpy.da.UpdateCursor('SJ_span_FS',
                                   ("Join_Count")) as cursor:
            for rows in cursor:
                    if rows[0] <= 1:
                        cursor.deleteRow()
            del cursor

        SJ_span_FS_count = []
        with arcpy.da.UpdateCursor('SJ_span_FS',
                                   ("TARGET_FID", "Join_Count")) as cursor:
            for rows in cursor:
                    if rows[0] not in SJ_span_FS_count:
                        SJ_span_FS_count.append(rows[0])
            del cursor
        arcpy.AddMessage("SJ_span_FS_count")
        arcpy.AddMessage(SJ_span_FS_count)

        arcpy.SpatialJoin_analysis(fibre_sheath, "span_copy1" ,"SJ_FS_span",
                                   "JOIN_ONE_TO_MANY",
                                   "KEEP_ALL", "", "SHARE_A_LINE_SEGMENT_WITH")

        with arcpy.da.UpdateCursor('SJ_FS_span',
                                   ("Join_Count" , "JOIN_FID")) as cursor:
            for rows in cursor:
                    if rows[0] < 1 or rows[1] == -1:
                        cursor.deleteRow()
            del cursor

        fs_ID = []
        with arcpy.da.UpdateCursor('SJ_FS_span',
                                   ("TARGET_FID", "JOIN_FID")) as cursor:
            for rows in cursor:
                if rows[0] not in fs_ID:
                    fs_ID.append([rows[0],[]])
                elif rows[0] in fs_ID:
                    pass
            del cursor
        # arcpy.AddMessage("fs_ID")
        # arcpy.AddMessage(fs_ID)


        for data in fs_ID:
            span_id = data[1]
            # arcpy.AddMessage(span_id)
            with arcpy.da.UpdateCursor('SJ_FS_span',
                                       ("TARGET_FID", "JOIN_FID")) as cursor:
                for rows in cursor:
                    if rows[0] == data[0]:
                        span_id.append(rows[1])

                    elif rows[0] != data[0]:
                        pass
                del cursor


        fs_ID2 = []
        for data in fs_ID:
            if data not in fs_ID2:
                fs_ID2.append(data)
            elif data in fs_ID2:
                pass
        arcpy.AddMessage("fs_ID2")
        arcpy.AddMessage(fs_ID2)

        SJ_FS_span = []

        for data in fs_ID2:
            counter = 0
            span_id = data[1]
            for x in span_id:
                for items in SJ_span_FS_count:
                    if x == items:
                        counter = counter + 1
                        print(counter)
                        if counter > 1:
                            if data[0] not in SJ_FS_span:
                                SJ_FS_span.append(data[0])

        print(SJ_FS_span)
        arcpy.AddMessage("SJ_FS_span")
        arcpy.AddMessage(SJ_FS_span)

        if arcpy.Exists("SJ_FS_span"):
            lstFields_sc = arcpy.ListFields("SJ_FS_span")
            field_names_fs = [f.name for f in lstFields_sc]
            if "Error" not in field_names_fs:
                arcpy.AddMessage("adding field (Error) in SJ_FS_span..")
                arcpy.AddField_management("SJ_FS_span", "Error", "TEXT", 100)


        with arcpy.da.UpdateCursor('SJ_FS_span',
                                   ("TARGET_FID", "Error")) as cursor:
            for rows in cursor:
                if rows[0] in SJ_FS_span:
                    arcpy.AddMessage(str(rows[0]))
                    rows[1] = "Over 1 Fibre Sheath on 2 or more consecutive aerial span is prohibited"
                    cursor.updateRow(rows)
            del cursor

        try:
            SPATIAL_JOIN_OUTPUT_span_SHEATH2 = arcpy.da.FeatureClassToNumPyArray("SJ_FS_span", ["TARGET_FID", "Error"], skip_nulls=True)
            # arcpy.AddMessage(spatial_join_sheath_duct)
            filtered_arr_QC3_Boundaries = self.remove_duplicates_None_values(SPATIAL_JOIN_OUTPUT_span_SHEATH2)
            arcpy.AddMessage("filtered_arr_QC3_Boundaries")
            arcpy.AddMessage(filtered_arr_QC3_Boundaries)
        except Exception as e:
            arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final Log File- 4.2.7")
            for x in filtered_arr_QC3_Boundaries:
                with arcpy.da.UpdateCursor(self.Output_name, ("OBJECTID", "Error", "Error1")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            arcpy.AddMessage(str(rows[0]) + " / " + str(x[0]))
                            if row[1] != None:
                                # arcpy.AddMessage(str(row[1]))
                                msg = str(row[1]) + ',' + x[1]
                                row[1] = msg
                                row[2] = msg
                            elif row[1] == None:
                                row[1] = x[1]
                                row[2] = x[1]
                                print(x[1])
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final  Log File- 4.2.7:  " + str(e))

    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("item4point2point7..deleting intermediate layers")
        try:
            if arcpy.Exists("SJ_FS_span"):
                arcpy.Delete_management("SJ_FS_span")
            if arcpy.Exists("span_copy1"):
                arcpy.Delete_management("span_copy1")
            if arcpy.Exists("SJ_span_FS"):
                arcpy.Delete_management("SJ_span_FS")
        except Exception as e:
            arcpy.AddMessage("Issue in item4point2point7..deleting intermediate layers :  " + str(e))


class BaseClass(ParentItem2point1):
    def __init__(self):
        from datetime import datetime

        now = datetime.now()
        start_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run Start's @" + start_time)

        if arcpy.Exists("CablePlacement"):
            arcpy.Delete_management("CablePlacement")

        ### Object Intialize Here ####
        if item2point1 == 'true':
            ParentItem2point1.__init__(self, 'CablePlacement')
            ParentItem2point1.ItemNo(self)
            ParentItem2point1.__del__(self)

        if item2point2 == 'true':
            ParentItem2.__init__(self, 'CablePlacement')
            ParentItem2.ItemNo(self)
            ParentItem2.__del__(self)

        if item2point3 == 'true':
            ParentItem3.__init__(self, 'CablePlacement')
            ParentItem3.ItemNo(self)
            ParentItem3.__del__(self)

        if item2point4 == 'true':
            ParentItem4.__init__(self, 'CablePlacement')
            ParentItem4.ItemNo(self)
            ParentItem4.__del__(self)

        if item2point5 == "true":
            ParentItem2point6.__init__(self, 'CablePlacement')
            ParentItem2point6.ItemNo(self)
            ParentItem2point6.__del__(self)

        if item2point6 == "true":
            ParentItem2point7.__init__(self, 'CablePlacement')
            ParentItem2point7.ItemNo(self)
            ParentItem2point7.__del__(self)

    def checkFID(self, array, id):
        count = 0
        if len(array) > 0 and id != "":
            for x in array:
                if int(id) == int(x[0]):
                    count = count + 1
        return count

    def remove_duplicates_None_values(self, array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[1] != 'None':
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1

    def remove_duplicates_Span_values(self, array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[1] != 'None':
                    if len(array_find) == 0:
                        if str(items[1]) == "1":
                            array_find.append(items[0])
                            array_find1.append(items)
                    elif items[0] not in array_find and str(items[1]) == "1":
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1

    def remove_duplicates_zero_values(array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[2] != 0:
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1

    def find_No_array(self, array, value):
        count = 0
        if len(array) > 0:
            for data in array:
                if value in data:
                    count = count + 1
        return count

    def find_matching_cordinates(self, arr, arr_ids, arry_start):
        match = ''
        match_start = ''
        index = 0
        array_index_endx = []
        array_index_startx = []
        array_id_match = []
        for i in arr:
            if str(i) not in array_index_endx:
                array_index_endx.append(i)
                array_index_startx.append(arry_start[index])
            else:
                # arcpy.AddMessage(i)
                # array_id_match.append(arr_ids[index])
                match = i

            index = index + 1

        if str(match) in array_index_startx:
            indx = array_index_startx.index(str(match))
            array_index_startx.pop(indx)

        for i in range(0, len(arr)):
            if str(arr[i]) == str(match):
                array_id_match.append(arr_ids[i])
            # if str(arry_start[i]) in array_index_startx and str(arry_start[i]) != str(match):
            #     if arr_ids[i] not in array_id_match:
            #         array_id_match.append(arr_ids[i])
        return array_id_match

    def find_endpoints(self, array, id):
        count = 0
        array_id = []
        end_point_X_y = []
        start_point_X = []
        array_label = []
        if len(array) > 0:
            array_count = 0
            for data in array:
                if str(id) == str(data[0]):
                    end_point_X_y.append(data[1])
                    start_point_X.append(data[4])
                    array_id.append(data[3])
        # arcpy.AddMessage(array_id)
        lengt = len(end_point_X_y)
        matcing_ids = self.find_matching_cordinates(end_point_X_y, array_id, start_point_X)
        return matcing_ids

    #def find_geometry(self):

    def __del__(self):
        #### Object Remove Here ######
        fieldList = arcpy.ListFields(self.Output_name)
        fieldsToBeDeleted = []

        if 'true' in (item2point1, item2point4) and 'true' not in (item2point2, item2point3, item2point5,item2point6):
            arcpy.AddMessage("if")
            for field in fieldList:
                if str(field.name) not in (
                        ["OBJECTID", "Shape", "Shape_Area", "SHAPE_Length", "Shape_Length", "label", "SHAPE", "Warning1", "Shape__Length"]):
                    fieldsToBeDeleted.append(field.name)

            with arcpy.da.UpdateCursor(self.Output_name, ("Warning")) as cursor:
                for row in cursor:
                    if row[0] == None:
                        cursor.deleteRow()
                del cursor
        #
            if arcpy.Exists(self.Output_name):
                arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))
                arcpy.DeleteField_management(self.Output_name, fieldsToBeDeleted)

        elif 'true' not in (item2point1, item2point4) and 'true' in (item2point2, item2point3, item2point5, item2point6):
            arcpy.AddMessage("in elif")
            for field in fieldList:
                if str(field.name) not in (
                        #["OBJECTID", "Shape", "Shape_Area", "SHAPE_Length", "Shape_Length", "label", "SHAPE", "Error1"]):
                        ["OBJECTID", "Shape", "Shape_Area", "SHAPE_Length", "Shape_Length", "Shape__Length", "label", "SHAPE", "Error1"
                         ]):
                    fieldsToBeDeleted.append(field.name)

            with arcpy.da.UpdateCursor(self.Output_name, ("Error")) as cursor:
                for row in cursor:
                    if row[0] == "" or row[0] == None:
                        # arcpy.AddMessage("del")
                        cursor.deleteRow()
                del cursor

            if arcpy.Exists(self.Output_name):
                arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))
                arcpy.DeleteField_management(self.Output_name, fieldsToBeDeleted)

            lstFields_sc = arcpy.ListFields(self.Output_name)
            field_names_fs = [f.name for f in lstFields_sc]
            if "Error1" in field_names_fs:
                arcpy.AlterField_management(self.Output_name, 'Error1', 'Error', 'Error')
            # if "Warning1" in field_names_fs:
            #     arcpy.AlterField_management(self.Output_name, 'Warning1', 'Warning', 'Warning')
            arcpy.AddMessage("Finished Deleting unwanted fields from " + str(self.Output_name))

        else:
            arcpy.AddMessage("else")
            for field in fieldList:
                if str(field.name) not in (
                        ["OBJECTID", "Shape", "Shape_Area", "SHAPE_Length", "Shape_Length", "Shape__Length", "label", "SHAPE", "Error1", "Warning1"]):
                    fieldsToBeDeleted.append(field.name)

            with arcpy.da.UpdateCursor(self.Output_name, ("Error", "Warning")) as cursor:
                for row in cursor:
                    if row[0] == None and row[1] == None:
                        cursor.deleteRow()
                del cursor

            if arcpy.Exists(self.Output_name):
                arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))
                arcpy.DeleteField_management(self.Output_name, fieldsToBeDeleted)


        lstFields_sc = arcpy.ListFields(self.Output_name)
        field_names_fs = [f.name for f in lstFields_sc]
        if "Error1" in field_names_fs:
            arcpy.AlterField_management(self.Output_name, 'Error1', 'Error', 'Error')
        if "Warning1" in field_names_fs:
            arcpy.AlterField_management(self.Output_name, 'Warning1', 'Warning', 'Warning')
        arcpy.AddMessage("Finished Deleting unwanted fields from " + str(self.Output_name))

        # if item2point1 == 'true' or item2point4 == 'true' or 'true' in (item2point3, item2point5, item2point6, item2point2):
        #     arcpy.AddMessage("Inside all true..")
        #
        # if arcpy.Exists(self.Output_name):
        #     arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))
        #     arcpy.DeleteField_management(self.Output_name, fieldsToBeDeleted)





        from datetime import datetime

        now = datetime.now()
        end_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run End's @" + end_time)


######### Get All Input Parameter ###########

arcpy.env.overwriteOutput = True

input_geodatabase = arcpy.GetParameterAsText(0)
OLT = arcpy.GetParameterAsText(1)
fibre_sheath = arcpy.GetParameterAsText(2)
splice_closure = arcpy.GetParameterAsText(3)
Fibre_Duct = arcpy.GetParameterAsText(4)
span = arcpy.GetParameterAsText(5)
pole = arcpy.GetParameterAsText(6)
chambers = arcpy.GetParameterAsText(7)

######## Checked Input #########

item2point1 = arcpy.GetParameterAsText(8)
item2point2 = arcpy.GetParameterAsText(9)
item2point3 = arcpy.GetParameterAsText(10)
item2point4 = arcpy.GetParameterAsText(11)
item2point5 = arcpy.GetParameterAsText(12)
item2point6 = arcpy.GetParameterAsText(13)



######### Oject Creation #######
# main function
if __name__ == '__main__':
    # created the object for BaseClass
    if arcpy.Exists("CablePlacement"):
        arcpy.Delete_management("CablePlacement")

    ob = BaseClass()
