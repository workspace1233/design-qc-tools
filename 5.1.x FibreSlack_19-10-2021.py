import arcpy

arcpy.env.overwriteOutput = True

class ParentItem1(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = "fibre_copy1"
        self.FinalOutput1 = name

    def ItemNo(self):

        arcpy.AddMessage('Tool Check Run For item 5.1.1:')

        #arcpy.CopyFeatures_management(Fibre_Sheath, "fibre_copy")

        p2 = p1 + "\\" + "fibre_copy"
        #arcpy.CopyFeatures_management(Fibre_Sheath, "fibre_copy")
        if not arcpy.Exists("fibre_copy"):
            feats = arcpy.FeatureSet(table=Fibre_Sheath)
            feats.save(p2)
            arcpy.AddField_management("fibre_copy", "OriginalFID", "LONG", 10)
            arcpy.CalculateField_management("fibre_copy", "OriginalFID", "!OBJECTID!", "PYTHON3")

        #arcpy.CopyFeatures_management(Fibre_Slack, "slack_copy")
        p3 = p1 + "\\" + "slack_copy"

        if not arcpy.Exists("slack_copy"):
            feats1 = arcpy.FeatureSet(table=Fibre_Slack)
            feats1.save(p3)
            arcpy.AddField_management("slack_copy", "OriginalFID_slack", "LONG", 10)
            arcpy.AddField_management("slack_copy", "Error", "TEXT", 500)


        # try:
        #     if arcpy.Exists(self.Output_name):
        #         arcpy.AddMessage("Deleting self.Output_name Previous log file")
        #         arcpy.Delete_management(self.Output_name)
        # except Exception as e:
        #     arcpy.AddMessage("Issue self.Output_name to delete " + str(e))
        #
        # if not arcpy.Exists(self.Output_name):
        #     arcpy.AddMessage("Output does not exist so , creating new Output File")
        #     arcpy.CopyFeatures_management(Fibre_Sheath, self.Output_name)



        # try:
        #     if arcpy.Exists(self.Output_name):
        #         lstFields_sc = arcpy.ListFields(self.Output_name)
        #         field_names_sc = [f.name for f in lstFields_sc]
        #         if "Error" not in field_names_sc:
        #             arcpy.AddMessage("adding field (Error) in self.Output_name..")
        #             arcpy.AddField_management(self.Output_name, "Error", "TEXT", 100)
        #         else:
        #             pass
        # except Exception as e:
        #     arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        # try:
        #     if arcpy.Exists(self.FinalOutput):
        #         lstFields_sk = arcpy.ListFields(self.FinalOutput)
        #         field_names_sk = [f.name for f in lstFields_sk]
        #         if "Error" not in field_names_sk:
        #             arcpy.AddMessage("adding field (Error) in self.FinalOutput..")
        #             arcpy.AddField_management(self.FinalOutput, "Error", "TEXT", 100)
        #         else:
        #             pass
        # except Exception as e:
        #     arcpy.AddMessage("Issue in adding field (Error) to self.FinalOutput.. :  " + str(e))

        arcpy.analysis.CountOverlappingFeatures("slack_copy", "slack_copy_overlaps", 2, "slack_copy_overlaps_table")

        arcpy.management.JoinField("slack_copy", "OBJECTID", "slack_copy_overlaps_table", "ORIG_OID",  ["OVERLAP_OID"])

        try:
            if arcpy.Exists("slack_copy"):
                lstFields_sc = arcpy.ListFields("slack_copy")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Count" not in field_names_sc:
                    arcpy.AddField_management("slack_copy", "Count", "Long", 10)
        except Exception as e:
            arcpy.AddMessage("Issue in Adding field (Count) in slack_copy.. :  " + str(e))

        try:
            with arcpy.da.UpdateCursor("slack_copy", ("Count")) as cursor:
                for rows in cursor:
                    rows[0] = 1
                    cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in (Count) in slack_copy.. :  " + str(e))

        try:
            array1 = []
            with arcpy.da.UpdateCursor("slack_copy", ("Count", "OVERLAP_OID")) as cursor:
                for rows in cursor:
                    if rows[1] != None:
                        if str(rows[1]) in array1:
                            rows[0] = int(rows[0]) + 1
                            array1.append(str(rows[1]))
                        else:
                            rows[0] = 1
                            array1.append(str(rows[1]))
                        cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating Cursor (OVERLAP_OID) slack_copy.. :  " + str(e))

        with arcpy.da.UpdateCursor("slack_copy", ["Count"]) as cursor:
            for row in cursor:
                if row[0] != None:
                    if row[0] == 2:
                        cursor.deleteRow()
            del cursor


        try:
            arcpy.SpatialJoin_analysis("fibre_copy", "slack_copy", "fibre_slack_join", "JOIN_ONE_TO_ONE", "KEEP_ALL", "", "INTERSECT")
        except Exception as e:
            arcpy.AddMessage("Issue in SpatialJoin_analysis of Fibre_Sheath, Fibre_Slack.. :  " + str(e))

        # try:
        #     if arcpy.Exists("SPATIAL_JOIN_OUTPUT_Sheath_Slack"):
        #         lstFields_sc = arcpy.ListFields("SPATIAL_JOIN_OUTPUT_Sheath_Slack")
        #         field_names_sc = [f.name for f in lstFields_sc]
        #         if "Error" not in field_names_sc:
        #             arcpy.AddMessage("adding field (Error) in SPATIAL_JOIN_OUTPUT_Sheath_Slack..")
        #             arcpy.AddField_management("SPATIAL_JOIN_OUTPUT_Sheath_Slack", "Error", "TEXT", 100)
        #         else:
        #             pass
        # except Exception as e:
        #     arcpy.AddMessage("Issue in adding field (Error) to SPATIAL_JOIN_OUTPUT_Sheath_Slack.. :  " + str(e))


        # try:
        #     with arcpy.da.UpdateCursor("fibre_slack_join",("calc_lengt", "Error" , "Join_Count" , "TARGET_FID", "label"))as cursor:
        #         for rows in cursor:
        #             if rows[0] != None and rows[2] != None:
        #                 if rows[4] != None:
        #                     label = rows[4]
        #                 else:
        #                     label = rows[3]
        #                 if int(rows[0]) > 1000:
        #                     numb = self.slack_required(rows[0])
        #
        #                     if rows[2] < numb:
        #                         rows[1] = 'Slack  on cable ' + '"' + str(label) + '"' + ' required  over '+ str(numb * 1000)+'m'
        #                     elif rows[2] > numb:
        #                         rows[1] = str(rows[2] - numb) + ' Extra Slack  on cable ' + '"' + str(label) + '"'
        #
        #                 # if int(rows[0]) > 1000 and rows[2] != 1:
        #                 #     rows[1] = 'Slack  on cable ' + '"' + str(label) + '"' + ' required  over 1000m'
        #                 # if int(rows[0]) >= 2000 and rows[2] != 2:
        #                 #     rows[1] = 'Slack  on cable ' + '"' + str(label) + '"' + ' required  over 2000m'
        #                 # if int(rows[0]) >= 3000 and rows[2] != 3:
        #                 #     rows[1] = 'Slack  on cable ' + '"' + str(label) + '"' + ' required  over 3000m'
        #                 # if int(rows[0]) >= 4000 and rows[2] != 4:
        #                 #     rows[1] = 'Slack  on cable ' + '"' + str(label) + '"' + ' required  over 4000m'
        #                 # if int(rows[0]) >= 5000 and rows[2] != 5:
        #                 #     rows[1] = 'Slack  on cable ' + '"' + str(label) + '"' + ' required  over 5000m'
        #                 # if int(rows[0]) >= 6000 and rows[2] != 6:
        #                 #     rows[1] = 'Slack  on cable ' + '"' + str(label) + '"' + ' required  over 6000m'
        #                 # if int(rows[0]) >= 7000 and rows[2] != 7:
        #                 #     rows[1] = 'Slack  on cable ' + '"' + str(label) + '"' + ' required  over 7000m'
        #                 # if int(rows[0]) >= 8000 and rows[2] != 8:
        #                 #     rows[1] = 'Slack  on cable ' + '"' + str(label) + '"' + ' required  over 8000m'
        #                 # if int(rows[0]) >= 9000 and rows[2] != 9:
        #                 #     rows[1] = 'Slack  on cable ' + '"' + str(label) + '"' + ' required  over 9000m'
        #                 # if int(rows[0]) >= 10000 and rows[2] != 10:
        #                 #     rows[1] = 'Slack  on cable ' + '"' + str(label) + '"' + ' required  over 10000m'
        #
        #                 cursor.updateRow(rows)
        #         del cursor
        # except Exception as e:
        #     arcpy.AddMessage(str(e))

        try:
            with arcpy.da.UpdateCursor("fibre_slack_join", ("calc_lengt", "Error", "Join_Count" , "label"))as cursor:
                for rows in cursor:
                    if rows[0] != None and rows[2] != None:
                        if rows[0] >= 1000 and rows[0] <= 2000 and rows[2] < 1:
                                rows[1] = "Slack  on cable " + str(rows[3]) +" required  over 1000m"
                        if rows[0] >= 2000 and rows[0] <= 3000 and rows[2] < 2:
                                rows[1] = "Additional Slack on cable " + str(rows[3]) +" required  over 2000m"
                        if rows[0] >= 3000 and rows[0] <= 4000 and rows[2] < 3:
                            rows[1] = "Additional Slack on cable " + str(rows[3]) + " required  over 3000m"

                        if rows[0] > 4000:
                            rows[1] = "Incorrect Length on cable " + str(rows[3])
                        cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage(str(e))

        try:
            spatial_join_sheath_duct = arcpy.da.FeatureClassToNumPyArray("fibre_slack_join", ["TARGET_FID", "Error"], skip_nulls=True)
            arcpy.AddMessage(spatial_join_sheath_duct)
            filtered_arr_QC3_Boundaries = self.remove_duplicates_None_values(spatial_join_sheath_duct)
            arcpy.AddMessage(filtered_arr_QC3_Boundaries)
        except Exception as e:
            arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))

        if not arcpy.Exists(self.FinalOutput1):
            #arcpy.CopyFeatures_management(Fibre_Sheath, self.FinalOutput1)
            p16 = p1 + "\\" + self.FinalOutput1
            feats15 = arcpy.FeatureSet(table=Fibre_Sheath)
            feats15.save(p16)

        try:
            if arcpy.Exists(self.FinalOutput1):
                lstFields = arcpy.ListFields(self.FinalOutput1)
                field_names = [f.name for f in lstFields]
                if "Error" not in field_names:
                    arcpy.AddMessage("adding field (Error) in self.FinalOutput1..")
                    arcpy.AddField_management(self.FinalOutput1, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final Log File- 5.1.1 ")
            for x in filtered_arr_QC3_Boundaries:
                with arcpy.da.UpdateCursor(self.FinalOutput1, ("OBJECTID", "Error")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            if row[1] != None:
                                arcpy.AddMessage(str(row[1]))
                                row[1] = str(row[1]) + "," + x[1]
                            elif row[1] == None:
                                row[1] = x[1]
                                print(x[1])
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final  Log File- 5.1.1:  " + str(e))



    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("5.1.1..deleting intermediate layers")
        try:
            if arcpy.Exists("fibre_copy"):
                arcpy.Delete_management("fibre_copy")
            if arcpy.Exists("slack_copy"):
                arcpy.Delete_management("slack_copy")
            if arcpy.Exists("fibre_slack_join"):
                arcpy.Delete_management("fibre_slack_join")

            if arcpy.Exists("fibre_slack_join2"):
                arcpy.Delete_management("fibre_slack_join2")
            if arcpy.Exists("slack_copy_overlaps"):
                arcpy.Delete_management("slack_copy_overlaps")
            if arcpy.Exists("slack_copy_overlaps_table"):
                arcpy.Delete_management("slack_copy_overlaps_table")

        except Exception as e:
            arcpy.AddMessage("Issue in deleting layers :  " + str(e))

class ParentItem2(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.FinalOutput = name

    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 5.1.3')

        if not arcpy.Exists(self.FinalOutput):
            arcpy.AddMessage("Output does not exist so, creating new Output File")
            p7 = p1 + "\\" + self.FinalOutput
            feats6 = arcpy.FeatureSet(table=Fibre_Slack)
            feats6.save(p7)
            #arcpy.CopyFeatures_management(Fibre_Slack, self.FinalOutput)

        arcpy.SpatialJoin_analysis(Fibre_Slack, Poles, "output_slack", match_option="WITHIN_A_DISTANCE", search_radius=1)
        if arcpy.Exists("output_slack"):
            arcpy.AddField_management("output_slack", "Error", "TEXT", 100)

        ############# Item No 3.2.1 Update ###################
        try:
            with arcpy.da.UpdateCursor("output_slack", ["Join_Count", "dp_capacit", "Error"]) as cursor:
                for rows in cursor:
                    if rows[0] != None and rows[1] != None:
                        if int(rows[0]) != 0:
                            if rows[1] == 2:
                                rows[2] = "Slack on Pole with no additional capacity"
                                cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating error Aerial Slack will require Wayleave .. :  " + str(e))

        try:
            if arcpy.Exists(self.FinalOutput):
                lstFields_sc = arcpy.ListFields(self.FinalOutput)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.FinalOutput, "Error", "TEXT", 100)
                if "Warning" not in field_names_sc:
                    arcpy.AddMessage("adding field (Warning) in self.FinalOutput..")
                    arcpy.AddField_management(self.FinalOutput, "Warning", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.FinalOutput.. :  " + str(e))

        #
        try:
            arcpy.SpatialJoin_analysis(Fibre_Slack, Chambers, "SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers", "JOIN_ONE_TO_ONE", "KEEP_ALL", "", "INTERSECT", "1")
        except Exception as e:
            arcpy.AddMessage("Issue in SpatialJoin_analysis of Fibre_Slack, Splice_Closures.. :  " + str(e))

        try:
            if arcpy.Exists("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers"):
                lstFields_sc = arcpy.ListFields("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers..")
                    arcpy.AddField_management("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers", "Error", "TEXT", 500)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to SPATIAL_JOIN_OUTPUT_Sheath_Slack.. :  " + str(e))

        try:
            if arcpy.Exists("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers"):
                lstFields_sc = arcpy.ListFields("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Warning" not in field_names_sc:
                    arcpy.AddMessage("adding field (Warning) in SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers..")
                    arcpy.AddField_management("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers", "Warning", "TEXT", 500)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to SPATIAL_JOIN_OUTPUT_Sheath_Slack.. :  " + str(e))


        ############## Item No 3.2.2 Update ###################

        array_check = [12, 16, 17, 30, 31, 32, 34, 35, 36, 37, 39, 44, 45]
        try:
            with arcpy.da.UpdateCursor("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers", ("Join_Count", "type", "Error", "Warning")) as cursor:
                for row in cursor:
                    if row[0] != None and row[1] != None:
                        if row[0] != 0:
                            if row[1] not in array_check and row[1] != 9 and row[1] != 42:
                                row[2] = "Slack in unsuitable chamber Type: [" + str(row[1]) + "]"
                            if row[1] not in array_check and row[1] == 9:
                                row[3] = "Slack in unsuitable chamber Type: [9]"
                            if row[1] not in array_check and row[1] == 42:
                                row[3] = "Slack in unsuitable chamber Type: [42]"
                            cursor.updateRow(row)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating error Slack in unsuitable chamber Type .. :  " + str(e))

        ############## Item No 3.2.3 Update ###################
        try:
            with arcpy.da.UpdateCursor("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers", ("Join_Count", "condition", "Error")) as cursor:
                for row in cursor:
                    if row[0] != None and row[1] != None:
                        if row[0] != 0:
                            if int(row[1]) == 4 or int(row[1]) == 5:
                                if row[2] == None:
                                    row[2] = "Slack in chamber with poor condition."
                                elif row[2] != None:
                                    row[2] = str(row[2]) + ", Slack in chamber with poor condition."
                            cursor.updateRow(row)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating error Slack in chamber with poor condition.. :  " + str(e))

        ############## Item No 3.2.4 Update ###################
        try:
            with arcpy.da.UpdateCursor("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers", ("Join_Count", "lid_con", "Error")) as cursor:
                for row in cursor:
                    if row[0] != None and row[1] != None:
                        if row[0] != 0:
                            if row[1] == 2:
                                if row[2] == None:
                                    row[2] = "Slack in chamber with failed lid."
                                elif row[2] != None:
                                    row[2] = str(row[2]) + ", Slack in chamber with failed lid."
                            cursor.updateRow(row)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating error Slack in chamber with failed lid.. :  " + str(e))

        ############## Item No 3.2.5 Update ###################
        try:
            with arcpy.da.UpdateCursor("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers", ("Join_Count", "dp_capacit", "Error", "Warning")) as cursor:
                for row in cursor:
                    if row[0] and row[1] != None:
                        if row[0] != 0:
                            if row[1] == 2:
                                if row[2] ==  None:
                                    row[2] = "Slack in chamber without capacity for DP. "
                                elif row[2] != None:
                                    row[2] = str(row[2]) + ", Slack in chamber without capacity for DP. "
                            if row[1] == 3:
                                 if row[3] ==  None:
                                    row[3] = "Slack in chamber Unable to Determine. "
                                 elif row[3] != None:
                                     row[3] = str(row[3]) + ", Slack in chamber Unable to Determine. "
                            cursor.updateRow(row)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating error Slack in chamber without capacity for DP... :  " + str(e))
        ############## Item No 3.2.6 Update ###################
        # try:
        #     with arcpy.da.UpdateCursor("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers", ("hazard_1", "Warning", "Join_Count_1")) as cursor:
        #         for row in cursor:
        #             if row[0] != None:
        #                 if int(row[0]) == 2 and row[2] == 1:
        #                     if row[1] != None:
        #                         row[1] = str(row[1]) + ',' + "Slack in traffic sensitive chamber "
        #                     else:
        #                         row[1] = "Slack in traffic sensitive chamber "
        #                 cursor.updateRow(row)
        #         del cursor
        # except Exception as e:
        #     arcpy.AddMessage("Issue in Updating error Slack in traffic sensitive chamber.... :  " + str(e))

        try:
            spatial_join_sheath_duct = arcpy.da.FeatureClassToNumPyArray("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers", ["TARGET_FID", "Error", "Warning"], skip_nulls=False)
            arcpy.AddMessage("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers ..................  spatial_join_sheath_duct")
            arcpy.AddMessage(spatial_join_sheath_duct)
            # filtered_arr_QC3_Boundaries = self.remove_duplicates_None_values(spatial_join_sheath_duct)
            # arcpy.AddMessage(filtered_arr_QC3_Boundaries)
        except Exception as e:
            arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final Log File- 5.1.3 ")
            for x in spatial_join_sheath_duct:
                with arcpy.da.UpdateCursor(self.FinalOutput, ("OBJECTID", "Error", "Warning")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            if row[1] != None and x[1] != "None":
                                row[1] = str(row[1]) + "," + x[1]
                            elif row[1] == None and x[1] != "None":
                                row[1] = x[1]

                            if row[2] != None and x[2] != "None":
                                row[2] = str(row[2]) + "," + x[2]
                            elif row[2] == None and x[2] != "None":
                                row[2] = x[2]
                            cursor.updateRow(row)
                        else:
                            pass

                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final  Log File- 5.1.3:  " + str(e))

        try:
            spatial_join_sheath_duct2 = arcpy.da.FeatureClassToNumPyArray("output_slack", ["TARGET_FID", "Error"], skip_nulls=True)
            arcpy.AddMessage("output_slack........... spatial_join_sheath_duct2")
            arcpy.AddMessage(spatial_join_sheath_duct2)
            # filtered_arr_QC3_Boundaries = self.remove_duplicates_None_values(spatial_join_sheath_duct)
            # arcpy.AddMessage(filtered_arr_QC3_Boundaries)
        except Exception as e:
            arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final Log File- 5.1.3 ")
            for x in spatial_join_sheath_duct2:
                with arcpy.da.UpdateCursor(self.FinalOutput, ("OBJECTID", "Error")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            arcpy.AddMessage(str(row[0]))
                            if row[1] != None:
                                row[1] = str(row[1]) + "," + x[1]
                            elif row[1] == None:
                                row[1] = x[1]
                            cursor.updateRow(row)
                        else:
                            pass

                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final  Log File- 5.1.3:  " + str(e))



    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("deleting intermediate layers")
        if arcpy.Exists("output_slack"):
            arcpy.Delete_management("output_slack")
        if arcpy.Exists("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers"):
            arcpy.Delete_management("SPATIAL_JOIN_OUTPUT_Slack_merged_pole_chambers")

class ParentItem3(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        #self.Output_name = "fibre_copy"
        self.FinalOutput = name


    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 5.1.4')
        if not arcpy.Exists(self.FinalOutput):
            arcpy.AddMessage("Output does not exist so , creating new Output File")
            #arcpy.CopyFeatures_management(Fibre_Slack, self.FinalOutput)
            p8 = p1 + "\\" + self.FinalOutput
            feats7 = arcpy.FeatureSet(table=Fibre_Slack)
            feats7.save(p8)

        try:
            if arcpy.Exists(self.FinalOutput):
                lstFields_sc = arcpy.ListFields(self.FinalOutput)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.FinalOutput, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        try:
            arcpy.SpatialJoin_analysis(Fibre_Slack, Splice_Closures, "SPATIAL_JOIN_OUTPUT_Slack_Closures", "JOIN_ONE_TO_MANY",
                                       "KEEP_ALL", "", "INTERSECT", "1")
        except Exception as e:
            arcpy.AddMessage("Issue in SpatialJoin_analysis of Fibre_Slack, Splice_Closures.. :  " + str(e))

        try:
            if arcpy.Exists("SPATIAL_JOIN_OUTPUT_Slack_Closures"):
                lstFields_sc = arcpy.ListFields("SPATIAL_JOIN_OUTPUT_Slack_Closures")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in SPATIAL_JOIN_OUTPUT_Slack_Closures..")
                    arcpy.AddField_management("SPATIAL_JOIN_OUTPUT_Slack_Closures", "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to SPATIAL_JOIN_OUTPUT_Sheath_Slack.. :  " + str(e))

        try:
            with arcpy.da.UpdateCursor("SPATIAL_JOIN_OUTPUT_Slack_Closures", ("Join_Count", "Error"))as cursor:
                for rows in cursor:
                    if rows[0] == 1:
                        rows[1] = " Fibre Slack  shouldn't be in the same place with splice closure "
                    cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Calculating Error at SPATIAL_JOIN_OUTPUT_Sheath_Slack.. :  " + str(e))

        try:
            spatial_join_sheath_duct = arcpy.da.FeatureClassToNumPyArray("SPATIAL_JOIN_OUTPUT_Slack_Closures", ["TARGET_FID", "Error"], skip_nulls=True)
            arcpy.AddMessage(spatial_join_sheath_duct)
            filtered_arr_QC3_Boundaries = self.remove_duplicates_None_values(spatial_join_sheath_duct)
            arcpy.AddMessage(filtered_arr_QC3_Boundaries)
        except Exception as e:
            arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final Log File- 5.1.4 ")
            for x in filtered_arr_QC3_Boundaries:
                with arcpy.da.UpdateCursor(self.FinalOutput, ("OBJECTID", "Error")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            # arcpy.AddMessage(str(rows[0]) + " / " + str(x[0]))
                            if row[1] != None:
                                arcpy.AddMessage(str(row[1]))
                                row[1] = str(row[1]) + "," + x[1]
                            elif row[1] == None:
                                row[1] = x[1]
                                print(x[1])
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final  Log File- 5.1.4:  " + str(e))

    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("item5point1point3..deleting intermediate layers")
        try:
            if arcpy.Exists("SPATIAL_JOIN_OUTPUT_Slack_Closures"):
                arcpy.Delete_management("SPATIAL_JOIN_OUTPUT_Slack_Closures")
        except Exception as e:
            arcpy.AddMessage("Issue in UDeleting 5.1.3 :  " + str(e))

class ParentItem4(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.fibre_slack = "fibre_slack_copy"
        self.pole_copy = "pole_copy"
        self.chambers_copy = "chambers_copy"
        self.FinalOutput = name


    def ItemNo(self):
        arcpy.AddMessage('Tool Check Run For item 5.1.5')
        if not arcpy.Exists(self.FinalOutput):
            arcpy.AddMessage("Output does not exist so , creating new Output File")
            #arcpy.CopyFeatures_management(Fibre_Slack, self.FinalOutput)
            p8 = p1 + "\\" + self.FinalOutput
            feats7 = arcpy.FeatureSet(table=Fibre_Slack)
            feats7.save(p8)
        try:
            if arcpy.Exists(self.FinalOutput):
                lstFields_sc = arcpy.ListFields(self.FinalOutput)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddMessage("adding field (Error) in self.Output_name..")
                    arcpy.AddField_management(self.FinalOutput, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        arcpy.AddMessage("Tool Check: Is the slack not snapped to a pole or chamber?")

        if not arcpy.Exists(self.fibre_slack):
            #arcpy.CopyFeatures_management(Fibre_Slack, self.fibre_slack)
            p9 = p1 + "\\" + self.fibre_slack
            feats8 = arcpy.FeatureSet(table=Fibre_Slack)
            feats8.save(p9)
        if not arcpy.Exists(self.chambers_copy):
            arcpy.CopyFeatures_management(Chambers, self.chambers_copy)
            p10 = p1 + "\\" + self.chambers_copy
            feats9 = arcpy.FeatureSet(table=Chambers)
            feats9.save(p10)
        if not arcpy.Exists(self.pole_copy):
            #arcpy.CopyFeatures_management(Poles, self.pole_copy)
            p11 = p1 + "\\" + self.pole_copy
            feats10 = arcpy.FeatureSet(table=Poles)
            feats10.save(p11)

        lstFields_fibre = arcpy.ListFields(Fibre_Slack)
        field_names_fibre = [f.name for f in lstFields_fibre]
        if "OriginalFID_fs" not in field_names_fibre:
            arcpy.AddField_management(self.fibre_slack, "OriginalFID_fs", "LONG", 100)
            arcpy.CalculateField_management(self.fibre_slack, "OriginalFID_fs", "!OBJECTID!", "PYTHON3")

        arcpy.Merge_management([self.pole_copy, self.chambers_copy], "pole_chamber_merged")

        arcpy.SpatialJoin_analysis(self.fibre_slack, "pole_chamber_merged", "common_layer",
                                   join_operation='JOIN_ONE_TO_MANY', join_type='KEEP_ALL', field_mapping='',
                                   match_option='INTERSECT')

        if arcpy.Exists("common_layer"):
            lstFields_sc = arcpy.ListFields("common_layer")
            field_names_sc = [f.name for f in lstFields_sc]
            if "Error" not in field_names_sc:
                arcpy.AddField_management("common_layer", "Error", "Text", 10)
            else:
                pass

        with arcpy.da.UpdateCursor("common_layer", ("Join_Count", "Error", "OriginalFID_fs")) as cursor:
            for rows in cursor:
                if rows[0] == 0 and str(rows[2]) != "None":
                    rows[1] = "Slack not snapped to structure"
                    cursor.updateRow(rows)
                else:
                    cursor.deleteRow()

        array_common_layer = arcpy.da.FeatureClassToNumPyArray("common_layer", ["OriginalFID_fs", "Error"])

        for data in array_common_layer:
            with arcpy.da.UpdateCursor(self.FinalOutput, ("OBJECTID", "Error")) as cursor:
                for row in cursor:
                    if row[0] != None:
                        if str(row[0]) == str(data[0]):
                            if row[1] != None:
                                row[1] = str(row[1]) + ',' + str(data[1])
                            else:
                                row[1] = str(data[1])
                            cursor.updateRow(row)
                del cursor

    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("item5point1point5..deleting intermediate layers")
        if arcpy.Exists("common_layer"):
                arcpy.Delete_management("common_layer")
        if arcpy.Exists("pole_chamber_merged"):
                arcpy.Delete_management("pole_chamber_merged")
        if arcpy.Exists(self.fibre_slack):
                arcpy.Delete_management(self.fibre_slack)
        if arcpy.Exists(self.pole_copy):
                arcpy.Delete_management(self.pole_copy)
        if arcpy.Exists(self.chambers_copy):
                arcpy.Delete_management(self.chambers_copy)

class ParentItem5(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        arcpy.AddMessage("entered successfully inside - 5.1.6")
        self.FinalOutput = name
        self.Fibre_Slack = "Fibre_Slack_copy"
        self.chambers_copy = "chambers_copy"
        self.pole_copy = "pole_copy"

        ########################################## item 5.1.6 ##################################################3

    def ItemNo(self):
        arcpy.AddMessage("Tool Check: Flag any coincident Fibre_Slack ?")

        if not arcpy.Exists(self.FinalOutput):
            #arcpy.CopyFeatures_management(Fibre_Slack, self.FinalOutput)
            p12 = p1 + "\\" + self.FinalOutput
            feats10 = arcpy.FeatureSet(table=Fibre_Slack)
            feats10.save(p12)

        if not arcpy.Exists(self.chambers_copy):
            #arcpy.CopyFeatures_management(Chambers, self.chambers_copy)
            p13 = p1 + "\\" + self.chambers_copy
            feats11 = arcpy.FeatureSet(table=Chambers)
            feats11.save(p13)

        if not arcpy.Exists(self.pole_copy):
            #arcpy.CopyFeatures_management(Poles, self.pole_copy)
            p14 = p1 + "\\" + self.pole_copy
            feats12 = arcpy.FeatureSet(table=Poles)
            feats12.save(p14)

        # try:
        #     filed_list = arcpy.ListFields(self.FinalOutput)
        #     if "Warning" not in filed_list:
        #         arcpy.AddField_management(self.FinalOutput, "Warning", "Text", 500)
        #     if "Error" not in filed_list:
        #         arcpy.AddField_management(self.FinalOutput, "Error", "Text", 500)
        #
        # except Exception as e:
        #     arcpy.AddMessage("Issue in adding field (Error, Warning) to self.FinalOutput.. :  " + str(e))

        try:
            arcpy.SpatialJoin_analysis(self.chambers_copy, Fibre_Slack, "common_layer_slack_chamber7",
                                       join_operation='JOIN_ONE_TO_MANY', join_type='KEEP_ALL', field_mapping='',
                                       match_option='INTERSECT')
        except Exception as e:
            arcpy.AddMessage("Issue in Spatial Join chambers and fibre_slack.. :  " + str(e))

        try:
            arcpy.SpatialJoin_analysis(self.pole_copy, Fibre_Slack, "common_layer_slack_pole1",
                                       join_operation='JOIN_ONE_TO_MANY', join_type='KEEP_ALL', field_mapping='',
                                       match_option='INTERSECT')
        except Exception as e:
            arcpy.AddMessage("Issue in Spatial Join Pole and fibre_slack.. :  " + str(e))

        try:
            if arcpy.Exists("common_layer_slack_chamber7"):
                lstFields_sc = arcpy.ListFields("common_layer_slack_chamber7")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Count" not in field_names_sc:
                    arcpy.AddField_management("common_layer_slack_chamber7", "Count", "Long", 10)
                if "Warning" not in field_names_sc:
                    arcpy.AddField_management("common_layer_slack_chamber7", "Warning", "Text", 10)
        except Exception as e:
            arcpy.AddMessage("Issue in Adding field (Count) and (warning) in common_layer_slack_chamber7.. :  " + str(e))

        try:
            with arcpy.da.UpdateCursor("common_layer_slack_chamber7", ("Count")) as cursor:
                for rows in cursor:
                    rows[0] = 1
                    cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in (Count) in common_layer_slack_chamber7.. :  " + str(e))

        try:
            array = []
            with arcpy.da.UpdateCursor("common_layer_slack_chamber7", ("Count", "label")) as cursor:
                for rows in cursor:
                    if rows[1] != None:
                        if str(rows[1]) in array and str(rows[0]) != "None":
                            rows[0] = int(rows[0]) + 1
                            array.append(rows[1])
                        else:
                            rows[0] = 1
                            array.append(rows[1])
                        cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in counting and putting in array[] in common_layer_slack_chamber7.. :  " + str(e))

        try:
            with arcpy.da.UpdateCursor("common_layer_slack_chamber7", ("Count", "Warning")) as cursor:
                for rows in cursor:
                    if rows[0] != None:
                        if int(rows[0]) > 1:
                            rows[1] = "More than 1 slack in the same  chamber"
                            cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in writing (warning) common_layer_slack_chamber7.. :  " + str(e))
        #
        try:
            if arcpy.Exists("common_layer_slack_pole1"):
                lstFields_sc = arcpy.ListFields("common_layer_slack_pole1")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Count" not in field_names_sc:
                    arcpy.AddField_management("common_layer_slack_pole1", "Count", "Long", 100)
                if "Error" not in field_names_sc:
                    arcpy.AddField_management("common_layer_slack_pole1", "Error", "Text", 10)
        except Exception as e:
            arcpy.AddMessage("Issue in Adding field (Count) and (warning) in common_layer_slack_pole1.. :  " + str(e))
        #
        try:
            with arcpy.da.UpdateCursor("common_layer_slack_pole1", ("Count")) as cursor:
                for rows in cursor:
                    rows[0] = 1
                    cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in (Count) in common_layer_slack_pole1.. :  " + str(e))
        #
        try:
            array1 = []
            with arcpy.da.UpdateCursor("common_layer_slack_pole1", ("Count", "barcode")) as cursor:
                for rows in cursor:
                    if rows[1] != None:
                        if str(rows[1]) in array1:
                            rows[0] = int(rows[0]) + 1
                            array1.append(str(rows[1]))
                        else:
                            rows[0] = 1
                            array1.append(str(rows[1]))
                        cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in counting and putting in array1[] common_layer_slack_pole1.. :  " + str(e))

        try:
            with arcpy.da.UpdateCursor("common_layer_slack_pole1", ("Count", "Error")) as cursor:
                for rows in cursor:
                    if rows[0] != None:
                        if int(rows[0]) > 1:
                            rows[1] = "More than 1 slack in the same  pole"
                        cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in writing (Error) common_layer_slack_pole1.. :  " + str(e))

        try:
            array_chamber = arcpy.da.FeatureClassToNumPyArray("common_layer_slack_chamber7", ["JOIN_FID", "Warning"],
                                                              skip_nulls=True)
            arcpy.AddMessage(array_chamber)
        except Exception as e:
            arcpy.AddMessage("Issue in array_chamber.. :  " + str(e))

        try:
            array_pole = arcpy.da.FeatureClassToNumPyArray("common_layer_slack_pole1", ["JOIN_FID", "Error"],
                                                           skip_nulls=True)
            arcpy.AddMessage(array_pole)
        except Exception as e:
            arcpy.AddMessage("Issue in array_pole.. :  " + str(e))

        try:
            if arcpy.Exists(self.FinalOutput):
                lstFields_sc = arcpy.ListFields(self.FinalOutput)
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddField_management(self.FinalOutput, "Error", "Text", 10)
                if "Warning" not in field_names_sc:
                    arcpy.AddField_management(self.FinalOutput, "Warning", "Text", 10)
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error, Warning) to self.FinalOutput.. :  " + str(e))

        try:
            for data in array_pole:
                with arcpy.da.UpdateCursor(self.FinalOutput, ("OBJECTID", "Error")) as cursor:
                    for row in cursor:
                        if str(row[0]) != "None":
                            if str(row[0]) == str(data[0]):
                                if row[1] != None:
                                    row[1] = str(row[1]) + ',' + str(data[1])
                                else:
                                    row[1] = str(data[1])
                            cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in final (Error) array_pole to self.FinalOutput.. :  " + str(e))

        try:
            for data1 in array_chamber:
                with arcpy.da.UpdateCursor(self.FinalOutput, ("OBJECTID", "Warning")) as cursor:
                    for row in cursor:
                        if str(row[0]) != "None":
                            if str(row[0]) == str(data1[0]):
                                if row[1] != None:
                                    row[1] = str(row[1]) + ',' + str(data1[1])
                                else:
                                    row[1] = str(data1[1])
                            cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in final (Warning) array_chamber to self.FinalOutput.. :  " + str(e))

    def __del__(self):
        if arcpy.Exists(self.chambers_copy):
            arcpy.Delete_management(self.chambers_copy)
        if arcpy.Exists(self.pole_copy):
            arcpy.Delete_management(self.pole_copy)
        if arcpy.Exists("common_layer_slack_chamber7"):
            arcpy.Delete_management("common_layer_slack_chamber7")
        if arcpy.Exists("common_layer_slack_pole1"):
            arcpy.Delete_management("common_layer_slack_pole1")

class ParentItem6(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.Output_name = "fibre_copy1"
        self.FinalOutput = name

    def ItemNo(self):

        arcpy.AddMessage('Tool Check Run For item 5.1.2:')

        #arcpy.CopyFeatures_management(Fibre_Sheath, "fibre_copy")
        #arcpy.CopyFeatures_management(Fibre_Slack, "slack_copy")

        p4 = p1 + "\\" + "fibre_copy"
        p5 = p1 + "\\" + "slack_copy"

        feats2 = arcpy.FeatureSet(table=Fibre_Sheath)
        feats2.save(p4)

        feats3 = arcpy.FeatureSet(table=Fibre_Slack)
        feats3.save(p5)

        if arcpy.Exists("slack_copy"):
            arcpy.AddField_management("slack_copy", "OriginalFID_slack", "LONG", 10)
            arcpy.AddField_management("slack_copy", "Error", "TEXT", 500)
            arcpy.CalculateField_management("slack_copy", "OriginalFID_slack", "!OBJECTID!", "PYTHON3")

        arcpy.analysis.CountOverlappingFeatures("slack_copy", "slack_copy_overlaps", 2, "slack_copy_overlaps_table")

        arcpy.management.JoinField("slack_copy", "OBJECTID", "slack_copy_overlaps_table", "ORIG_OID",  ["OVERLAP_OID"])

        try:
            if arcpy.Exists("slack_copy"):
                lstFields_sc = arcpy.ListFields("slack_copy")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Count" not in field_names_sc:
                    arcpy.AddField_management("slack_copy", "Count", "Long", 10)
        except Exception as e:
            arcpy.AddMessage("Issue in Adding field (Count) in slack_copy.. :  " + str(e))

        try:
            with arcpy.da.UpdateCursor("slack_copy", ("Count")) as cursor:
                for rows in cursor:
                    rows[0] = 1
                    cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in (Count) in slack_copy.. :  " + str(e))

        try:
            array1 = []
            with arcpy.da.UpdateCursor("slack_copy", ("Count", "OVERLAP_OID")) as cursor:
                for rows in cursor:
                    if rows[1] != None:
                        if str(rows[1]) in array1:
                            rows[0] = int(rows[0]) + 1
                            array1.append(str(rows[1]))
                        else:
                            rows[0] = 1
                            array1.append(str(rows[1]))
                        cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating Cursor (OVERLAP_OID) slack_copy.. :  " + str(e))

        with arcpy.da.UpdateCursor("slack_copy", ["Count"]) as cursor:
            for row in cursor:
                if row[0] != None:
                    if row[0] == 2:
                        cursor.deleteRow()
            del cursor

        # try:
        #     arcpy.SpatialJoin_analysis("slack_copy", "fibre_copy", "slack_fibre_join", "JOIN_ONE_TO_ONE", "KEEP_ALL", "", "INTERSECT")
        # except Exception as e:
        #     arcpy.AddMessage("Issue in SpatialJoin_analysis of Fibre_Sheath, Fibre_Slack.. :  " + str(e))
        #
        # try:
        #     with arcpy.da.UpdateCursor("slack_fibre_join", ("calc_lengt", "Error", "Join_Count" , "TARGET_FID", "label"))as cursor:
        #         for rows in cursor:
        #             if rows[0] != None and rows[2] != None:
        #                 if rows[4] != None:
        #                     label = rows[4]
        #                 else:
        #                     label = rows[3]
        #
        #                 if rows[0] < 1000 and rows[2] == 1:
        #                     rows[1] = 'Slack  on cable ' + '"' + str(label) + '"' + ' not required below 1000m'
        #                 cursor.updateRow(rows)
        #         del cursor
        # except Exception as e:
        #     arcpy.AddMessage(str(e))

        try:
            arcpy.SpatialJoin_analysis("fibre_copy", "slack_copy", "fibre_slack_join2", "JOIN_ONE_TO_MANY", "KEEP_ALL", "", "INTERSECT")
        except Exception as e:
            arcpy.AddMessage("Issue in SpatialJoin_analysis of Fibre_Sheath, Fibre_Slack.. :  " + str(e))

        try:
            if arcpy.Exists("fibre_slack_join2"):
                lstFields_sc = arcpy.ListFields("fibre_slack_join2")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Count" not in field_names_sc:
                    arcpy.AddField_management("fibre_slack_join2", "Count", "Long", 10)
        except Exception as e:
            arcpy.AddMessage("Issue in Adding field (Count) in fibre_slack_join2.. :  " + str(e))

        try:
            with arcpy.da.UpdateCursor("fibre_slack_join2", ("Count")) as cursor:
                for rows in cursor:
                    rows[0] = 1
                    cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in (Count) in fibre_slack_join2.. :  " + str(e))

        try:
            index = 1
            array = []
            with arcpy.da.UpdateCursor("fibre_slack_join2", ("Count", "TARGET_FID")) as cursor:
                for rows in cursor:
                    if len(array) == 0:
                        array.append(rows[1])
                    if rows[1] in array:
                        index = index + 1
                        rows[0] = index
                    if rows[1] not in array:
                        array = []
                        array.append(rows[1])
                        index = 1
                    cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage(
                "Issue in counting and putting in array[] in fibre_slack_join2.. :  " + str(e))

        try:
            if arcpy.Exists("fibre_slack_join2"):
                lstFields_sc = arcpy.ListFields("fibre_slack_join2")
                field_names_sc = [f.name for f in lstFields_sc]
                if "Error" not in field_names_sc:
                    arcpy.AddField_management("fibre_slack_join2", "Error", "TEXT", 100)
        except Exception as e:
            arcpy.AddMessage("Issue in Adding field (Error) in fibre_slack_join2.. :  " + str(e))

        try:
            with arcpy.da.UpdateCursor("fibre_slack_join2", ("calc_lengt", "Error", "Count" , "label"))as cursor:
                for rows in cursor:
                    if rows[0] != None and rows[2] != None and rows[3] != None:
                        if rows[0] < 1000 and rows[2] > 0:
                            rows[1] = "Slack  on cable " + str(rows[3]) +"  not required below 1000m"
                        if rows[0] < 2000 and rows[0] > 1000 and rows[2] > 1:
                            rows[1] = "Additional Slack on cable " + str(rows[3]) +" not required  below 2000m"
                        if rows[0] < 3000 and rows[0] > 2000 and rows[2] > 2:
                            rows[1] = "Additional Slack on cable " + str(rows[3]) + " not required  below 3000m"
                        if rows[0] < 4000 and rows[0] > 3000 and rows[2] > 3:
                            rows[1] = "Additional Slack on cable " + str(rows[3]) + " not required  below 4000m"
                        cursor.updateRow(rows)
                del cursor
        except Exception as e:
            arcpy.AddMessage(str(e))


        if not arcpy.Exists(self.FinalOutput):
            #arcpy.CopyFeatures_management(Fibre_Slack, self.FinalOutput)
            p5 = p1 + "\\" + self.FinalOutput
            feats4 = arcpy.FeatureSet(table=Fibre_Slack)
            feats4.save(p5)


        try:
            if arcpy.Exists(self.FinalOutput):
                lstFields = arcpy.ListFields(self.FinalOutput)
                field_names = [f.name for f in lstFields]
                if "Error" not in field_names:
                    arcpy.AddMessage("adding field (Error) in self.FinalOutput..")
                    arcpy.AddField_management(self.FinalOutput, "Error", "TEXT", 100)
                else:
                    pass
        except Exception as e:
            arcpy.AddMessage("Issue in adding field (Error) to self.Output_name.. :  " + str(e))

        try:
            spatial_join_sheath_duct = arcpy.da.FeatureClassToNumPyArray("fibre_slack_join2", ["JOIN_FID", "Error"], skip_nulls=True)
            arcpy.AddMessage(spatial_join_sheath_duct)
            filtered_arr_QC3_Boundaries = self.remove_duplicates_None_values(spatial_join_sheath_duct)
            #arcpy.AddMessage(filtered_arr_QC3_Boundaries)
        except Exception as e:
            arcpy.AddMessage("Issue in Removing duplicate boundary names and errorless values : " + str(e))

        try:
            arcpy.AddMessage("Updating errors in final Log File - 5.1.2 ")
            for x in filtered_arr_QC3_Boundaries:
                with arcpy.da.UpdateCursor(self.FinalOutput, ("OBJECTID", "Error")) as cursor:
                    for row in cursor:
                        if str(row[0]) == str(x[0]):
                            if row[1] != None:
                                row[1] = str(row[1]) + "," + x[1]
                            elif row[1] == None:
                                row[1] = x[1]
                        else:
                            pass
                        cursor.updateRow(row)
                    del cursor
        except Exception as e:
            arcpy.AddMessage("Issue in Updating errors in final  Log File- 5.1.2:  " + str(e))


    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("item4point2point1..deleting intermediate layers")
        try:
            if arcpy.Exists("fibre_copy"):
                arcpy.Delete_management("fibre_copy")
            if arcpy.Exists("slack_copy"):
                arcpy.Delete_management("slack_copy")
            if arcpy.Exists("slack_fibre_join"):
                arcpy.Delete_management("slack_fibre_join")

            if arcpy.Exists("fibre_slack_join2"):
                arcpy.Delete_management("fibre_slack_join2")
            if arcpy.Exists("slack_copy_overlaps"):
                arcpy.Delete_management("slack_copy_overlaps")
            if arcpy.Exists("slack_copy_overlaps_table"):
                arcpy.Delete_management("slack_copy_overlaps_table")

        except Exception as e:
            arcpy.AddMessage("Issue in deleting layers :  " + str(e))

class BaseClass(ParentItem1, ParentItem2, ParentItem3, ParentItem4, ParentItem5):
    def __init__(self):
        from datetime import datetime

        now = datetime.now()
        start_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run Start's @" + start_time)
        arcpy.AddMessage("Script Run Start's @" + start_time)

        try:
            if arcpy.Exists('FibreSlack'):
                arcpy.AddMessage("Deleting (FibreSlack) Previous log file")
                arcpy.Delete_management('FibreSlack')
            if arcpy.Exists('SlackPlacement'):
                arcpy.AddMessage("Deleting (SlackPlacement) Previous log file")
                arcpy.Delete_management('SlackPlacement')
        except Exception as e:
            arcpy.AddMessage("Issue (SlackPlacement) to delete " + str(e))

        ### Object Intialize Here ####
        if item5point1point1and2 == 'true':
            ParentItem1.__init__(self, 'FibreSlack')
            ParentItem1.ItemNo(self)
            ParentItem1.__del__(self)

        if item5point1point3 == 'true':
            ParentItem2.__init__(self, 'SlackPlacement')
            ParentItem2.ItemNo(self)
            ParentItem2.__del__(self)

        if item5point1point4 == 'true':
            ParentItem3.__init__(self, 'SlackPlacement')
            ParentItem3.ItemNo(self)
            ParentItem3.__del__(self)

        if item5point1point5 == 'true':
            ParentItem4.__init__(self, 'SlackPlacement')
            ParentItem4.ItemNo(self)
            ParentItem4.__del__(self)

        if item5point1point6 == 'true':
            ParentItem5.__init__(self, 'SlackPlacement')
            ParentItem5.ItemNo(self)
            ParentItem5.__del__(self)

        if item5point1point1and2 == 'true':
            ParentItem6.__init__(self, 'SlackPlacement')
            ParentItem6.ItemNo(self)
            ParentItem6.__del__(self)

    def slack_required(self, cal_length):
        return int(cal_length/1000)

    def remove_duplicates_None_values(self, array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[1] != 'None':
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1

    def remove_duplicates_zero_values(array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[2] != 0:
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1


    def __del__(self):
        ## Object Remove Here ######

        if item5point1point1and2 == 'true' or item5point1point3 == 'true' or item5point1point4 == 'true' or item5point1point5 == 'true' or item5point1point6 == 'true':
            arcpy.AddMessage("inside del1")

            arcpy.AddMessage("inside del")
            fieldList = arcpy.ListFields(self.FinalOutput)
            fieldsToBeDeleted = []
            for field in fieldList:
                if str(field.name) not in (["OBJECTID", "Shape", "name", "Warning", "Error", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area",
                         "SHAPE_Area"]):
                    fieldsToBeDeleted.append(field.name)

            arcpy.AddMessage("Deleting these fields all others: " + str(fieldsToBeDeleted))

            try:
                if item5point1point4 != 'true' and item5point1point5 != 'true' and 'true' in (item5point1point3, item5point1point6):
                    with arcpy.da.UpdateCursor(self.FinalOutput, ("Error", "Warning")) as cursor:
                        for row in cursor:
                            if row[0] == None and row[1] == None:
                                cursor.deleteRow()
                        del cursor
                elif item5point1point1and2 == 'true' and 'true' not in (item5point1point3, item5point1point4, item5point1point5, item5point1point6):
                    with arcpy.da.UpdateCursor(self.FinalOutput, ("Error")) as cursor:
                        for row in cursor:
                            if row[0] == None:
                                cursor.deleteRow()
                        del cursor
                elif item5point1point3 != 'true' and item5point1point6 != 'true'  and 'true' in (item5point1point4, item5point1point5):
                    with arcpy.da.UpdateCursor(self.FinalOutput, ("Error")) as cursor:
                        for row in cursor:
                            if row[0] == None:
                                cursor.deleteRow()
                        del cursor
                elif item5point1point3 != 'true' and 'true' in (item5point1point1and2, item5point1point4, item5point1point5, item5point1point6):
                    with arcpy.da.UpdateCursor(self.FinalOutput, ("Error", "Warning")) as cursor:
                        for row in cursor:
                            if row[0] == None and row[1] == None:
                                cursor.deleteRow()
                        del cursor
                elif item5point1point3 != 'true' and 'true' in (item5point1point1and2, item5point1point4, item5point1point5):
                    with arcpy.da.UpdateCursor(self.FinalOutput, ("Error")) as cursor:
                        for row in cursor:
                            if row[0] == None:
                                cursor.deleteRow()
                        del cursor

                else:
                    arcpy.AddMessage("in else")
                    with arcpy.da.UpdateCursor(self.FinalOutput, ("Error", "Warning")) as cursor:
                        for row in cursor:
                            if row[0] == None and row[1] == None:
                                cursor.deleteRow()
                        del cursor


            except Exception as ex:
               arcpy.AddMessage(ex)

            try:
                arcpy.DeleteField_management(self.FinalOutput, fieldsToBeDeleted)
            except Exception as e:
                print(e)
            except arcpy.ExecuteError as ex:
                print(ex)

            arcpy.AddMessage("Finished Deleting unwanted fields of error ")

            arcpy.AddMessage(str(self.FinalOutput) + " - Log File has been Generated")


            if arcpy.Exists(self.FinalOutput1):
                fieldList1 = arcpy.ListFields(self.FinalOutput1)
                fieldsToBeDeleted1 = []
                for field in fieldList1:
                    if str(field.name) not in (
                            ["OBJECTID", "Shape", "name", "Shape_Length", "SHAPE", "SHAPE_Length", "Shape_Area",
                             "SHAPE_Area", "Error", "Shape__Length_3", "Shape__Length_2", "Shape__Length"]):
                        fieldsToBeDeleted1.append(field.name)

                arcpy.AddMessage("Deleting these fields for 5.1.1 : " + str(fieldsToBeDeleted1))

                try:
                    with arcpy.da.UpdateCursor(self.FinalOutput1, ("Error")) as cursor:
                        for row in cursor:
                            if row[0] == None:
                                cursor.deleteRow()
                        del cursor
                except Exception as ex:
                    arcpy.AddMessage(ex)

                try:
                    arcpy.DeleteField_management(self.FinalOutput1, fieldsToBeDeleted1)
                except Exception as e:
                    print(e)
                except arcpy.ExecuteError as ex:
                    print(ex)

                arcpy.AddMessage("Finished Deleting unwanted fields of error ")
                arcpy.AddMessage(str(self.FinalOutput1) + " - Log File has been Generated")

        from datetime import datetime

        now = datetime.now()
        end_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run End's @" + end_time)


######### Get All Input Parameter ###########

arcpy.env.overwriteOutput = True

input_geodatabase = arcpy.GetParameterAsText(0)
Fibre_Sheath = arcpy.GetParameterAsText(1)
Fibre_Slack = arcpy.GetParameterAsText(2)
Splice_Closures = arcpy.GetParameterAsText(3)
Poles = arcpy.GetParameterAsText(4)
Chambers = arcpy.GetParameterAsText(5)
######## Checked Input #########

item5point1point1and2 = arcpy.GetParameterAsText(6)
item5point1point3 = arcpy.GetParameterAsText(7)
item5point1point4 = arcpy.GetParameterAsText(8)
item5point1point5 = arcpy.GetParameterAsText(9)
item5point1point6 = arcpy.GetParameterAsText(10)

p = arcpy.mp.ArcGISProject("CURRENT")
p1 = p.defaultGeodatabase

######### Oject Creation #######
# main function
if __name__ == '__main__':
    # created the object for BaseClass
    if arcpy.Exists("FibreSlack"):
        arcpy.Delete_management('FibreSlack')
    if arcpy.Exists("SlackPlacement"):
        arcpy.Delete_management('SlackPlacement')

    ob = BaseClass()
