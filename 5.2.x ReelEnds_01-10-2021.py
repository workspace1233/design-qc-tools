import arcpy

arcpy.env.overwriteOutput = True

class ParentItem1(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.fibre_sheath_copy = "fibre_copy"
        self.splice_closure_copy = "splice_copy"
        self.Output_name = name

    def ItemNo(self):

        arcpy.AddMessage('Tool Check Run For item 5.2.1:')

        arcpy.CopyFeatures_management(fibre_sheath, self.fibre_sheath_copy)
        #arcpy.CopyFeatures_management(fibre_sheath, self.Output_name)
        arcpy.CopyFeatures_management(splice_closure, self.splice_closure_copy)

        lstFields_fibre = arcpy.ListFields(fibre_sheath)
        field_names_fibre = [f.name for f in lstFields_fibre]
        if "OriginalFID_fs" not in field_names_fibre:
            arcpy.AddField_management(self.fibre_sheath_copy, "OriginalFID_fs", "LONG", 10)
            arcpy.CalculateField_management(self.fibre_sheath_copy, "OriginalFID_fs", "!OBJECTID!", "PYTHON3")

        lstFields_splice = arcpy.ListFields(splice_closure)
        field_names_splice = [f.name for f in lstFields_splice]
        if "OriginalFID_sc" not in field_names_splice:
            arcpy.AddField_management(self.splice_closure_copy, "OriginalFID_sc", "LONG", 10)
            arcpy.AddField_management(self.splice_closure_copy, "out_type", "Text", 500)
            arcpy.CalculateField_management(self.splice_closure_copy, "OriginalFID_sc", "!OBJECTID!", "PYTHON3")

        if arcpy.Exists(self.fibre_sheath_copy):
            arcpy.AddField_management(self.fibre_sheath_copy, "start_point_x", "Text", 50)
            arcpy.AddField_management(self.fibre_sheath_copy, "start_point_y", "Text", 50)
            arcpy.AddField_management(self.fibre_sheath_copy, "end_point_x", "Text", 50)
            arcpy.AddField_management(self.fibre_sheath_copy, "end_point_y", "Text", 50)
            arcpy.AddField_management(self.fibre_sheath_copy, "ORIGINAL_REF_ID", "Text", 100)

        if arcpy.Exists(self.splice_closure_copy):
            arcpy.AddField_management(self.splice_closure_copy, "Error", "Text", 100)

        with arcpy.da.UpdateCursor(self.fibre_sheath_copy,
                                   ["start_point_x", "start_point_y", "end_point_x", "end_point_y",
                                    "SHAPE@"]) as cursor:
            for row in cursor:
                if row[4] != None:
                    start_point = row[4].firstPoint
                    end_point = row[4].lastPoint
                    row[0] = str(start_point.X)
                    row[1] = str(start_point.Y)
                    row[2] = str(end_point.X)
                    row[3] = str(end_point.Y)
                    cursor.updateRow(row)
            del cursor

        array_fibre = arcpy.da.FeatureClassToNumPyArray(self.fibre_sheath_copy,["OBJECTID", "end_point_x"], skip_nulls=True)

        array_fs_start = arcpy.da.FeatureClassToNumPyArray(self.fibre_sheath_copy,["start_point_x", "start_point_y", "type"], skip_nulls=True)


        with arcpy.da.UpdateCursor(self.fibre_sheath_copy,["start_point_x", "ORIGINAL_REF_ID", "start_point_y"]) as cursor:
            for row in cursor:
                if row[0] != None and len(array_fibre) != 0:
                    id = self.find_ObjectID_Type(array_fibre, row[0])
                    row[1] = str(id)
                cursor.updateRow(row)
            del cursor

        with arcpy.da.UpdateCursor(self.splice_closure_copy,["out_type", "SHAPE@XY"]) as cursor:
            for row in cursor:
                cable_type = self.findmacthing(array_fs_start, row[1])
                n_type = ''
                for type in cable_type:
                    if n_type == '':
                        n_type = str(type)
                    else:
                        n_type = str(n_type) + ', ' + str(type)
                row[0] = n_type
                cursor.updateRow(row)
            del cursor

        #array_fibre = arcpy.da.FeatureClassToNumPyArray(self.fibre_sheath_copy, ["ORIGINAL_REF_ID", "type", "OriginalFID_fs"], skip_nulls=True)

        arcpy.SpatialJoin_analysis(self.splice_closure_copy, self.fibre_sheath_copy, self.Output_name, join_operation='JOIN_ONE_TO_ONE', join_type='KEEP_ALL', match_option='INTERSECT')

        array_fibre_check = arcpy.da.FeatureClassToNumPyArray(self.Output_name, ["ORIGINAL_REF_ID", "type", "reel_end", "OriginalFID_fs", "OriginalFID_sc"], skip_nulls=True)
        #arcpy.AddMessage(array_fibre_check)

        with arcpy.da.UpdateCursor(self.Output_name,["OriginalFID_fs", "type", "Error", "ORIGINAL_REF_ID", "reel_end", "Shape", "end_point_x", "out_type"]) as cursor:
            for row in cursor:
                if row[0] != None:
                    x = row[5][0]
                    if str(x) == str(row[6]) and row[4] == 2:
                        record = self.find_RellEnd_Required(array_fibre_check, row[0], row[1], row[7])
                        if len(record) != 0:
                            arcpy.AddMessage(record)
                            if record[0]['type'] == True and record[0]['status'] == True:
                                row[2] = "Reel end needed, changing cable type"
                                cursor.updateRow(row)
                            # with arcpy.da.UpdateCursor(self.Output_name, ["OriginalFID_fs", "type", "Error", "ORIGINAL_REF_ID"]) as cursor1:
                            #     for row1 in cursor1:
                            #         if row1[0] != None:
                            #             if row1[0] in record[0]['data']:
                            #                 row1[2] = "Reel end needed, changing cable type"
                            #                 cursor1.updateRow(row1)
                            #     del cursor1
            del cursor

        # with arcpy.da.UpdateCursor(self.Output_name,["type", "Error", "reel_end", "out_type"]) as cursor:
        #     for row in cursor:
        #         out_type_arr = []
        #         out_type_arr.append(row[3])
        #         arcpy.AddMessage(out_type_arr)
        #     del cursor


    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("deleting intermediate layers")
        if arcpy.Exists(self.splice_closure_copy):
            arcpy.Delete_management(self.splice_closure_copy)

        if arcpy.Exists(self.fibre_sheath_copy):
            arcpy.Delete_management(self.fibre_sheath_copy)


class ParentItem2(object):
    def __init__(self, name):
        ### Object Intialize Here ####
        self.fibre_sheath_copy = "fibre_copy"
        self.splice_closure_copy = "splice_copy"
        self.Output_name = name

    def ItemNo(self):

        arcpy.AddMessage('Tool Check Run For item 5.2.2:')

        arcpy.CopyFeatures_management(fibre_sheath, self.fibre_sheath_copy)
        arcpy.CopyFeatures_management(splice_closure, self.splice_closure_copy)


        arcpy.SpatialJoin_analysis(self.splice_closure_copy, self.fibre_sheath_copy, "fibre_splice_join", join_operation='JOIN_ONE_TO_ONE', join_type='KEEP_ALL', match_option='INTERSECT')

        if not arcpy.Exists(self.Output_name):
            arcpy.CopyFeatures_management(splice_closure, self.Output_name)
            lstFields_fibre = arcpy.ListFields(self.Output_name)
            field_names_fibre = [f.name for f in lstFields_fibre]
            if "Error" not in field_names_fibre:
                arcpy.AddField_management(self.Output_name, "Error", "Text", 100)

        array_fibre_check = arcpy.da.FeatureClassToNumPyArray("fibre_splice_join", ["reel_end", "Join_Count", "OBJECTID"], skip_nulls=True)

        for data in array_fibre_check:
            with arcpy.da.UpdateCursor(self.Output_name, ["OBJECTID", "Error"]) as cursor:
                for row in cursor:
                    if row[0] != None:
                        if str(row[0]) == str(data[2]) and str(data[1]) == "1" and str(data[0]) != "1":
                            if row[1] != None:
                                row[1] = row[1] + ',' + "Reel end needed when closure doesn't have output cable."
                            else:
                                row[1] = "Reel end needed when closure doesn't have output cable."
                        cursor.updateRow(row)
                del cursor




    def __del__(self):
        #### Object Remove Here ######
        arcpy.AddMessage("deleting intermediate layers")
        if arcpy.Exists(self.splice_closure_copy):
            arcpy.Delete_management(self.splice_closure_copy)

        if arcpy.Exists(self.fibre_sheath_copy):
            arcpy.Delete_management(self.fibre_sheath_copy)

        if arcpy.Exists("fibre_splice_join"):
            arcpy.Delete_management("fibre_splice_join")


# class ParentItem4(object):
#     def __init__(self, name):
#         ### Object Intialize Here ####
#         self.fibre_sheath_copy = "fibre_copy"
#         self.splice_closure_copy = "splice_copy"
#         self.Output_name = name
#
#     def ItemNo(self):
#
#         arcpy.AddMessage('Tool Check Run For item 5.2.4:')
#         try:
#             arcpy.CopyFeatures_management(fibre_sheath, self.fibre_sheath_copy)
#             # arcpy.CopyFeatures_management(fibre_sheath, self.Output_name)
#             arcpy.CopyFeatures_management(splice_closure, self.splice_closure_copy)
#         except Exception as e:
#             arcpy.AddMessage(e)
#
#         lstFields_fibre = arcpy.ListFields(fibre_sheath)
#         field_names_fibre = [f.name for f in lstFields_fibre]
#         if "OriginalFID_fs" not in field_names_fibre:
#             arcpy.AddField_management(self.fibre_sheath_copy, "OriginalFID_fs", "LONG", 10)
#             arcpy.CalculateField_management(self.fibre_sheath_copy, "OriginalFID_fs", "!OBJECTID!", "PYTHON3")
#
#         lstFields_splice = arcpy.ListFields(splice_closure)
#         field_names_splice = [f.name for f in lstFields_splice]
#         if "OriginalFID_sc" not in field_names_splice:
#             arcpy.AddField_management(self.splice_closure_copy, "OriginalFID_sc", "LONG", 10)
#             arcpy.CalculateField_management(self.splice_closure_copy, "OriginalFID_sc", "!OBJECTID!", "PYTHON3")
#
#         if arcpy.Exists(self.fibre_sheath_copy):
#             arcpy.AddField_management(self.fibre_sheath_copy, "start_point_x", "Text", 50)
#             arcpy.AddField_management(self.fibre_sheath_copy, "start_point_y", "Text", 50)
#             arcpy.AddField_management(self.fibre_sheath_copy, "end_point_x", "Text", 50)
#             arcpy.AddField_management(self.fibre_sheath_copy, "end_point_y", "Text", 50)
#             arcpy.AddField_management(self.fibre_sheath_copy, "ORIGINAL_REF_ID", "Text", 100)
#
#         if arcpy.Exists(self.splice_closure_copy):
#             arcpy.AddField_management(self.splice_closure_copy, "Error", "Text", 100)
#
#         with arcpy.da.UpdateCursor(self.fibre_sheath_copy,
#                                    ["start_point_x", "start_point_y", "end_point_x", "end_point_y",
#                                     "SHAPE@"]) as cursor:
#             for row in cursor:
#                 if row[4] != None:
#                     start_point = row[4].firstPoint
#                     end_point = row[4].lastPoint
#                     row[0] = str(start_point.X)
#                     row[1] = str(start_point.Y)
#                     row[2] = str(end_point.X)
#                     row[3] = str(end_point.Y)
#                     cursor.updateRow(row)
#             del cursor
#
#         array_fibre = arcpy.da.FeatureClassToNumPyArray(self.fibre_sheath_copy, ["OBJECTID", "start_point_x"],
#                                                         skip_nulls=True)
#
#         with arcpy.da.SearchCursor(self.fibre_sheath_copy, ["end_point_x", "OBJECTID"]) as cursor:
#             for row in cursor:
#                 if row[1] != None:
#                     if row[0] != None and len(array_fibre) != 0:
#                         id = self.find_ObjectID(array_fibre, row[0])
#                         if len(id) != 0:
#                             for i in range(len(id)):
#                                 with arcpy.da.UpdateCursor(self.fibre_sheath_copy, ["OBJECTID","ORIGINAL_REF_ID"]) as cursor1:
#                                     for row1 in cursor1:
#                                         if str(row1[0]) == str(id[i]):
#                                             ref_id = str(row[1])
#                                             row1[1] = ref_id
#                                             cursor1.updateRow(row1)
#                                     del cursor1
#             del cursor
#
#         arcpy.SpatialJoin_analysis(self.splice_closure_copy, self.fibre_sheath_copy, self.Output_name, join_operation='JOIN_ONE_TO_MANY', join_type='KEEP_ALL', match_option='INTERSECT', search_radius="1" )
#
#         array_fibre_check = arcpy.da.FeatureClassToNumPyArray(self.Output_name,
#                                                               ["ORIGINAL_REF_ID", "type", "reel_end",
#                                                                "OriginalFID_fs", "end_point_x", "end_point_y"], skip_nulls=True)
#
#         #array_geometry_end = arcpy.da.FeatureClassToNumPyArray(self.Output_name, ["end_point_x", "end_point_y"], skip_nulls=True)
#
#         array_geometry_start = arcpy.da.FeatureClassToNumPyArray(self.Output_name, ["start_point_x", "start_point_y"], skip_nulls=True)
#
#         with arcpy.da.SearchCursor(self.fibre_sheath_copy,
#                                    ["OriginalFID_fs", "type", "ORIGINAL_REF_ID", "Shape@"]) as cursor:
#             for row in cursor:
#                 if row[0] != None:
#                     if str(row[0]) == "407":
#                         record = self.find_RellEnd_NotRequired(array_fibre_check, row[0], row[1])
#                         if len(record) != 0:
#                             arcpy.AddMessage(row[0])
#                             arcpy.AddMessage(record[0]['data'])
#                             arcpy.AddMessage(record[0]['end_geom'])
#
#                             with arcpy.da.UpdateCursor(self.Output_name, ["OriginalFID_fs", "type", "Error",
#                                                                           "ORIGINAL_REF_ID", "reel_end", "Shape@", "end_point_x"]) as cursor1:
#                                 for row1 in cursor1:
#                                     # if row1[0] != None:
#                                     #     if row1[0] in record[0]['data']:
#                                     #         row1[2] = "Reel end not needed"
#                                     #         cursor1.updateRow(row1)
#                                     if row1[0] != None and row1[1] != None:
#                                         if row[0] == row1[0] and row1[4] == 1 and str(row1[5][0].X) == row1[6]:
#                                             arcpy.AddMessage(row1[5][0].X)
#                                             arcpy.AddMessage(row1[6])
#                                             # end_geom = self.find_Matching(array_geometry_start, record[0]['end_geom'][i])
#                                             # arcpy.AddMessage(record[0]['end_geom'][i])
#                                             # arcpy.AddMessage(end_geom)
#                                             #if end_geom != True:
#                                             row1[2] = "Reel end not needed"
#                                             cursor1.updateRow(row1)
#                                         # elif row1[0] == record[0]['data'][i] and row1[1] != row[1] and row1[4] == 2:
#                                         #     end_geom = self.find_Matching(array_geometry_start, record[0]['end_geom'][i])
#                                         #     arcpy.AddMessage(record[0]['end_geom'][i])
#                                         #     arcpy.AddMessage(end_geom)
#                                         #     arcpy.AddMessage('elif')
#                                         #     if end_geom != True:
#                                         #         row1[2] = "Reel end not needed"
#                                         #         cursor1.updateRow(row1)
#
#                                 del cursor1
#             del cursor
#
#     def __del__(self):
#         #### Object Remove Here ######
#         arcpy.AddMessage("deleting intermediate layers")
#         if arcpy.Exists(self.splice_closure_copy):
#             arcpy.Delete_management(self.splice_closure_copy)
#
#         if arcpy.Exists(self.fibre_sheath_copy):
#             arcpy.Delete_management(self.fibre_sheath_copy)


class BaseClass(ParentItem1):
    def __init__(self):
        from datetime import datetime

        now = datetime.now()
        start_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run Start's @" + start_time)

        try:
            if arcpy.Exists('FibreSlack'):
                arcpy.AddMessage("Deleting (FibreSlack) Previous log file")
                arcpy.Delete_management('FibreSlack')
        except Exception as e:
            arcpy.AddMessage("Issue (FibreSlack) to delete " + str(e))

        ### Object Intialize Here ####
        if item5point2point1 == 'true':
            ParentItem1.__init__(self, 'ReelEnds')
            ParentItem1.ItemNo(self)
            ParentItem1.__del__(self)

        if item5point2point4 == 'true':
            ParentItem2.__init__(self, 'ReelEnds')
            ParentItem2.ItemNo(self)
            ParentItem2.__del__(self)
        #
        # if item5point2point3 == 'true':
        #     ParentItem3.__init__(self, 'ReelEnds')
        #     ParentItem3.ItemNo(self)
        #     ParentItem3.__del__(self)
        #
        # if item5point2point4 == 'true':
        #     ParentItem3.__init__(self, 'ReelEnds')
        #     ParentItem3.ItemNo(self)
        #     ParentItem3.__del__(self)

    def findmacthing(self, array, coordinate):
        type = []
        if len(array) > 0:
            for data in array:
                if str(data[0]) == str(coordinate[0]) and str(data[1]) == str(coordinate[1]):
                    type.append(data[2])
        return type

    def remove_duplicates_None_values(self, array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[1] != 'None':
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1

    def remove_duplicates_zero_values(array):
        array_find = []
        array_find1 = []
        if len(array) != 0:
            for items in array:
                if items[2] != 0:
                    if len(array_find) == 0:
                        array_find.append(items[0])
                        array_find1.append(items)
                    elif items[0] not in array_find:
                        array_find.append(items[0])
                        array_find1.append(items)
                    else:
                        pass
        return array_find1

    def find_ObjectID_Type(self, array, geom_start_x):
        id = ""
        id_array = []
        if(len(array) != 0):
            for data in array:
                if str(data[1]) == str(geom_start_x):
                    id = data[0]
                    break
        return id

    def find_ObjectID(self, array, geom_start_x):
        id = ""
        id_array = []
        if(len(array) != 0):
            for data in array:
                if str(data[1]) == str(geom_start_x):
                    id = data[0]
                    id_array.append(data[0])
                    #break
        return id_array

    def find_Matching(self, array, geom_end_x):
        matching = True
        if(len(array) != 0):
            for data in array:
                if str(data[0]) == str(geom_end_x):
                    matching = False
                    break
        return matching

    def find_RellEnd_Required(self, array, ObjectID, type, match_type):
        required = False
        arcpy.AddMessage("OBJECTID"+ str(ObjectID))
        arcpy.AddMessage("type"+ str(type))
        array_objectID = []
        array_record = []
        type_matched = False
        n_type = ''
        matching_type = ''
        compare_type = ''
        if (len(array) != 0):
            type_array = []
            reel_array = []
            if match_type != '':
                matching_type = match_type.split(',')
                matching_type[:] = list(map(int, matching_type))

            for data in array:
                if str(data[0]) == str(ObjectID):
                    if n_type == '':
                        n_type = str(data[1])
                    else:
                        n_type = str(n_type)+ ', ' + str(data[1])
                    type_array.append(data[1])
                    reel_array.append(data[2])
                    array_record.append(data[3])
            arcpy.AddMessage(type_array)
            arcpy.AddMessage(reel_array)
            if len(type_array) != 0 and len(reel_array) != 0:
                # if type not in type_array and 1 not in reel_array:
                if type not in type_array:
                    required = True
                    compare_type = n_type.split(',')
                    compare_type[:] = list(map(int, compare_type))
                    # if match_type == n_type:
                    #     type_matched = True
                    if len(compare_type) != len(matching_type):
                        type_matched = False
                    if sorted(compare_type) == sorted(matching_type):
                        type_matched = True

                    array_objectID.append({"status": required, "data": array_record, 'type': type_matched})
        return array_objectID

    def find_RellEnd_NotRequired(self, array, ObjectID, type):
        required = False
        array_objectID = []
        array_record = []
        if (len(array) != 0):
            type_array = []
            reel_array = []
            geom_end_array = []
            for data in array:
                if str(data[0]) == str(ObjectID):
                    type_array.append(data[1])
                    #reel_array.append(data[2])
                    array_record.append(data[3])
                    geom_end_array.append(data[4])
        if len(type_array) != 0:
            if type in type_array:
                required = True
                array_objectID.append({"status": required, "data": array_record, "end_geom": geom_end_array})
        return array_objectID

    def __del__(self):
        ## Object Remove Here ######

        if item5point2point1 == 'true' or item5point2point4 == 'true':
            fieldList = arcpy.ListFields(self.Output_name)
            fieldsToBeDeleted = []
            for field in fieldList:
                #if str(field.name) not in (["OBJECTID", "Shape",  "Error",  "SHAPE",  "Shape_Area", "SHAPE_Area", "reel_end"]):
                if str(field.name) not in (["OBJECTID", "Shape",  "Error",  "SHAPE",  "Shape_Area", "SHAPE_Area"]):
                    fieldsToBeDeleted.append(field.name)

            arcpy.AddMessage("Deleting these fields: " + str(fieldsToBeDeleted))

            try:
                with arcpy.da.UpdateCursor(self.Output_name, ("Error")) as cursor:
                    for row in cursor:
                        if row[0] == None:
                            cursor.deleteRow()
                    del cursor

            except Exception as ex:
                arcpy.AddMessage(ex)

            try:
                if arcpy.Exists(self.Output_name):
                    arcpy.DeleteField_management(self.Output_name, fieldsToBeDeleted)
            except Exception as e:
                print(e)
            except arcpy.ExecuteError as ex:
                print(ex)
        arcpy.AddMessage("Finished Deleting unwanted fields of error ")
        arcpy.AddMessage(str(self.Output_name) + " - Log File has been Generated")

        from datetime import datetime

        now = datetime.now()
        end_time = now.strftime("%H:%M:%S")

        arcpy.AddMessage("Script Run End's @" + end_time)


######### Get All Input Parameter ###########

arcpy.env.overwriteOutput = True

input_geodatabase = arcpy.GetParameterAsText(0)
splice_closure = arcpy.GetParameterAsText(1)
fibre_sheath = arcpy.GetParameterAsText(2)
######## Checked Input #########

item5point2point1 = arcpy.GetParameterAsText(3)
# item5point2point2 = arcpy.GetParameterAsText(3)
# item5point2point3 = arcpy.GetParameterAsText(4)
item5point2point4 = arcpy.GetParameterAsText(4)

######### Oject Creation #######
# main function
if __name__ == '__main__':
    # created the object for BaseClass
    if arcpy.Exists("ReelEnds"):
        arcpy.Delete_management("ReelEnds")
    ob = BaseClass()
